var Basedata = {
 module: function () {
  return 'basedata';
 },

 add: function () {
  window.location.href = url.base_url(Basedata.module()) + "add";
 },

 createTable: function () {
  window.location.href = url.base_url(Basedata.module()) + "createTable";
 },

 main: function () {
  window.location.href = url.base_url(Basedata.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Basedata.module()) + "index";
 },

 ubah: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "ubah/" + id;
 },

 ubahData: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "ubahData/" + id;
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Basedata.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Basedata.module()) + "index";
   }
  }
 },

 searchData: function (elm, e) {
  var table_id = $(elm).attr('table');
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Basedata.module()) + "searchData" + '/' + table_id + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Basedata.module()) + "data_input" + '/' + table_id;
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'jobdesk': {
    'pegawai': $('#pegawai').val(),
    'keterangan': $('#keterangan').val(),
    'tanggal': $('#tanggal').val(),
    'time_target': $('#time_target').val(),
    'sifat': $('#sifat').val(),
    'proses_bisnis': $('#proses_bisnis').val(),
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Basedata.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
//  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Basedata.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Basedata.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 doneTask: function (id, e) {
  e.preventDefault();
  var status = $('input#status').val();
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    status: status
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Basedata.module()) + "doneTask",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
     var reload = function () {
      window.location.href = url.base_url(Basedata.module()) + "detail" + '/' + id;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 ubah: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Basedata.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 hapusData: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Basedata.execDeletedData(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Basedata.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Basedata.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 execDeletedData: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Basedata.module()) + "deleteData/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Basedata.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Basedata.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Basedata.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Basedata.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Basedata.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',
   todayHighlight: true,
   autoclose: true,
  });

  $('input#time_target').datepicker({
   format: 'yyyy-mm-dd',
   todayHighlight: true,
   autoclose: true,
  });

  var dateToday = new Date();

  $("#tanggal_date").datepicker({
   format: 'yyyy-mm-dd',
   startDate: 'today',
   todayHighlight: true,
   changeMonth: true,
   changeYear: true,
  })
          .hide()
          .click(function () {
           $(this).hide();
          });
  $("#tanggal_date").show();
 },

 addField: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(1)').html('<i class="fa fa-minus fa-lg hover" onclick="Basedata.removeField(this)"></i>');
  tr.after(newTr);
 },

 removeField: function (elm) {
  $(elm).closest('tr').remove();
 },

 removeFieldData: function (elm) {
  $(elm).closest('tr').addClass('deleted');
  $(elm).closest('tr').addClass('hidden-content');
 },

 getPostTable: function () {
  var data = [];
  var tb_field = $('table#tb_field').find('tbody').find('tr');
  $.each(tb_field, function () {
   var field = $(this).find('input').val();
   if (field != "") {
    data.push({
     'id': $(this).attr('data_id') == "" ? "" : $(this).attr('data_id'),
     'field': field,
     'deleted': $(this).hasClass('deleted') ? 1 : 0
    });
   }
  });

  return {
   'id': $('input#id').val(),
   'nama_table': $('input#nama_tabel').val(),
   'field': data
  };
 },

 simpanTable: function (id) {
  var data = Basedata.getPostTable();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Basedata.module()) + "simpanTable",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Basedata.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanPegawai: function (id) {
  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     user_id: $('select#pegawai').val(),
     table: $('input#id').val()
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Basedata.module()) + "simpanPegawai",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 deletePegawai: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Basedata.execDeletedPegawai(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeletedPegawai: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Basedata.module()) + "deletePegawai/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 detailInputData: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "data_input/" + id;
 },

 detailData: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "detailData/" + id;
 },

 backData: function (id) {
  window.location.href = url.base_url(Basedata.module()) + "data_input/" + id;
 },

 addValue: function (table) {
  window.location.href = url.base_url(Basedata.module()) + "addData/" + table;
 },

 getPostValue: function () {
  var form_input = $('div.form_input');
  var data = [];
  $.each(form_input, function () {
   var input = $(this).find('input');
   var field_id = input.attr('field');
   var isi = input.val();

   data.push({
    field_id: field_id,
    'isi': isi
   });
  });

  return data;
 },

 simpanValue: function (id) {
  var data = Basedata.getPostValue();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('id', id);
  formData.append('table_id', $('input#table_id').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Basedata.module()) + "simpanValue",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
     var reload = function () {
      window.location.href = url.base_url(Basedata.module()) + "detailData/" + resp.id;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 showDaftarData: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    id: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Basedata.module()) + "showDaftarData",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('div.content_data').html(resp);
   }
  });
 },

 addData: function (elm, id) {
  var no = $.trim($(elm).closest('tbody').find('tr:last').find('td:eq(0)').text());
  var no = parseInt(no) + 1;
  $.ajax({
   type: 'POST',
   data: {
    row_id: id,
    no: no
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Basedata.module()) + "addDatarow",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    $(elm).closest('tbody').find('tr:last').after(resp);
   }
  });
 },

 removeData: function (elm) {
  $(elm).closest('tr').remove();
 },

 getPostDataKolom: function () {
  var data = [];
  var tr_input = $('tr.input_tr');
  var i = 1;
  $.each(tr_input, function () {
   var kolom = [];
   var kolom_input = $(this).find('input.kolom');
   $.each(kolom_input, function () {
    var id_kolom = $(this).attr('field_id');
    kolom.push({
     'field': id_kolom,
     'isi': $(this).val()
    });
   });

   data.push({
    'row': i++,
    'field': kolom
   });
  });

  return data;
 },

 simpanRow: function () {
  var kolom_input = $('input.kolom');
  if (kolom_input.length > 0) {
   var data = Basedata.getPostDataKolom();

   var formData = new FormData();
   formData.append('data', JSON.stringify(data));
   formData.append('table_id', $('input#table_id').val());

   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Basedata.module()) + "simpanRow",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  } else {
   toastr.error("Tidak ada input");
  }
 },

 searchInTableValue: function (elm, e) {
  Basedata.searchInTable($(elm).val(), '#tb_value');
 },

 searchInTable: function (value, elm) {
  $("" + elm + " tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 sortableTableEdit: function () {
  $('table#tb_value tbody').sortable({
   helper: function (e, ui) {
    ui.children().each(function () {
     $(this).width($(this).width());
    });
    return ui;
   },
   cursor: 'move',
   update: function (event, ui) {
    var data = $(this);

    var row_data = $('table#tb_value tbody').find('tr');
    var result_row = [];
    var i = 1;
    $.each(row_data, function () {
     var row_id = $(this).attr('row');
     result_row.push({
      'row_id': row_id,
      'urutan': i++
     });
    });

    Basedata.updateUrutan(result_row);
//   
//   $.ajax({
//    data: data,
//    type: 'POST',
//    url: 'URL_HERE'
//   });
   }
  }).disableSelection();
 },

 updateUrutan: function (data) {
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Basedata.module()) + "updateUrutan",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Diperbarui...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diperbarui");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Diperbarui");
    }
    message.closeLoading();
   }
  });
 }
};

$(function () {
 Basedata.setDate();
 $('select#pegawai').select2();
 Basedata.sortableTableEdit();
});