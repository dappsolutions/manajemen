var Fitur = {
 module: function () {
  return 'fitur';
 },

 add: function () {
  window.location.href = url.base_url(Fitur.module()) + "add";
 },

 main: function () {
  window.location.href = url.base_url(Fitur.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Fitur.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Fitur.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Fitur.module()) + "index";
   }
  }
 },

 getPostMenu: function () {
  var data = [];
  var menu = $('table#tb_menu').find('tbody').find('tr');
  $.each(menu, function () {
   var tr = $(this).closest('tr');
   if (tr.find('input#check').is(':checked')) {
    data.push({
     'menu': tr.attr('data_id'),
     'user_input': tr.attr('user_input'),
     'is_update': tr.attr('has') == '0' ? 0 : 1,
     'is_deleted': tr.attr('has') == '1' && tr.find('input#check').is(':checked') == false ? 1 : 0,
    });
   } else {
    if (tr.attr('has') == '1') {
     data.push({
      'menu': tr.attr('data_id'),
      'user_input': tr.attr('user_input'),
      'is_update': tr.attr('has') == '0' ? 0 : 1,
      'is_deleted': tr.attr('has') == '1' && tr.find('input#check').is(':checked') == false ? 1 : 0,
     });
    }
   }
  });

  return data;
 },
 
 getPstDashboard: function () {
  var data = [];
  var menu = $('table#tb_menu_dashboard').find('tbody').find('tr');
  $.each(menu, function () {
   var tr = $(this).closest('tr');
   if (tr.find('input#check_menu').is(':checked')) {
    data.push({
     'menu': tr.attr('data_id'),
     'user_input': tr.attr('user_input'),
     'is_update': tr.attr('has') == '0' ? 0 : 1,
     'is_deleted': tr.attr('has') == '1' && tr.find('input#check_menu').is(':checked') == false ? 1 : 0,
    });
   } else {
    if (tr.attr('has') == '1') {
     data.push({
      'menu': tr.attr('data_id'),
      'user_input': tr.attr('user_input'),
      'is_update': tr.attr('has') == '0' ? 0 : 1,
      'is_deleted': tr.attr('has') == '1' && tr.find('input#check_menu').is(':checked') == false ? 1 : 0,
     });
    }
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'fitur': {
    'user': $('#user').val(),
    'menu': Fitur.getPostMenu(),
    'dashboard': Fitur.getPstDashboard()
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Fitur.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
//  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Fitur.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Fitur.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 detailFitur: function (user) {
  $.ajax({
   type: 'POST',
   data: {
    user: user
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Fitur.module()) + "detailFitur",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 ubah: function (id) {
  window.location.href = url.base_url(Fitur.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Fitur.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Fitur.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Fitur.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Fitur.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Fitur.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Fitur.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Fitur.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Fitur.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Fitur.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Fitur.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 checkAll: function (elm) {
  var checked = $(elm).is(':checked');
  $('input.check_child').prop('checked', checked);
 },

 checkAllMenu: function (elm) {
  var checked = $(elm).is(':checked');
  $('input.check_child_menu').prop('checked', checked);
 },

 check: function (elm) {
  var total_row = $('input.check_child').length;
  var total_checked = 0;
  $.each($('input.check_child'), function () {
   if ($(this).is(':checked')) {
    total_checked += 1;
   }
  });

  if (total_checked == total_row) {
   $('input#check_all').prop('checked', true);
  } else {
   $('input#check_all').prop('checked', false);
  }
 },
 checkMenu: function (elm) {
  var total_row = $('input.check_child_menu').length;
  var total_checked = 0;
  $.each($('input.check_child_menu'), function () {
   if ($(this).is(':checked')) {
    total_checked += 1;
   }
  });

  if (total_checked == total_row) {
   $('input#check_all_menu').prop('checked', true);
  } else {
   $('input#check_all_menu').prop('checked', false);
  }
 }
};

$(function () {
// Fitur.setDate();
 $('select#user').select2();
});