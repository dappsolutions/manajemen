var Plan = {
 module: function () {
  return 'plan';
 },

 add: function () {
  window.location.href = url.base_url(Plan.module()) + "add";
 },

 createPlan: function () {
  window.location.href = url.base_url(Plan.module()) + "createPlan";
 },

 main: function () {
  window.location.href = url.base_url(Plan.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Plan.module()) + "index";
 },

 ubah: function (id) {
  window.location.href = url.base_url(Plan.module()) + "ubah/" + id;
 },

 ubahData: function (id) {
  window.location.href = url.base_url(Plan.module()) + "ubahData/" + id;
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Plan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Plan.module()) + "index";
   }
  }
 },

 searchData: function (elm, e) {
  var table_id = $(elm).attr('table');
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Plan.module()) + "searchData" + '/' + table_id + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Plan.module()) + "data_input" + '/' + table_id;
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'jobdesk': {
    'pegawai': $('#pegawai').val(),
    'keterangan': $('#keterangan').val(),
    'tanggal': $('#tanggal').val(),
    'time_target': $('#time_target').val(),
    'sifat': $('#sifat').val(),
    'proses_bisnis': $('#proses_bisnis').val(),
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Plan.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
//  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Plan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Plan.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 doneTask: function (id, e) {
  e.preventDefault();
  var status = $('input#status').val();
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    status: status
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Plan.module()) + "doneTask",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
     var reload = function () {
      window.location.href = url.base_url(Plan.module()) + "detail" + '/' + id;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 ubah: function (id) {
  window.location.href = url.base_url(Plan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Plan.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Plan.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 hapusData: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Plan.execDeletedData(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Plan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Plan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 execDeletedData: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Plan.module()) + "deleteData/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Plan.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Plan.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Plan.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Plan.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Plan.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',
   todayHighlight: true,
   autoclose: true,
   orientation: 'bottom left'
  });
 },

 addField: function (elm) {
  var tr = $(elm).closest('tbody').find('tr.plan:last');
  var tr_real = $(elm).closest('tbody').find('tr.real:last');

  var no_tr = tr.attr('no');
  var newTr = tr.clone();
  newTr.find('input').val('');

  var no_tr_clone = parseInt(no_tr) + 1;
  newTr.attr('no', no_tr_clone);
  newTr.attr('id', "tr_plan_" + no_tr_clone);
  newTr.attr('user_id', '');
  newTr.find('input#tanggal').datepicker('destroy').datepicker(
          {
           format: 'yyyy-mm-dd',
           todayHighlight: true,
           autoclose: true,
           orientation: 'bottom left'
          });

  newTr.find('td:eq(2)').html('<button class="btn btn-primary" onclick="Plan.choosePIC(this, event)">PIC</button>');
  newTr.find('td:last').html('<i class="fa fa-minus fa-lg hover" onclick="Plan.removeField(this)"></i>');
  tr.after(newTr);

  var newTrReal = tr_real.clone();
  tr.after(newTrReal);
//  $(elm).closest('tbody').find('tr.plan:last').after(newTrReal);
 },

 removeField: function (elm) {
  var index = $(elm).closest('tr').attr('no');
  index = parseInt(index) - 1;

  $('tr.plan:eq(' + index + ')').remove();
  $('tr.real:eq(' + index + ')').remove();
 },

 removeFieldData: function (elm) {
  $(elm).closest('tr').addClass('deleted');
  $(elm).closest('tr').addClass('hidden-content');
 },

 getPostTable: function () {
  var data = [];
  var tb_field = $('table#tb_field').find('tbody').find('tr');
  $.each(tb_field, function () {
   var field = $(this).find('input#nama_item').val();
   if (field != "") {
    data.push({
     'id': $(this).attr('data_id') == "" ? "" : $(this).attr('data_id'),
     'item': field,
     'tanggal': $(this).find('input#tanggal').val(),
     'keterangan': $(this).find('textarea#keterangan').val(),
     'deleted': $(this).hasClass('deleted') ? 1 : 0
    });
   }
  });

  return {
   'id': $('input#id').val(),
   'title': $('input#title').val(),
   'item': data
  };
 },

 getPostWeekTimeline: function (elm) {
  var data = [];
  var week_data = $(elm).find('td.week');
  $.each(week_data, function () {
   data.push({
    'week': $(this).text()
   });
  });

  return data;
 },

 getPostWeekRealTimeline: function (index_tr) {
  var data = [];
  var week_data = $('tr.real:eq(' + index_tr + ')').find('td.week');
  $.each(week_data, function () {
   data.push({
    'week': $(this).text()
   });
  });

  return data;
 },

 getPostPlan: function () {
  var data = [];
  var tr_plan = $('tr.plan');
  $.each(tr_plan, function () {
   var nama_item = $(this).find('input#nama_item').val();
   var deadline = $(this).find('input#tanggal').val();
   var pic = $(this).attr('user_id');
   var index_tr = $(this).index();
   if (pic != '') {
    data.push({
     'nama_item': nama_item,
     'deadline': deadline,
     'pic': pic,
     'plan_timeline': Plan.getPostWeekTimeline($(this)),
     'real_timeline': Plan.getPostWeekRealTimeline(index_tr),
    });
   }
  });

  return data;
 },

 getPostPlanTimeline: function () {
  var data = [];
  var td_bulan = $('td.bulan_column');
  $.each(td_bulan, function () {
   var bulan = $(this).find('select#bulan').val();
   data.push({
    'timeline': bulan
   });
  });

  return data;
 },

 getPostItemData: function () {
  var data = {
   'id': $('input#id').val(),
   'title': $('input#title').val(),
   'plan': Plan.getPostPlan(),
   'plan_timeline': Plan.getPostPlanTimeline()
  };

  return data;
 },

 simpanPlan: function (id) {
  var data = Plan.getPostItemData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Plan.module()) + "simpanPlan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Plan.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanPegawai: function (id) {
  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     user_id: $('select#pegawai').val(),
     table: $('input#id').val()
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Plan.module()) + "simpanPegawai",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 deletePegawai: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Plan.execDeletedPegawai(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeletedPegawai: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Plan.module()) + "deletePegawai/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 detailInputData: function (id) {
  window.location.href = url.base_url(Plan.module()) + "data_input/" + id;
 },

 detailData: function (id) {
  window.location.href = url.base_url(Plan.module()) + "detailData/" + id;
 },

 backData: function (id) {
  window.location.href = url.base_url(Plan.module()) + "data_input/" + id;
 },

 addValue: function (table) {
  window.location.href = url.base_url(Plan.module()) + "addData/" + table;
 },

 getPostValue: function () {
  var form_input = $('div.form_input');
  var data = [];
  $.each(form_input, function () {
   var input = $(this).find('input');
   var field_id = input.attr('field');
   var isi = input.val();

   data.push({
    field_id: field_id,
    'isi': isi
   });
  });

  return data;
 },

 simpanValue: function (id) {
  var data = Plan.getPostValue();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('id', id);
  formData.append('table_id', $('input#table_id').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Plan.module()) + "simpanValue",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
     var reload = function () {
      window.location.href = url.base_url(Plan.module()) + "detailData/" + resp.id;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 removeDate: function (elm) {
  var index = $(elm).closest('td.bulan_column').index();

  var column_timeline = $('th#th_timeline');
  $(elm).closest('td').remove();

//timeline th
  var total_column_date = $('td.bulan_column');
  var colspan_timeline = parseInt($(column_timeline).attr('colspan'));


  //tr weekly day
  var tr_field = $('table#tb_field').find('thead').find('tr:eq(2)');

  var index_awal_week = (index * 4) - 1;
  var _week = tr_field.find('td.week');
  for (i = 0; i < 4; i++) {
   var no = index_awal_week + 1;
   tr_field.find('td.week:eq(' + no + ')').remove();
  }

  //body week day
  var tr_field_body = $('table#tb_field').find('tbody').find('tr');
  $.each(tr_field_body, function () {
   var _week = $(this).find('td.week');
   for (i = 0; i < 4; i++) {
    var no = index_awal_week + 1;
    $(this).find('td.week:eq(' + no + ')').remove();
   }
  });

  column_timeline.attr('colspan', colspan_timeline - 4);
 },

 addHeader: function (elm) {
  var column_timeline = $('th#th_timeline');
  var column_date = $(elm).closest('thead').find('tr:eq(1)').find('td:last');
  var clone_column_data = column_date.clone();
  clone_column_data.find('i').removeClass('fa-plus');
  clone_column_data.find('i').addClass('fa-trash');
  clone_column_data.find('i').attr('onclick', "Plan.removeDate(this)");
  column_date.after(clone_column_data);

  //timeline th
  var total_column_date = $('td.bulan_column');
  var colspan_timeline = parseInt($(column_timeline).attr('colspan'));

  //tr weekly day
  var tr_field = $('table#tb_field').find('thead').find('tr:eq(2)');
  var _week = tr_field.find('td.week');
  for (i = 0; i < 4; i++) {
   var no = i + 1;
   var add_week = '<td class="text-center week">' + no + '</td>';
   tr_field.append(add_week);
  }

  //body week day
  var tr_field_body = $('table#tb_field').find('tbody').find('tr');
  $.each(tr_field_body, function () {
   var _week = $(this).find('td.week');
   for (i = 0; i < 4; i++) {
    var add_week = '<td class="text-center week">&nbsp;</td>';
    $(this).find('td.week:last').after(add_week);
   }
  });

  column_timeline.attr('colspan', colspan_timeline + 4);
 },

 choosePIC: function (elm, e) {
  var index_content = $(elm).closest('tr').attr('no');
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    index_content: index_content
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Plan.module()) + "choosePIC",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 searchInTablePegawai: function (elm, e) {
  Plan.searchInTable($(elm).val(), '#tb_pegawai');
 },

 searchInTable: function (value, elm) {
  $("" + elm + " tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 exeChoosePIC: function (elm) {
  var user_id = $(elm).closest('tr').attr('user_id');
  var nama_pegawai = $(elm).closest('tr').find('td:eq(2)').text();
  var index_plan = $('input#index_content').val();
  var tr_plan = $('tr#tr_plan_' + index_plan);

  tr_plan.find('td:eq(2)').html(nama_pegawai);
  tr_plan.attr('user_id', user_id);
  message.closeDialog();
 },

 showCalendar: function (elm) {
  var id = $(elm).closest('td').attr('data_id');
  $(elm).datepicker({
   format: 'yyyy-mm-dd',
   todayHighlight: true,
   autoclose: true,
   orientation: 'bottom left'
  }).on('changeDate', function (selected) {
   var minDate = new Date(selected.date.valueOf());
   var month = minDate.getMonth() < 10 ? '0' + minDate.getMonth().toString() : minDate.getMonth();
   var day = minDate.getDate() < 10 ? '0' + minDate.getDate().toString() : minDate.getDate();
   
   var date_str = minDate.getFullYear() + "-" + month + "-" + day;
//   console.log(date_str);
//   return;
   $(elm).closest('td').html(date_str);
   Plan.updateWeekTimeline(id, date_str);
//   $('#dateTo').datepicker('setStartDate', minDate);
  });
 },

 updateWeekTimeline: function (id, date_str) {
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    date_str: date_str
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Plan.module()) + "updateWeekTimeline",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disimpan");
//     var reload = function () {
//      window.location.reload();
//     };
//
//     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 }
};

$(function () {
 Plan.setDate();
 $('select#pegawai').select2();
});