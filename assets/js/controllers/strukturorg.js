var Strukturorg = {
 module: function () {
  return 'strukturorg';
 },

 add: function (elm) {
  var jabatan_header = $(elm).attr('jabatan_header');
  window.location.href = url.base_url(Strukturorg.module()) + "add/" + jabatan_header;
 },

 main: function () {
  window.location.href = url.base_url(Strukturorg.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Strukturorg.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Strukturorg.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Strukturorg.module()) + "index";
   }
  }
 },

 getPostBawahan: function () {
  var data = [];
  var table_bawahan = $('table#tb_bawahan').find('tbody').find('tr');
  $.each(table_bawahan, function () {
   data.push({
    'bawahan': $(this).find('select').val()
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'jabatan_header': $('#jabatan_header').val(),
   'struktur': {
    'atasan': $('#atasan').val(),
    'bawahan': Strukturorg.getPostBawahan(),
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Strukturorg.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
//  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Strukturorg.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Strukturorg.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Strukturorg.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Strukturorg.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Strukturorg.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 deletePop: function (elm) {
  var id = $.trim($(elm).attr('id_data'));
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Strukturorg.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Strukturorg.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Strukturorg.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 execDeletedHeader: function (elm, id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Strukturorg.module()) + "deleteHeader/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Strukturorg.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 editPop: function (elm) {
  var id = $(elm).attr('id_data');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Strukturorg.module()) + "editPop/" + id,

   error: function () {
    toastr.error("Gagal");
   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
    });
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var no = tr.find('select').attr('no');
  var no = parseInt(no) + 1;
//  var newTr = tr.clone();
//  newTr.find('td:eq(0)').find('select').attr('no', no);
//  newTr.find('td:eq(0)').find('select').attr('id', 'bawahan-' + no);
//  newTr.find('select#bawahan-' + no).select2();
//  newTr.find('td:eq(1)').html('<i class="fa fa-minus fa-2x text-danger hover" onclick="Strukturorg.removeDetail(this)"></i>');
//  tr.after(newTr);

  $.ajax({
   type: 'POST',
   data: {
    no: no
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Strukturorg.module()) + "addDetail",
   error: function () {
    toastr.error("Gagal");
   },

   success: function (resp) {
    $('table#tb_bawahan').find('tbody').append(resp);
   }
  });
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Strukturorg.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Strukturorg.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Strukturorg.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Strukturorg.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Strukturorg.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 getDetailOrg: function (elm, id) {
//  $('input#ket_header').val($(elm).val());
  $('i#btn_add_org').attr('jabatan_header', $(elm).attr('header_id'));

  $('label#title_bagan').text($(elm).attr('title_head'));
  Strukturorg.makeChartOrg(id);
 },

 makeChartOrg: function (header = '') {
  $.ajax({
   type: 'POST',
   data: {
    header: header
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Strukturorg.module()) + "getDataJson",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving...");
   },

   success: function (resp) {
    message.closeLoading();
    var data = resp;
    $('#tree').html('');
    OrgTree.makeOrgTree($('#tree'), data);
//    new Treant(data);
//    console.log("DATA",data);
//    new Treant([
//     {
//      container: "#org",
//
//      connectors: {
//       type: 'step'
//      },
//      node: {
//       HTMLclass: 'nodeExample1'
//      }
//     },
//     ceo = {
//      text: {
//       name: "Mark Hill",
//       title: "Chief executive officer",
//       contact: "Tel: 01 213 123 134",
//      },
//      image: "../assets/images/faces-clipart/pic-1.png"
//     }, {
//      parent: ceo,
//      text: {
//       name: "Joe Linux",
//       title: "Chief Technology Officer",
//      },
//      stackChildren: true,
//      image: "../assets/images/faces-clipart/pic-2.png"
//     },
//    ]);
   }
  });
 },

 addJabatanHeader: function (nama = '', id = "") {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul Organisasi</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='title' placeholder='Masukkan Judul' value='" + nama + "'/><br/>";
  html += "<button data_id='" + id + "' class='btn btn-success font-10'onclick='Strukturorg.simpanJabatanHeader(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 simpanJabatanHeader: function (elm) {
  var id = $(elm).attr('data_id');
  if (validation.run()) {
   var title = $('input#title').val();
   $.ajax({
    type: 'POST',
    data: {
     title: title
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Strukturorg.module()) + "simpanJabatanHeader",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Strukturorg.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 deleteJabatanHeader: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Strukturorg.execDeletedHeader(this," + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },
};

$(function () {
// Strukturorg.setDate();
// Strukturorg.makeChartOrg(); 
 Strukturorg.makeChartOrg();

 $('select#atasan').select2();
 $('select#bawahan-1').select2();
});