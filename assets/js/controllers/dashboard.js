var Dashboard = {
 module: function () {
  return 'dashboard';
 },

 removeMenu: function (elm) {
  $(elm).closest('li').remove();
 },

 addLeftMenu: function (elm) {
  var left_menu = $(elm).closest('li');
  var newLeft = left_menu.clone();
  var input = '<div><input type="text" onkeyup="Dashboard.saveTitle(this, event)" class = "form-control required" error="Judul" placeholder="Isi Judul" /></div>';
  input += '<div class="text-right"><i class="fa fa-minus text-danger hover fa-lg" onclick="Dashboard.removeMenu(this)"></i></div>';
  newLeft.html(input);
  newLeft.css({
   padding: '16px'
  });
  left_menu.before(newLeft);
 },

 editLeftMenu: function (elm) {
  var room_title = $(elm).closest('li').attr('room_name');
  var room = $(elm).closest('li').attr('room');
  var id = $(elm).closest('li').attr('data_id');
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='room_title_child' placeholder='Masukkan Judul' value='" + room_title + "'/><br/>";
  html += "<button data_id='" + id + "' parent_room='" + room + "' class='btn btn-success font-10'onclick='Dashboard.saveTitleEdit(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 editBodyDetail: function (elm, id) {
  var room_title = $(elm).closest('tr').attr('data_name');
  var id = $(elm).closest('tr').attr('data_id');
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='room_title_child' placeholder='Masukkan Judul' value='" + room_title + "'/><br/>";
  html += "<button data_id='" + id + "' class='btn btn-success font-10'onclick='Dashboard.saveTitleEditBody(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 addRoom: function (nama = '', id = "") {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-left'>";
  html += "<h5>Masukkan Judul Room</h5>";
  html += "<div class='text-right'>";
  html += "<input type='text' class='form-control required' error='Judul' id='room_title' placeholder='Masukkan Judul' value='" + nama + "'/><br/>";
  html += "<button data_id='" + id + "' class='btn btn-success font-10'onclick='Dashboard.simpanRoom(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 deleteRoom: function (id, table) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button table='" + table + "' class='btn btn-success font-10'onclick='Dashboard.execDeleted(this," + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (elm, id) {
  var table = $(elm).attr('table');
  $.ajax({
   type: 'POST',
   data: {
    table: table
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Dashboard.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Dashboard.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addBody: function (elm, room_header) {
  $.ajax({
   type: 'POST',
   data: {
    room_header: room_header
   },
   dataType: 'html',
   async: false,
   url: "dashboard/addBody",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 editBody: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    id: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "editBody",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 simpanBody: function (elm, room_header) {
  if (validation.run()) {
   var title = $('input#title-body').val();
   var id = $('input#id_body').val();
   var pesan = $('input#pesan').val();
   var url = $('input#url').val();
   var tipe_upload = $('#tipe_upload').val();

   var data = {
    'title': title,
    'id': id,
    'pesan': pesan,
    'tipe_upload': tipe_upload,
    'room_header': room_header,
    'url': url
   };
   var formData = new FormData();
   formData.append('data', JSON.stringify(data));
   formData.append('file', $('input#file').prop('files')[0]);
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: "dashboard/simpanBody",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanRoom: function (elm) {
  var id = $(elm).attr('data_id');
  if (validation.run()) {
   var title = $('input#room_title').val();
   $.ajax({
    type: 'POST',
    data: {
     title: title
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "simpanRoom",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 saveTitle: function (elm, e) {
  if (e.keyCode == 13) {
   if (validation.run()) {
    var parent_room = $(elm).closest('div.row').attr('parent_room');
    var title = $(elm).val();
    $.ajax({
     type: 'POST',
     data: {
      title: title,
      room: parent_room
      , id: ''
     },
     dataType: 'json',
     async: false,
     url: url.base_url(Dashboard.module()) + "simpanTitle",
     error: function () {
      toastr.error("Gagal");
      message.closeLoading();
     },

     beforeSend: function () {
      message.loadingProses("Proses Simpan...");
     },

     success: function (resp) {
      if (resp.is_valid) {
       toastr.success("Berhasil Disimpan");
       var reload = function () {
        window.location.href = url.base_url(Dashboard.module()) + "index";
       };

       setTimeout(reload(), 1000);
      } else {
       toastr.error("Gagal Disimpan");
      }
      message.closeLoading();
     }
    });
   }
  }
 },

 saveTitleEdit: function (elm) {
  if (validation.run()) {
   var parent_room = $(elm).attr('parent_room');
   var title = $(elm).closest('div').find('input').val();
   var id = $(elm).attr('data_id');
   $.ajax({
    type: 'POST',
    data: {
     title: title,
     room: parent_room
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "simpanTitle",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 saveTitleEditBody: function (elm) {
  if (validation.run()) {
   var title = $(elm).closest('div').find('input').val();
   var id = $(elm).attr('data_id');
   $.ajax({
    type: 'POST',
    data: {
     title: title
     , id: id
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Dashboard.module()) + "saveTitleEditBody",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 detailBody: function (elm) {
  var id = $(elm).closest('li').attr('data_id');
  var room = $(elm).closest('li').attr('room');
  var room_name = $(elm).closest('li').attr('room_name');


  $.ajax({
   type: 'POST',
   data: {
    room: room,
    room_name: room_name,
    id: id
   },
   dataType: 'json',
   async: false,
   url: "dashboard/detailBody",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    $('b#judul-' + room).text(room_name);
//    $('btn#btn_add_body').attr('id_parent', id);
    $('div#add_body-' + room).html(resp.view_btn);
    $('table#tb_body-' + room).find('tbody').html(resp.view_body);
   }
  });
 },

 detail_trans: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "detail_trans/" + id_body;
 },

 createTable: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "createTable/" + id_body;
 },

 back: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "index";
 },

 detailTrans: function (elm, id_body) {
  window.location.href = url.base_url(Dashboard.module()) + "detailTrans" + '/' + id_body;
 },

 changeTipeUpload: function (elm) {
  var tipe = $(elm).val();
  if (tipe == "no_upload") {
   $('div#div_upload').removeClass('hidden-content');
   $('div#div_upload').addClass('hidden-content');
  } else {
   if (tipe == "1") {
    $('div#div_upload').removeClass('hidden-content');
    tipe = "jpg";
    $('#file').attr('onchange', "Dashboard.checkFile(this, '" + tipe + "')");
   } else {
    if (tipe == '2') {
     $('div#div_upload').removeClass('hidden-content');
     tipe = "pdf";
     $('#file').attr('onchange', "Dashboard.checkFile(this, '" + tipe + "')");
    }

    if (tipe == '3') {
     $('div#div_url').removeClass('hidden-content');
    }
   }
  }
 },

 getPostBodyData: function () {
  var data = {
   'room_body': $('input#room_body').val(),
   'title': $('input#judul').val(),
   'tipe_upload': $('select#tipe_upload').val(),
  };
  return data;
 },

 simpanTrans: function (id) {
  if (validation.run()) {
   var data = Dashboard.getPostBodyData();

   var formData = new FormData();
   formData.append('data', JSON.stringify(data));
   formData.append("id", id);
   formData.append('file', $('input#file').prop('files')[0]);

   if (validation.run()) {
    $.ajax({
     type: 'POST',
     data: formData,
     dataType: 'json',
     processData: false,
     contentType: false,
     async: false,
     url: url.base_url(Dashboard.module()) + "simpanTrans",
     error: function () {
      toastr.error("Gagal");
      message.closeLoading();
     },

     beforeSend: function () {
      message.loadingProses("Proses Simpan...");
     },

     success: function (resp) {
      if (resp.is_valid) {
       toastr.success("Berhasil Disimpan");
       var reload = function () {
        window.location.href = url.base_url(Dashboard.module()) + "detailTrans" + '/' + resp.id;
       };

       setTimeout(reload(), 1000);
      } else {
       toastr.error(resp.message);
      }
      message.closeLoading();
     }
    });
   }
  }
 },

 checkFile: function (elm, type) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    type = 'png';
   }
   if (type_file == 'jpeg') {
    type = 'jpeg';
   }

//   console.log(data_file);
//   return;
   if (type_file == type) {
    var ukuran = 25000000;//per 1000 dianggap 1 MB
    if (data_file.size <= 25000000) {
//     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
     console.log("boleh upload");
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 25 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showFile: function (elm, tipe) {
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text()),
    tipe: tipe
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showFile",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 showDetail: function (elm) {
  var id = $(elm).closest('tr').attr('data_id');
  $.ajax({
   type: 'POST',
   data: {
    room_body: id,
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showDetail",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 minimizeLeft: function (elm, id) {
  console.log(id);

  var class_ul = 'list_' + id;

  if ($(elm).hasClass('fa-angle-down')) {
   $(elm).removeClass('fa-angle-down');
   $(elm).addClass('fa-angle-up');
   $(elm).closest('div.tile').find('ul.' + class_ul).addClass('hidden-content');
   var id_parent = "";
   var left_menu = $('li.left_menu_' + id);

//  console.log("parent",id_parent);
//  return;
   $.each(left_menu, function () {
    var parent = $(this).attr('parent');
    if (parent == id_parent) {
     if ($(this).hasClass('hidden-content')) {
      $(this).removeClass('hidden-content');
     } else {
      $(this).addClass('hidden-content');
     }
    }
   });
  } else {
   console.log("NOT");
   $(elm).removeClass('fa-angle-up');
   $(elm).addClass('fa-angle-down');
   $(elm).closest('div.tile').find('ul.' + class_ul).removeClass('hidden-content');
   var id_parent = "";
   var left_menu = $('li.left_menu_' + id);
   console.log(left_menu);

//  console.log("parent",id_parent);
//  return;
   $.each(left_menu, function () {
    var parent = $(this).attr('parent');
    if (parent == id_parent) {
     if ($(this).hasClass('hidden-content')) {
      $(this).removeClass('hidden-content');
     } else {
      $(this).addClass('hidden-content');
     }
    }
   });
  }
 },

 minimizeBody: function (elm) {
  var room = $(elm).attr('room');
  if ($(elm).hasClass('fa-angle-down')) {
   $(elm).removeClass('fa-angle-down');
   $(elm).addClass('fa-angle-up');
   $('div.mailbox-messages-' + room).addClass('hidden-content');

  } else {
   $(elm).removeClass('fa-angle-up');
   $(elm).addClass('fa-angle-down');
   $('div.mailbox-messages-' + room).removeClass('hidden-content');

  }
 },

 addChildRoom: function (elm) {
  var parent = $(elm).attr('parent');
  var room = $(elm).attr('room');

  $.ajax({
   type: 'POST',
   data: {
    parent: parent,
    room: room,
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "addChildRoom",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 getPostRoomHeaderData: function () {
  var data = {
   'parent': $('input#parent').val(),
   'room': $('input#room').val(),
   'title': $('input#judul').val(),
   'tipe_upload': $('select#tipe_upload').val(),
  };
  return data;
 },

 simpanRoomHeader: function (id) {
  if (validation.run()) {
   var data = Dashboard.getPostRoomHeaderData();

   var formData = new FormData();
   formData.append('data', JSON.stringify(data));
   formData.append("id", id);
   formData.append('file', $('input#file').prop('files')[0]);

   if (validation.run()) {
    $.ajax({
     type: 'POST',
     data: formData,
     dataType: 'json',
     processData: false,
     contentType: false,
     async: false,
     url: url.base_url(Dashboard.module()) + "simpanRoomHeader",
     error: function () {
      toastr.error("Gagal");
      message.closeLoading();
     },

     beforeSend: function () {
      message.loadingProses("Proses Simpan...");
     },

     success: function (resp) {
      if (resp.is_valid) {
       toastr.success("Berhasil Disimpan");
       var reload = function () {
        window.location.href = url.base_url(Dashboard.module()) + "index";
       };

       setTimeout(reload(), 1000);
      } else {
       toastr.error(resp.message);
      }
      message.closeLoading();
     }
    });
   }
  }
 },

 showFileRoomHeader: function (elm) {
  var file = $(elm).attr('file');
  var tipe = $(elm).attr('tipe');
  $.ajax({
   type: 'POST',
   data: {
    file: file,
    tipe: tipe
   },
   dataType: 'html',
   async: false,
   url: "dashboard/showFile",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 downloadBodyDetail: function (elm, id) {
  var file = $(elm).attr('file');
  var nama_file = $(elm).attr('nama_file');

  var req = new XMLHttpRequest();
  req.open("GET", file, true);
  req.responseType = "blob";
  req.onload = function (event) {
   var blob = req.response;
   var fileName = req.getResponseHeader("fileName") //if you have the fileName header available
   var link = document.createElement('a');
   link.href = window.URL.createObjectURL(blob);
   link.download = nama_file;
   link.click();
  };

  req.send();
 },

 like: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    id: id,
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "likeBody",
   error: function () {
    toastr.error('Gagal');
   },

   success: function (resp) {
    $(elm).closest('td').find('label#suka_label').text(resp);
   }
  });
 },

 changeFileUpload: function (elm) {
  $('input#file').removeClass('hidden-content');
  $(elm).closest('div.input-group').addClass('hidden-content');
 },

 showChild: function (elm) {
  var li = $(elm).closest('li');
  var id_parent = li.attr('data_id');
  var left_menu = $('li.content-left-menu');

//  console.log("parent",id_parent);
//  return;
  $.each(left_menu, function () {
   var parent = $(this).attr('parent');
   if (parent == id_parent) {
    if ($(this).hasClass('hidden-content')) {
     $(this).removeClass('hidden-content');
    } else {
     $(this).addClass('hidden-content');
    }
   }
  });
 },

 ubahUrutan: function (elm, parent = "") {
  var room = $(elm).attr('room');
  if (parent == "") {
   window.location.href = url.base_url(Dashboard.module()) + "ubahUrutan" + '/' + room;
  } else {
   window.location.href = url.base_url(Dashboard.module()) + "ubahUrutan" + '/' + room + "/" + parent;
  }
 },

 ubahUrutanRoom: function (elm, parent = "") {
  var data_id = $(elm).attr('data_id');
  window.location.href = url.base_url(Dashboard.module()) + "ubahUrutanRoom" + '/' + data_id;
 },
 
 ubahUrutanBody: function (elm, parent = "") {
  var data_id = $(elm).attr('data_id');
  window.location.href = url.base_url(Dashboard.module()) + "ubahUrutanBody" + '/' + data_id;
 },

 getPostDataSortable: function () {
  var tb_header_room = $('table#tb_header_room').find('tbody').find('tr');
  var data = [];
  var no = 1;
  $.each(tb_header_room, function () {
   var title = $.trim($(this).text());
   if (title == "") {
    $(this).remove();
   }
  });
  $.each(tb_header_room, function () {
   var title = $.trim($(this).text());
   if (title != "") {
    var parent_id = $.trim($(this).attr('parent'));
    var data_id = $.trim($(this).attr('data_id'));
    var row_now = no++;

    var prev_tr = $(this).prev();

    data.push({
     'id': data_id,
     'title': $.trim($(this).find('td:eq(0)').text()),
     'row': row_now
    });
   }
  });

  return data;
 },

 updateHeader: function () {
  var data = Dashboard.getPostDataSortable();

  var room = $('input#room').val();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('room', room);

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Dashboard.module()) + "updateHeader",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Update...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diperbaharui");
     var reload = function () {
      window.location.href = url.base_url(Dashboard.module()) + "ubahUrutan" + '/' + room;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 updateUrutanRoom: function () {
  var data = Dashboard.getPostDataSortable();

  var room = $('input#room').val();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
//  formData.append('room', room);

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Dashboard.module()) + "updateUrutanRoom",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Update...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diperbaharui");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },
 
 updateUrutanBody: function () {
  var data = Dashboard.getPostDataSortable();

  var room_header = $('input#room_header_urut_id').val();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('room_header', room_header);

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Dashboard.module()) + "updateUrutanBody",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Update...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diperbaharui");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 sortableTableEdit: function () {
  $('table#tb_header_room tbody').sortable({
   helper: function (e, ui) {
    ui.children().each(function () {
     $(this).width($(this).width());
    });
    return ui;
   },
   cursor: 'move',
   update: function (event, ui) {
//   var data = $(this).sortable('serialize');
//   
//   $.ajax({
//    data: data,
//    type: 'POST',
//    url: 'URL_HERE'
//   });
   }
  }).disableSelection();
 },

 addField: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(1)').html('<i class="fa fa-minus fa-lg hover" onclick="Dashboard.removeField(this)"></i>');
  tr.after(newTr);
 },

 removeField: function (elm) {
  $(elm).closest('tr').remove();
 },

 getPostTable: function () {
  var data = [];
  var tb_field = $('table#tb_field').find('tbody').find('tr');
  $.each(tb_field, function () {
   var field = $(this).find('input').val();
   if (field != "") {
    data.push({
     'field': field
    });
   }
  });

  return {
   'room_body': $('input#room_body').val(),
   'nama_table': $('input#nama_tabel').val(),
   'field': data
  };
 },

 simpanTable: function (id) {
  var data = Dashboard.getPostTable();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Dashboard.module()) + "simpanTable",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Dashboard.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 gotoLinkDoc: function (elm) {
  var link = $(elm).attr('link');
  if (link != '') {
   window.open(link);
  }
 },

 commentDocument: function (elm, room_body) {
  var title = $(elm).attr('title_body');
  $.ajax({
   type: 'POST',
   data: {
    room_body: room_body,
    title: title
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "showFormComment",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 simpanComment: function (elm) {
  var room_body = $('input#room_body').val();
  if (validation.run()) {
   var data = {
    room_body: room_body,
    judul: $('input#judul').val(),
    komentar: $('textarea#komentar').val()
   };

   $.ajax({
    type: 'POST',
    data: data,
    dataType: 'json',
    async: false,
    url: "dashboard/simpanComment",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 addPermissionBody: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    room_body: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "addPermissionBody",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();

    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 addPermissionHeader: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    room_header: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "addPermissionHeader",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();

    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 addPermissionRoom: function (elm, id) {
  $.ajax({
   type: 'POST',
   data: {
    room: id
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Dashboard.module()) + "addPermissionRoom",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();

    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 searchInTablePegawai: function (elm, e) {
  Dashboard.searchInTable($(elm).val(), '#tb_pegawai');
 },

 searchInTable: function (value, elm) {
  $("" + elm + " tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 getPostDataCheck: function () {
  var data = [];
  var tr = $('table#tb_pegawai').find('tbody').find('tr');

  $.each(tr, function () {
   var user_id = $(this).attr('user_id');
   var check_view = $(this).find('input#view_check').is(':checked');
   var check_down = $(this).find('input#download_check').is(':checked');

   var permission_id = $(this).attr('permission_id');
   if (check_view || check_down || permission_id != "") {
    data.push({
     'user_id': user_id,
     'check_view': check_view ? 1 : 0,
     'check_down': check_down ? 1 : 0,
     'permission_id': permission_id
    });
   }
  });

  return data;
 },

 savePermissionUser: function (elm) {
  var data = Dashboard.getPostDataCheck();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('room_body', $('input#room_body_id').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Dashboard.module()) + "savePermissionUser",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 savePermissionHeaderUser: function (elm) {
  var data = Dashboard.getPostDataCheck();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('room_header', $('input#room_header_id').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Dashboard.module()) + "savePermissionHeaderUser",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 savePermissionRoomUser: function (elm) {
  var data = Dashboard.getPostDataCheck();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('room', $('input#room_id').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   processData: false,
   contentType: false,
   async: false,
   url: url.base_url(Dashboard.module()) + "savePermissionRoomUser",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Simpan...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disimpan");
    }
    message.closeLoading();
   }
  });
 },

 checkAllView: function (elm) {
  var check = $(elm).is(':checked');
  $('input.view_check').prop('checked', check);
 },

 checkDownView: function (elm) {
  var check = $(elm).is(':checked');
  $('input.download_check').prop('checked', check);
 },

 checkedView: function (elm) {
  var check = $(elm).is(':checked');
  var total = $('input.view_check').length;

  var checked = 0;
  $.each($('input.view_check'), function () {
   if ($(this).is(':checked')) {
    checked += 1;
   }
  });

  if (checked == total) {
   $('input#head_view_check').prop('checked', true);
  } else {
   $('input#head_view_check').prop('checked', false);
  }
 },

 checkedDown: function (elm) {
  var check = $(elm).is(':checked');
  var total = $('input.download_check').length;

  var checked = 0;
  $.each($('input.download_check'), function () {
   if ($(this).is(':checked')) {
    checked += 1;
   }
  });

  if (checked == total) {
   $('input#head_down_check').prop('checked', true);
  } else {
   $('input#head_down_check').prop('checked', false);
  }
 }
};


$(function () {
 Dashboard.sortableTableEdit();
});
