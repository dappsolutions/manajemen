<?php

class Rekapuser extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'rekapuser';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/rekapuser.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai';
 }

 public function getRootModule() {
  return "Laporan";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Rekapuser";
  $data['title_content'] = 'Rekapuser';
  $content = $this->getDataRekapuser();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRekapuser($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('j.jabatan', $keyword),
       array('p.nama', $keyword),
       array('p.nip', $keyword),
       array('u.nama', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'Superadmin') {
   $where = "p.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama as nama_ultg', 
                    'j.jabatan', 'su.sub_ultg as sub_unit', 'j.nama_panjang'),
                'like' => $like,
                'join' => array(
                    array('ultg u', 'u.id = p.ultg'),
                    array('sub_ultg su', 'p.sub_ultg = su.id', 'left'),
                    array('jabatan j', 'j.id = p.jabatan'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama as nama_ultg', 
                    'j.jabatan', 'su.sub_ultg as sub_unit', 'j.nama_panjang'),
                'like' => $like,
                'join' => array(
                    array('ultg u', 'u.id = p.ultg'),
                    array('sub_ultg su', 'p.sub_ultg = su.id', 'left'),
                    array('jabatan j', 'j.id = p.jabatan'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataRekapuser($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('j.jabatan', $keyword),
       array('p.nama', $keyword),
       array('p.nip', $keyword),
       array('u.nama', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "p.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama as nama_ultg', 
                    'j.jabatan', 'su.sub_ultg as sub_unit', 'j.nama_panjang'),
                'like' => $like,
                'join' => array(
                    array('ultg u', 'u.id = p.ultg'),
                    array('sub_ultg su', 'p.sub_ultg = su.id', 'left'),
                    array('jabatan j', 'j.id = p.jabatan'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'u.nama as nama_ultg', 
                    'j.jabatan', 'su.sub_ultg as sub_unit', 'j.nama_panjang'),
                'like' => $like,
                'join' => array(
                    array('ultg u', 'u.id = p.ultg'),
                    array('sub_ultg su', 'p.sub_ultg = su.id', 'left'),
                    array('jabatan j', 'j.id = p.jabatan'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataRekapuser($keyword)
  );
 }

 public function getDetailDataRekapuser($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'u.nama as nama_ultg', 
                  'j.jabatan', 'j.id as jabatan_id', 
                  'j.nama_panjang', 'su.sub_ultg as sub_unit'),
              'join' => array(
                  array('ultg u', 'u.id = p.ultg'),
                  array('sub_ultg su', 'p.sub_ultg = su.id', 'left'),
                  array('jabatan j', 'j.id = p.jabatan'),
              ),
              'where' => "p.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListJabatan() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListUltg() {
  $data = Modules::run('database/get', array(
              'table' => 'ultg',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getListSubUltg() {
  $data = Modules::run('database/get', array(
              'table' => 'sub_ultg',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Rekapuser";
  $data['title_content'] = 'Tambah Rekapuser';
  $data['list_ultg'] = $this->getListUltg();
  $data['list_subultg'] = $this->getListSubUltg();
  $data['list_jabatan'] = $this->getListJabatan();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataRekapuser($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Rekapuser";
  $data['title_content'] = 'Ubah Rekapuser';
  $data['list_ultg'] = $this->getListUltg();
  $data['list_subultg'] = $this->getListSubUltg();
  $data['list_jabatan'] = $this->getListJabatan();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataRekapuser($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Rekapuser";
  $data['title_content'] = "Detail Rekapuser";
  echo Modules::run('template', $data);
 }

 public function getPostDataRekapuser($value) {
  $data['jabatan'] = $value->jabatan;
  $data['nip'] = $value->nip;
  $data['email'] = $value->email;
  $data['nama'] = $value->nama;
  $data['ultg'] = $value->ultg;
  $data['sub_ultg'] = $value->sub_ultg;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataRekapuser($data->pegawai);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Rekapuser";
  $data['title_content'] = 'Data Rekapuser';
  $content = $this->getDataRekapuser($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function getMenuRoomPermission($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'room_permission rp',
              'field'=> array('rp.*', 'r.nama'),
              'join' => array(
                  array('user u', 'rp.user = u.id'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('room r', 'r.id = rp.room'),
              ),
              'where' => "rp.deleted = 0 and pg.id = '".$pegawai."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getMenuRoomHeaderPermission($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'room_header_permission rp',
              'field'=> array('rp.*', 'r.title'),
              'join' => array(
                  array('user u', 'rp.user = u.id'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('room_header r', 'r.id = rp.room_header'),
              ),
              'where' => "rp.deleted = 0 and pg.id = '".$pegawai."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['nama'] = $value['title'];
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getMenuRoomBodyPermission($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_permission rp',
              'field'=> array('rp.*', 'r.title'),
              'join' => array(
                  array('user u', 'rp.user = u.id'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('room_body r', 'r.id = rp.room_body'),
              ),
              'where' => "rp.deleted = 0 and pg.id = '".$pegawai."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['nama'] = $value['title'];
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function showPermission() {
  $pegawai = $_POST['pegawai'];
  $room_permission = $this->getMenuRoomPermission($pegawai);  
  $room_header = $this->getMenuRoomHeaderPermission($pegawai);  
  $room_body = $this->getMenuRoomBodyPermission($pegawai);  
//  echo '<pre>';
//  print_r($room_body);die;
  $data['list_menu'] = array(
      'room'=> $room_permission,
      'room_header'=> $room_header,
      'room_body'=> $room_body,
  );
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('laporan_rekap_permission', $data, true);
 }
}
