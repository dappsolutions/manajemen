<div class="row">
 <div class="col-md-12">
  <div class="form-group">
   <div class="app-search">
    <input class="form-control" type="search" onkeyup="Rekapuser.searchInTablePermission(this, event)" id="keyword" placeholder="Pencarian">
    <button class="app-search__button"><i class="fa fa-search"></i></button>
   </div>
  </div>
 </div>
</div>  
<hr/>
<br/>

<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <div class="" style="height: 300px;overflow: auto;">
    <table class="table table-bordered" id="tb_permission">
     <thead>
      <tr class="table-warning">
       <th class="text-center">No</th>
       <th>Menu</th>
       <th class="text-center">View</th>
       <th class="text-center">Download</th>
      </tr>
     </thead>
     <tbody>
      <?php if (!empty($list_menu)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($list_menu['room'] as $v_r) { ?>
        <tr class="">
         <td  class="text-center"><?php echo $no++ ?></td>
         <td><?php echo $v_r['nama'] ?></td>
         <td class="text-center">
          <?php $checked = $v_r['view'] == 1 ? 'checked' : '' ?>
          <input disabled="" type="checkbox" class="" <?php echo $checked ?>/>
         </td>
         <td class="text-center">
          <?php $checked = $v_r['download'] == 1 ? 'checked' : '' ?>
          <input disabled="" type="checkbox" class="" <?php echo $checked ?>/>
         </td>
        </tr>
       <?php } ?>
       <?php foreach ($list_menu['room_header'] as $v_h) { ?>
        <tr class="">
         <td  class="text-center"><?php echo $no++ ?></td>
         <td><?php echo $v_h['nama'] ?></td>
         <td class="text-center">
          <?php $checked = $v_h['view'] == 1 ? 'checked' : '' ?>
          <input disabled="" type="checkbox" class="" <?php echo $checked ?>/>
         </td>
         <td class="text-center">
          <?php $checked = $v_h['download'] == 1 ? 'checked' : '' ?>
          <input disabled="" type="checkbox" class="" <?php echo $checked ?>/>
         </td>
        </tr>
       <?php } ?>
       <?php foreach ($list_menu['room_body'] as $v_b) { ?>
        <tr class="">
         <td  class="text-center"><?php echo $no++ ?></td>
         <td><?php echo $v_b['nama'] ?></td>
         <td class="text-center">
          <?php $checked = $v_b['view'] == 1 ? 'checked' : '' ?>
          <input disabled="" type="checkbox" class="" <?php echo $checked ?>/>
         </td>
         <td class="text-center">
          <?php $checked = $v_b['download'] == 1 ? 'checked' : '' ?>
          <input disabled="" type="checkbox" class="" <?php echo $checked ?>/>
         </td>
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr class="text-center">
        <td colspan="8">Tidak Ada Data Ditemukan</td>
       </tr>
      <?php } ?>
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>