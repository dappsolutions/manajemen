<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-9">
  <div class="row">
   <!--   <div class="col-md-3">
       <div class="tile">
        <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;<?php echo $title_content ?></h5>
       </div>
      </div>-->
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Form Data' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <div class="row">
       <div class="col-md-3 ">
        <b>Nama Tabel</b>
       </div> 
       <div class="text-success">
        <?php echo $nama ?>
       </div>        
      </div>
      <hr/>

      <div class="row">
       <div class="col-md-3 ">
        <b>Daftar Kolom</b>
       </div>        
      </div>
      <br/>

      <div class="row">
       <div class="col-md-6">
        <table class="table table-bordered" id="tb_field">
         <thead>
          <tr class="table-warning">
           <th>Nama Kolom</th>
          </tr>
         </thead>
         <tbody>
          <?php if ($list_kolom) { ?>
           <?php foreach ($list_kolom as $value) { ?>
            <tr  data_id="<?php echo $value['id'] ?>">
             <td>
              <?php echo $value['nama_field'] ?>
             </td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Basedata.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>

 <div class="col-md-3">
  <div class="tile">
   <div class="tile-body">
    <div class="row">
     <div class="col-md-12">
      <select class="form-control required" id="pegawai" error="Pegawai">
       <option value="">Pilih Pegawai</option>
       <?php if (!empty($list_pegawai)) { ?>
        <?php foreach ($list_pegawai as $value) { ?>
         <?php $selected = '' ?>
         <option <?php echo $selected ?> value="<?php echo $value['user_id'] ?>"><?php echo $value['nip'] . ' -' . $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>            
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-12 text-right">
      <button class="btn btn-warning" onclick="Basedata.simpanPegawai(this)">Tambah</button>
     </div>
    </div>
   </div>
  </div>

  <div class="tile" style="margin-top: -16px;">
   Entri Pegawai
   <hr/>
   <div class="tile-body">
    <table class="table table-bordered" id="tb_pegawai">
     <thead>
      <tr class="table-warning">
       <th>Nama Pegawai</th>
       <th class="text-center">Action</th>
      </tr>
     </thead>
     <tbody>
      <?php if ($list_entriuser) { ?>
       <?php foreach ($list_entriuser as $value) { ?>
        <tr  data_id="<?php echo $value['id'] ?>">
         <td>
          <?php echo $value['nama_pegawai'] ?>
         </td>         
         <td class="text-center">
          <i class="fa fa-trash hover fa-lg" onclick="Basedata.deletePegawai('<?php echo $value['id'] ?>')"></i>
         </td>         
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td colspan="3" class="text-center">Tidak ada data ditemukan</td>
       </tr>
      <?php } ?>
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>
