<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type="hidden" value="<?php echo isset($table_id) ? $table_id : '' ?>" id="table_id" class="form-control" />

<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Form Input' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <?php if (!empty($form)) { ?>
        <?php foreach ($form as $value) { ?>
         <div class="form-group form_input">
          <label class="control-label col-sm-1"><?php echo ucfirst($value['nama_field']) ?></label>
          <div class="col-sm-5">
           <input field="<?php echo $value['room_body_table_has_field'] ?>" class="form-control required" error="Basedata" id="jabatan" type="text" placeholder="Masukkan <?php echo ucfirst($value['nama_field']) ?>" value="<?php echo $value['isi'] ?>">
          </div>        
         </div>
        <?php } ?>
       <?php } ?>       
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="Basedata.simpanValue('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Basedata.back('<?php echo isset($id) ? $id : '' ?>')"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
