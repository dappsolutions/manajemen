<?php

class Basedata extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $jabatan_id;
 public $struktur_id;
 public $pegawai_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->jabatan_id = $this->session->userdata('jabatan_id');
  $this->struktur_id = $this->session->userdata('struktur_id');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'basedata';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<link href="' . base_url() . 'assets/css/jquery-ui.min.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/jquery-ui.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/basedata.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'room_body_table';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " -  table";
  $data['title_content'] = ' table';
  $content = $this->getDataBasedata();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getJabatanHasChild() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.parent = '" . $this->struktur_id . "'"
  ));

  $is_has = false;
  if (!empty($data)) {
   $is_has = true;
  } else {
   if ($this->akses == 'superadmin') {
    $is_has = true;
   }
  }

  return $is_has;
 }

 public function getTotalDataBasedata($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('tb.nama', $keyword),
   );
  }

  $where = "tb.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "tb.deleted = 0";
  } else {
   $where = "tb.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' tb',
                'field' => array('tb.*'),
                'like' => $like,
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' tb',
                'field' => array('tb.*'),
                'like' => $like,
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataBasedata($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('tb.nama', $keyword),
   );
  }

  $where = "tb.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "tb.deleted = 0";
  } else {
   $where = "tb.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' tb',
                'field' => array('tb.*'),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' tb',
                'field' => array('tb.*'),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataBasedata($keyword)
  );
 }

 public function getDetailDataBasedata($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pjd',
              'field' => array('pjd.*', 'p.nama as nama_pegawai',
                  'p.nip', 'pg.nama as nama_pemberi',
                  'jds.status', 'jd.jenis', 'ut.id as user_pegawai'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user ut', 'ut.pegawai = p.id'),
                  array('user u', 'pjd.giver = u.id', 'left'),
                  array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                  array('jabatan j', 'pg.jabatan = j.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
                  array('jenis_job jd', 'pjd.jenis_job = jd.id'),
              ),
              'where' => "pjd.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getDetailTable($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => "deleted = 0 and id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKolomTable($table) {
  $data = Modules::run('database/get', array(
              'table' => "	room_body_table_has_field",
              'where' => "deleted = 0 and room_body_table = '" . $table . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['room_body_table_has_field'] = $value['id'];
    $value['isi'] = "";
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => "pegawai pg",
              'field' => array('pg.*', 'u.id as user_id'),
              'join' => array(
                  array('user u', 'u.pegawai = pg.id')
              ),
              'where' => "pg.deleted = 0",
              "limit" => 2000
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function ubah($id) {
  $data = $this->getDetailTable($id);

  $data['view_file'] = 'form_add_edit';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Data";
  $data['title_content'] = 'Ubah Data';
  $data['list_kolom'] = $this->getListKolomTable($data['id']);
  echo Modules::run('template', $data);
 }

 public function getListEntriUser($table) {
  $data = Modules::run('database/get', array(
              'table' => 'room_table_permission rtp',
              'field' => array('rtp.*', 'pg.nama as nama_pegawai'),
              'join' => array(
                  array('user u', 'rtp.user = u.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "rtp.deleted = 0 and rtp.room_body_table = '" . $table . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailTable($id);

  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Data";
  $data['title_content'] = 'Ubah Data';
  $data['list_kolom'] = $this->getListKolomTable($data['id']);
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_entriuser'] = $this->getListEntriUser($data['id']);
  echo Modules::run('template', $data);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Basedata";
  $data['title_content'] = 'Data Basedata';
  $content = $this->getDataBasedata($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function simpanTable() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
  $id = $data->id;

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_tb['nama'] = $data->nama_table;

   $table = $id;
   if ($id == "") {
    $table = Modules::run('database/_insert', "room_body_table", $post_tb);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post_tb, array('id' => $id));
   }

   if (!empty($data->field)) {
    foreach ($data->field as $value) {
     $post_field['room_body_table'] = $table;
     $post_field['nama_field'] = $value->field;
     if ($value->id == "") {
      Modules::run('database/_insert', 'room_body_table_has_field', $post_field);
     } else {
      if ($value->deleted == 1) {
       $post_field['deleted'] = 1;
      }

      Modules::run('database/_update', "room_body_table_has_field", $post_field,
              array('id' => $value->id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function simpanPegawai() {
  $table = $this->input->post('table');
  $user = $this->input->post('user_id');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_tb['room_body_table'] = $table;
   $post_tb['user'] = $user;
   $data = Modules::run('database/get', array(
               'table' => 'room_table_permission',
               'where' => "deleted = 0 and user = '" . $user . "' "
               . "and room_body_table = '" . $table . "'"
   ));
   if (empty($data)) {
    Modules::run('database/_insert', "room_table_permission", $post_tb);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function deletePegawai($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', "room_table_permission", array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteData($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', "room_body_row_uniq", array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getFieldData($id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_table_has_field rf',
              'field' => array('rf.*'),
              'where' => "rf.deleted = 0 and rf.room_body_table = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getFieldValueData($id, $keyword = "") {
  $like = array();
  if ($keyword != "") {
   $like = array(
       array('rfv.isi', $keyword)
   );
   $data = Modules::run('database/get', array(
               'table' => 'room_body_row_uniq rfq',
               'field' => array('rfq.*', 'rfv.isi'),
               'join' => array(
                   array('room_body_field_value rfv', 'rfv.room_body_row_uniq = rfq.id'),
               ),
               'is_or_like' => true,
               'like' => $like,
               'inside_brackets' => true,
               'where' => "rfq.deleted = 0 "
               . "and rfq.room_body_table = '" . $id . "'",
               'orderby' => 'rfq.row'
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'room_body_row_uniq rfq',
               'field' => array('rfq.*', 'rfv.isi'),
               'join' => array(
                   array('room_body_field_value rfv', 'rfv.room_body_row_uniq = rfq.id'),
               ),
               'where' => "rfq.deleted = 0 "
               . "and rfq.room_body_table = '" . $id . "'",
               'orderby' => 'rfq.row'
   ));
  }

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   $temp = array();
   foreach ($data->result_array() as $value) {
    if (!in_array($value['id'], $temp)) {
     $detail = array();
     foreach ($data->result_array() as $v_d) {
      if ($v_d['id'] == $value['id']) {
       array_push($detail, $v_d);
      }
     }

//     echo '<pre>';
//       print_r($detail);die;
     $result[$value['id']] = $detail;

     $temp[] = $value['id'];
    }
   }
  }


  return $result;
 }

 public function data_input($id) {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $table_data = $this->getDetailTable($id);
  $data['table_name'] = $table_data['nama'];
  $data['view_file'] = 'data_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['title_content'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['kolom'] = $this->getFieldData($id);
  $data['value_data'] = $this->getFieldValueData($id);
//  echo '<pre>';
//  print_r($data['value_data']);die;
  $data['table_id'] = $id;
  $data['has_access'] = $this->hasAccessTable($id);

  echo Modules::run('template', $data);
 }

 public function showDaftarData() {
  $id = $_POST['id'];
  $table_data = $this->getDetailTable($id);
  $data['table_name'] = $table_data['nama'];
  $data['kolom'] = $this->getFieldData($id);
  $data['value_data'] = $this->getFieldValueData($id);
  $data['table_id'] = $id;
  $data['has_access'] = $this->hasAccessTable($id);

  $view = $this->load->view('daftar_data_input', $data, true);
  echo $view;
 }

 public function searchData($id, $keyword) {

  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $table_data = $this->getDetailTable($id);
  $data['table_name'] = $table_data['nama'];
  $data['view_file'] = 'data_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['title_content'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['kolom'] = $this->getFieldData($id);
  $data['value_data'] = $this->getFieldValueData($id, $keyword);
//  echo '<pre>';
//  print_r($data['value_data']);die;
  $data['table_id'] = $id;
  $data['has_access'] = $this->hasAccessTable($id);

  echo Modules::run('template', $data);
 }

 public function hasAccessTable($id) {
  $user = $this->session->userdata('user_id');
  $data = Modules::run('database/get', array(
              'table' => 'room_table_permission rtp',
              'where' => "rtp.deleted = 0 and rtp.user = '" . $user . "' "
              . "and rtp.room_body_table = '" . $id . "'"
  ));

  $is_has = 0;
  if (!empty($data)) {
   $is_has = 1;
  }


  return $is_has;
 }

 public function getDetailTableRow($id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_row_uniq ru',
              'field' => array('ru.*'),
              'where' => "ru.deleted = 0 and ru.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKolomTableValue($row_id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_field_value rv',
              'field' => array('rv.*', 'rb.nama_field'),
              'join' => array(
                  array('room_body_table_has_field rb', 'rb.id = rv.room_body_table_has_field')
              ),
              'where' => "rv.deleted = 0 and rv.room_body_row_uniq = '" . $row_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addData($table_id) {
  $data['view_file'] = 'form_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Data";
  $data['title_content'] = 'Tambah Data';
  $data['form'] = $this->getListKolomTable($table_id);
//  echo '<pre>';
//  print_r($data['form']);die;
  $data['table_id'] = $table_id;
  echo Modules::run('template', $data);
 }

 public function ubahData($id) {
  $table_data = $this->getDetailTableRow($id);

  $data['view_file'] = 'form_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Data";
  $data['title_content'] = 'Ubah Data';
  $data['form'] = $this->getListKolomTableValue($id);
  $data['id'] = $id;
//  echo '<pre>';
//  print_r($data['form']);die;
  echo Modules::run('template', $data);
 }

 public function detailData($id) {
  $table_data = $this->getDetailTableRow($id);

  $data['view_file'] = 'detail_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Data";
  $data['title_content'] = 'Detail Data';
  $data['form'] = $this->getListKolomTableValue($id);
  $data['id'] = $id;
  $data['table_id'] = $table_data['room_body_table'];
//  echo '<pre>';
//  print_r($data['form']);die;
  echo Modules::run('template', $data);
 }

 public function simpanValue() {
  $id = $this->input->post('id');
  $room_body_table = $this->input->post('table_id');
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if ($id == '') {
    if (!empty($data)) {
     $no_doc = Modules::run('no_generator/generateNoRow');
     $post_row['no_document'] = $no_doc;
     $post_row['room_body_table'] = $room_body_table;
     $post_row['user'] = $this->session->userdata('user_id');
     $id = Modules::run('database/_insert', 'room_body_row_uniq', $post_row);

     foreach ($data as $value) {
      $post_td['room_body_row_uniq'] = $id;
      $post_td['room_body_table_has_field'] = $value->field_id;
      $post_td['isi'] = $value->isi;
      Modules::run('database/_insert', 'room_body_field_value', $post_td);
     }
    }
   } else {
    //update
    if (!empty($data)) {
     foreach ($data as $value) {
      $post_data['isi'] = $value->isi;
      Modules::run('database/_update', 'room_body_field_value', $post_data,
              array(
                  'room_body_row_uniq' => $id,
                  'room_body_table_has_field' => $value->field_id
              )
      );
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function createTable() {
  $data['view_file'] = 'create_table';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Create Table ";
  $data['title_content'] = 'Create Table ';
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true),
           array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataKolom($row_id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_row_uniq rbr',
              'field' => array('rbth.*'),
              'join' => array(
                  array('room_body_table rbt', 'rbt.id = rbr.room_body_table'),
                  array('room_body_table_has_field rbth', 'rbth.room_body_table = rbr.room_body_table'),
              ),
              'where' => "rbr.deleted = 0 and rbr.id = '" . $row_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addDatarow() {
  $row_id = $_POST['row_id'];
  $data['no'] = $_POST['no'];
  $data['data_kolom'] = $this->getDataKolom($row_id);

  echo $this->load->view('add_row_content', $data, true);
 }

 public function simpanRow() {
  $data = json_decode($_POST['data']);
  $room_body_table = $_POST['table_id'];
//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;
  $this->db->trans_begin();
  try {
   foreach ($data as $value) {
    $no_doc = Modules::run('no_generator/generateNoRow');
    $post_row['no_document'] = $no_doc;
    $post_row['room_body_table'] = $room_body_table;
    $post_row['user'] = $this->session->userdata('user_id');
    $id = Modules::run('database/_insert', 'room_body_row_uniq', $post_row);

    foreach ($value->field as $v_col) {
     $post_td['room_body_row_uniq'] = $id;
     $post_td['room_body_table_has_field'] = $v_col->field;
     $post_td['isi'] = $v_col->isi;
     Modules::run('database/_insert', 'room_body_field_value', $post_td);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function updateUrutan() {
  $data = json_decode($_POST['data']);

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $post['row'] = $value->urutan;
     Modules::run('database/_update', 'room_body_row_uniq', $post,
             array('id' => $value->row_id));
     Modules::run('database/_update', 'room_body_field_value', $post,
             array('room_body_row_uniq' => $value->row_id));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
