<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type='hidden' name='' id='jabatan_header' class='form-control' value='<?php echo isset($jabatan_header) ? $jabatan_header : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucfirst($title_content) ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-1">Atasan</label>
        <div class="col-sm-5">
         <select class="form-control required select2" id="atasan" error="Atasan">
          <option value="">Pilih Pegawai</option>
          <?php if (!empty($list_pegawai)) { ?>
           <?php foreach ($list_pegawai as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($pegawai)) { ?>
             <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] . ' - ' . $value['nama_jabatan'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-1">Bawahan</label>
        <div class="col-sm-12">
         <table class="table table-bordered" id="tb_bawahan">
          <thead>
           <tr>
            <th>Bawahan</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td>
             <select class="form-control bawahan" no="1" id="bawahan-1" error="Bawahan">
              <option value="">Pilih Bawahan</option>
              <option value="tidak_ada">Tidak Punya</option>
              <?php if (!empty($list_pegawai)) { ?>
               <?php foreach ($list_pegawai as $value) { ?>
                <?php $selected = '' ?>
                <?php if (isset($pegawai)) { ?>
                 <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
                <?php } ?>
                <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] . ' - ' . $value['nama_jabatan'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td class="text-center">
             <i class="fa fa-plus-circle fa-2x text-primary hover" onclick="Strukturorg.addDetail(this)"></i>
            </td>
           </tr>
          </tbody>
         </table>

        </div>        
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="Strukturorg.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Strukturorg.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
