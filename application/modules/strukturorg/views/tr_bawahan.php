<tr>
 <td>
  <select class="form-control bawahan" no="<?php echo $no ?>" id="bawahan-<?php echo $no ?>" error="Bawahan">
   <option value="">Pilih Bawahan</option>
   <option value="tidak_ada">Tidak Punya</option>
   <?php if (!empty($list_pegawai)) { ?>
    <?php foreach ($list_pegawai as $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($pegawai)) { ?>
      <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] . ' - ' . $value['nama_jabatan'] ?></option>
    <?php } ?>
   <?php } ?>
  </select>
 </td>
 <td class="text-center">
  <i class="fa fa-minus fa-2x text-danger hover" onclick="Strukturorg.removeDetail(this)"></i>
 </td>
</tr>

<script>
 $(function () {
  $('select#bawahan-<?php echo $no ?>').select2();
 });
</script>