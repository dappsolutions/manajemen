
<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Struktur Organisasi' ?>
     </div>
    </div>
    <div class="tile" style="height: 300px;overflow: auto;">
     <?php if (!empty($jabatan_header)) { ?>
      <?php foreach ($jabatan_header as $value) { ?>
       <i data-toggle="tooltip" title="Lihat Detail Bagan Orgasasi <?php echo ucfirst($value['title']) ?>" class="fa fa-file-text-o hover" 
          header_id="<?php echo $value['id'] ?>" 
          title_head="<?php echo ucfirst($value['title']) ?>"
          onclick="Strukturorg.getDetailOrg(this, '<?php echo $value['id'] ?>')"></i>
       &nbsp;&nbsp;<?php echo ucfirst($value['title']) ?>
       <div class="text-right">
        <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
         <div class="text-right">
          <i class="fa fa-pencil fa-lg hover-content" onclick="Strukturorg.addJabatanHeader('<?php echo $value['title'] ?>', '<?php echo $value['id'] ?>')"></i>
          &nbsp;
          <i class="fa fa-trash-o fa-lg hover-content" onclick="Strukturorg.deleteJabatanHeader('<?php echo $value['id'] ?>')"></i>
         </div>
        <?php } ?>
       </div>
      <?php } ?>
     <?php } ?>       
     <hr/>
     <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
      <div class="text-right">
       <i class="fa fa-plus fa-lg hover-content" onclick="Strukturorg.addJabatanHeader()"></i>
      </div>
     <?php } ?>
    </div>
   </div>
  </div>
  <div class="row">   
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Bagan Organisasi' ?>
      <?php if (!empty($jabatan_header)) { ?>
       <label id="title_bagan"><?php echo $jabatan_header[0]['title'] ?></label>
      <?php } ?>
     </div>
    </div>
    <div class="tile">      
     <div class="tile-body">
      <div class="row">
       <!--<input type="text" value="" id="ket_header" class="form-control" />-->
       <div class="col-md-12">
        <div class="chart" id="tree"></div>
       </div>
      </div>
     </div>
     <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
      <div class="tile-footer text-right">
       <?php if (!empty($jabatan_header)) { ?>
        <i id="btn_add_org" jabatan_header="<?php echo $jabatan_header[0]['id'] ?>" 
           class="fa fa-plus-circle fa-lg hover" onclick="Strukturorg.add(this)"></i>
       <?php } ?>
      </div>
     <?php } ?>
    </div> 
   </div>
  </div>
 </div>
</div>