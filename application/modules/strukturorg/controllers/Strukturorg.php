<?php

class Strukturorg extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'strukturorg';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/orgchart.css">',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
//      '<script src="' . base_url() . 'assets/js/raphael.js"></script>',
//      '<script src="' . base_url() . 'assets/js/Treant.js"></script>',
      '<script src="' . base_url() . 'assets/js/orgchart.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/strukturorg.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'jabatan_struktur';
 }

 public function getRootModule() {
  return "organisasi";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Strukturorg";
  $data['title_content'] = 'Jabatan Struktur';
  $jabatan_header = $this->getJabatanHeader();
  $data['jabatan_header'] = $jabatan_header;
  echo Modules::run('template', $data);
 }

 public function getJabatanHeader() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_header',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getHeaderCurrent() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_header',
              'where' => "deleted = 0"
  ));

  return $data->row_array();
 }

 public function getDataJson() {
  $header = $this->input->post('header');
  $where = "";
  if ($header != '') {
   $where = "js.jabatan_header = '" . $header . "'";
  } else {
   $header_current = $this->getHeaderCurrent();
   $where = "js.jabatan_header = '" . $header_current['id'] . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'field' => array('js.*', 'j.jabatan as nama_jabatan',
                  'p.nama as nama_pegawai', 'p.nip'),
              'join' => array(
                  array('pegawai p', 'p.id = js.pegawai'),
                  array('jabatan j', 'p.jabatan = j.id'),
                  array('jabatan_header jh', 'js.jabatan_header = jh.id'),
              ),
              'where' => $where
  ));

  $json = json_encode(array());
  if (!empty($data)) {
   $data_tree = $this->buildTree($data->result_array());
   $json = json_encode($data_tree);
  }

  echo $json;
 }

 function buildTree(array $elements, $parentId = 0) {
  $branch = array();

  foreach ($elements as $element) {
   $element['label'] = $element['nama_jabatan'];
   $element['name'] = $element['nama_jabatan'];
   if ($element['parent'] == $parentId) {
    $children = $this->buildTree($elements, $element['id']);
    if ($children) {
//              print_r($children);die;
     $element['children'] = $children;
    }
    $id = $element['id'];
    $onclick = "onclick='Strukturorg.deletePop(this)'";
//    $onclick_edit = "onclick='Strukturorg.editPop(this)'";
    $html = "<div data-toggle='tooltip' title='".$element['nip'].",\n " . $element['nama_pegawai'] . ",\n " . $element['nama_jabatan'] . "' class='row'>";
    $html .= "<div class='col-md-12 text-center'>";
    $html .= "<label style='font-size:12px;'>" . substr($element['nama_pegawai'], 0, 5) . '<label>';
    $html .= "</div>";
    $html .= "<div class='col-md-12 text-center'>";
    $html .= "<label style='font-size:12px;font-weight:bold;'>" . $element['nama_jabatan'] . '<label>';
    $html .= "</div>";
    $html .= "<div class='col-md-12 text-right'>";
//    $html .= '<i id_data = "'.$id.'" class="fa fa-pencil text-primary hover" '.$onclick_edit.'></i>';
    $html .= "&nbsp;&nbsp;<i id_data='" . $id . "' class='fa fa-close text-danger hover' " . $onclick . "></i>";
    $html .= "</div>";
    $html .= "</div>";
    $element['label'] = $html;
    $element['name'] = $element['nama_jabatan'];
    $branch[] = $element;
   }
  }

  return $branch;
 }

 public function getDetailDataStrukturorg($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListJabatan() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDetailHeader($id) {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_header',
              'where' => "id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'p.jabatan = j.id')
              ),
              'where' => "p.deleted = 0",
              'limit' => 2000
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add($jabatan_header) {
  $detail_header = $this->getDetailHeader($jabatan_header);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah " . ucfirst($detail_header['title']);
  $data['title_content'] = 'Tambah ' . ucfirst($detail_header['title']);
  $data['list_pegawai'] = $this->getListPegawai();
  $data['jabatan_header'] = $detail_header['id'];
  echo Modules::run('template', $data);
 }

 public function editPop($id) {
  $data = $this->getDetailDataStrukturorg($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah";
  $data['title_content'] = 'Ubah';
  echo $this->load->view('form_pop');
 }

 public function detail($id) {
  $data = $this->getDetailDataStrukturorg($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Strukturorg";
  $data['title_content'] = "Detail Strukturorg";
  echo Modules::run('template', $data);
 }

 public function getIdAtasan($pegawai, $header) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => "pegawai = '" . $pegawai . "' and deleted = 0 and jabatan_header = '" . $header . "'"
  ));

  $id_atasan = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $id_atasan = $data['id'];
  } else {
   $id_atasan = Modules::run('database/_insert', $this->getTableName(), array(
               'pegawai' => $pegawai,
               'jabatan_header' => $header
   ));
  }

  return $id_atasan;
 }

 public function getDataStruktur($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur',
              'where' => "deleted = 0 and pegawai = '" . $pegawai . "'"
  ));

  if (!empty($data)) {
   return 1;
  }


  return 0;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
//

  $this->db->trans_begin();
  try {
   $data_post = $data->struktur;

   $id_atasan = $this->getIdAtasan($data_post->atasan, $data->jabatan_header);
   if (!empty($data_post->bawahan)) {
    $bawahan = $data_post->bawahan;
    foreach ($bawahan as $value) {
     $is_exist_data = $this->getDataStruktur($value->bawahan);
//     echo $is_exist_data;die;
     if ($is_exist_data == 0) {
      Modules::run('database/_insert', $this->getTableName(), array(
          'pegawai' => $value->bawahan,
          'jabatan_header' => $data->jabatan_header,
          'parent' => $id_atasan
      ));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Strukturorg";
  $data['title_content'] = 'Data Strukturorg';
  $content = $this->getDataStrukturorg($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteHeader($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'jabatan_header', array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function simpanJabatanHeader() {
  $title = $this->input->post('title');
  $id = $this->input->post('id');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['title'] = $title;
   if ($id == "") {
    $id = Modules::run('database/_insert', 'jabatan_header', $post);
   } else {
    unset($post['nomer_room']);
    Modules::run('database/_update', 'jabatan_header', $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addDetail() {
  $data['no'] = $this->input->post('no');
  $data['list_pegawai'] = $this->getListPegawai();
  echo $this->load->view('tr_bawahan', $data, true);
 }

}
