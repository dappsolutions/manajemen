<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Daftar' ?>
     </div>
    </div>
    <div class="tile">
     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" type="search" onkeyup="PlanPermission.search(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>    

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_permission">
          <thead>
           <tr class="table-warning">
            <th>#</th>
            <th>Pegawai</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['nama_pegawai'] ?></td>
              <td class="text-center">
               <i class="fa fa-trash grey-text hover" onclick="PlanPermission.delete('<?php echo $value['id'] ?>')"></i>
               <!--&nbsp;-->
  <!--               <i class="fa fa-pencil grey-text  hover" onclick="PlanPermission.ubah('<?php echo $value['id'] ?>')"></i>              
               &nbsp;-->
               <!--<i class="fa fa-file-text grey-text  hover" onclick="PlanPermission.detail('<?php echo $value['id'] ?>')"></i>-->
              </td>
             </tr>
            <?php } ?>
           <?php } ?>

           <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
            <tr class="text-center">
             <td>
              &nbsp;
             </td>
             <td>
              <select class="form-control user" no="1" id="user-1" error="User">
               <option value="">Pilih Pegawai</option>
               <option value="tidak_ada">Tidak Punya</option>
               <?php if (!empty($list_pegawai)) { ?>
                <?php foreach ($list_pegawai as $value) { ?>
                 <?php $selected = '' ?>
                 <?php if (isset($user)) { ?>
                  <?php $selected = $user == $value['user_id'] ? 'selected' : '' ?>
                 <?php } ?>
                 <option <?php echo $selected ?> value="<?php echo $value['user_id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] ?></option>
                <?php } ?>
               <?php } ?>
              </select>
             </td>
             <td class="text-center">
              <i class="fa fa-plus-circle fa-2x text-primary hover" onclick="PlanPermission.addDetail(this)"></i>
             </td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
       <div class="row">
        <div class="col-md-12 text-right">
         <button class="btn btn-warning" onclick="PlanPermission.prosesSimpan()">Simpan</button>
        </div>
       </div>
      <?php } ?>
      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div> 
   </div>
  </div>
 </div>
</div>