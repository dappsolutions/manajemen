<tr>
 <td>
  &nbsp;
 </td>
 <td>
  <select class="form-control user" no="<?php echo $no ?>" id="user-<?php echo $no ?>" error="Bawahan">
   <option value="">Pilih Pegawai</option>
   <option value="tidak_ada">Tidak Punya</option>
   <?php if (!empty($list_pegawai)) { ?>
    <?php foreach ($list_pegawai as $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($user_id)) { ?>
      <?php $selected = $user_id == $value['user_id'] ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['user_id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] ?></option>
    <?php } ?>
   <?php } ?>
  </select>
 </td>
 <td class="text-center">
  <i class="fa fa-minus fa-2x text-danger hover" onclick="PlanPermission.removeDetail(this)"></i>
 </td>
</tr>

<script>
 $(function () {
  $('select#user-<?php echo $no ?>').select2();
 });
</script>