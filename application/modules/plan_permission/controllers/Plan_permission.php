<?php

class Plan_permission extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'plan_permission';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/plan_permission.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'work_plan_permission';
 }

 public function getRootModule() {
  return "Work Plan";
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'j.jabatan as nama_jabatan', 'u.id as user_id'),
              'join' => array(
                  array('jabatan j', 'p.jabatan = j.id'),
                  array('user u', 'u.pegawai = p.id'),
              ),
              'where' => "p.deleted = 0"
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addDetail() {
  $data['no'] = $this->input->post('no');
  $data['list_pegawai'] = $this->getListPegawai();
  echo $this->load->view('tr_pegawai', $data, true);
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Permission";
  $data['title_content'] = 'Permission';
  $content = $this->getDataPermission();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['list_pegawai'] = $this->getListPegawai();
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPermission($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('pg.nama', $keyword),
   );
  }

  $where = "pp.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pp.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pp',
                'field' => array('pp.*', 'u.username', 'pg.nama as nama_pegawai'),
                'join' => array(
                    array('user u', 'pp.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pp',
                'field' => array('pp.*', 'u.username', 'pg.nama as nama_pegawai'),
                'join' => array(
                    array('user u', 'pp.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataPermission($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('pg.nama', $keyword),
   );
  }

  $where = "pp.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pp.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pp',
                'field' => array('pp.*', 'u.username', 'pg.nama as nama_pegawai'),
                'join' => array(
                    array('user u', 'pp.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pp',
                'field' => array('pp.*', 'u.username', 'pg.nama as nama_pegawai'),
                'join' => array(
                    array('user u', 'pp.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPermission($keyword)
  );
 }

 public function getDetailDataPermission($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Permission";
  $data['title_content'] = 'Tambah Permission';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPermission($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Permission";
  $data['title_content'] = 'Ubah Permission';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPermission($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Permission";
  $data['title_content'] = "Detail Permission";
  echo Modules::run('template', $data);
 }

 public function getPostDataPermission($value) {
  $data['user'] = $value->user;
  return $data;
 }

 public function getIsExistData($user) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => "deleted = 0 and user = '" . $user . "'"
  ));

  if (!empty($data)) {
   return 1;
  }

  return 0;
 }

 public function prosesSimpan() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;

  $message = "";
  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $post = $this->getPostDataPermission($value);
     $is_exist_data = $this->getIsExistData($value->user);
     if ($is_exist_data == 0) {
      Modules::run('database/_insert', $this->getTableName(), $post);
     } else {
      $message = "Data Sudah Diinput";
     }
    }
   }
   $this->db->trans_commit();
   if ($message == '') {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'message'=> $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Permission";
  $data['title_content'] = 'Data Permission';
  $content = $this->getDataPermission($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
