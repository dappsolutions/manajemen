<?php

class Dailytaskbawahan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $jabatan_id;
 public $struktur_id;
 public $pegawai_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->jabatan_id = $this->session->userdata('jabatan_id');
  $this->struktur_id = $this->session->userdata('struktur_id');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'dailytaskbawahan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/dailytaskbawahan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai_job_desk';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Dailytask bawahan";
  $data['title_content'] = 'Dailytask bawahan';
  $content = $this->getDataDailytaskbawahan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getJabatanHasChild() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.parent = '" . $this->struktur_id . "'"
  ));

  $is_has = false;
  if (!empty($data)) {
   $is_has = true;
  } else {
   if ($this->akses == 'superadmin') {
    $is_has = true;
   }
  }

  return $is_has;
 }

 public function getTotalDataDailytaskbawahan($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pjd.deleted = 0 and u.id = '" . $user_id . "'";
  } else {
   $where = "pjd.deleted = 0 and (u.id = '" . $user_id . "') and (jds.status != 'APPROVED' and jds.status != 'CLOSED')";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataDailytaskbawahan($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "pjd.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pjd.deleted = 0 and u.id = '" . $user_id . "'";
  } else {
   $where = "pjd.deleted = 0 and (u.id = '" . $user_id . "') and (jds.status != 'APPROVED' and jds.status != 'CLOSED')";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataDailytaskbawahan($keyword)
  );
 }

 public function getDetailDataDailytaskbawahan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pjd',
              'field' => array('pjd.*', 'p.nama as nama_pegawai',
                  'p.nip', 'pg.nama as nama_pemberi',
                  'jds.status', 'jd.jenis', 'ut.id as user_pegawai'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user ut', 'ut.pegawai = p.id'),
                  array('user u', 'pjd.giver = u.id', 'left'),
                  array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                  array('jabatan j', 'pg.jabatan = j.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
                  array('jenis_job jd', 'pjd.jenis_job = jd.id'),
              ),
              'where' => "pjd.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function detail($id) {
  $data = $this->getDetailDataDailytaskbawahan($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Dailytask bawahan";
  $data['title_content'] = "Detail Dailytask bawahan";
  $data['user_id'] = $this->session->userdata('user_id');
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Dailytaskbawahan";
  $data['title_content'] = 'Data Dailytaskbawahan';
  $content = $this->getDataDailytaskbawahan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

}
