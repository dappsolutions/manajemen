<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;Daftar
     </div>
    </div>

    <div class="tile">
     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" type="search" onkeyup="Pegawai.search(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>  

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="table-warning">
            <th>No</th>
            <th>NIP</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Nama Panjang Posisi</th>
            <th>Unit</th>
            <th>Sub Unit</th>
            <th>Email</th>            
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['nip'] ?></td>
              <td><?php echo $value['nama'] ?></td>
              <td><?php echo $value['jabatan'] ?></td>
              <td><?php echo $value['nama_panjang'] ?></td>
              <td><?php echo $value['nama_ultg'] ?></td>
              <td><?php echo $value['sub_unit'] ?></td>
              <td><?php echo $value['email'] ?></td>              
              <td class="text-center">
               <i class="fa fa-trash grey-color hover" onclick="Pegawai.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-pencil grey-color hover" onclick="Pegawai.ubah('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-file-text grey-color hover" onclick="Pegawai.detail('<?php echo $value['id'] ?>')"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr class="text-center">
             <td colspan="8">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>  
    </div> 
   </div>
  </div>
 </div>
</div>


<a href="#" class="float">
 <i class="fa fa-plus my-float fa-lg" onclick="Pegawai.add()"></i>
</a>