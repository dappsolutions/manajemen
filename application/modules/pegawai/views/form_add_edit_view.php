<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i class="fa fa-file-text-o hover" onclick="Pegawai.main()"></i>&nbsp;&nbsp;<?php echo ucfirst($module) ?>
     <hr/>
    </div>
   </div>
   <div class="col-md-9">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;Form
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-1">NIP</label>
        <div class="col-sm-6">
         <input class="form-control required" error="NIP" id="nip" type="text" placeholder="NIP" value="<?php echo isset($nip) ? $nip : '' ?>">
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-3">Nama Pegawai</label>
        <div class="col-sm-6">
         <input class="form-control required" error="Nama Pegawai" id="nama" type="text" placeholder="Nama Pegawai" value="<?php echo isset($nama) ? $nama : '' ?>">
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-1">Jabatan</label>
        <div class="col-sm-6">
         <select id="jabatan" class="form-control required" error="Jabatan">
          <option value="">Pilih Jabatan</option>
          <?php if (!empty($list_jabatan)) { ?>
           <?php foreach ($list_jabatan as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($jabatan_id)) { ?>
             <?php $selected = $jabatan_id == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jabatan'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-1">Email</label>
        <div class="col-sm-6">
         <input class="form-control required" error="Email" id="email" type="text" placeholder="Nama Pegawai" value="<?php echo isset($email) ? $email : '' ?>">
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-1">Unit</label>
        <div class="col-sm-6">
         <select id="ultg" class="form-control required" error="Unit" onchange="Pegawai.getListSubUltg(this)">
          <option value="">Pilih Unit</option>
          <?php if (!empty($list_ultg)) { ?>
           <?php foreach ($list_ultg as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($ultg)) { ?>
             <?php $selected = $ultg == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>       

       <div class="form-group">
        <label class="control-label col-sm-3">Sub Unit</label>
        <div class="col-sm-6">
         <select id="sub_ultg" class="form-control required" error="ULTG">
          <option value="">Pilih Sub Unit</option>
          <?php if (!empty($list_subultg)) { ?>
           <?php foreach ($list_subultg as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($sub_ultg)) { ?>
             <?php $selected = $sub_ultg == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option ultg="<?php echo $value['ultg'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['sub_ultg'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>       
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="Pegawai.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Pegawai.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
