<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <!--   <div class="col-md-3">
       <div class="tile">
        <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;<?php echo $title_content ?></h5>
       </div>
      </div>-->
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Form Data' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <br/>      
      <div class="row">
       <div class="col-md-3 ">
        <b>NIP</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $nip ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Nama Pegawai</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $nama ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Jabatan</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $jabatan ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Nama Panjang Posisi</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $nama_panjang ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Unit</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $nama_ultg ?>
       </div>        
      </div>
      <br/> 
      <div class="row">
       <div class="col-md-3 ">
        <b>Sub Unit</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $sub_unit ?>
       </div>        
      </div>
      <br/> 
      <div class="row">
       <div class="col-md-3 ">
        <b>Email</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $email ?>
       </div>        
      </div>
      <br/>           
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Pegawai.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
