<?php

class Dashboard extends MX_Controller {

 public $hak_akses;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<link href="' . base_url() . 'assets/css/jquery-ui.min.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/jquery-ui.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<link href="' . base_url() . 'assets/plugins/components/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
      '<script src="' . base_url() . 'assets/plugins/components/ckeditor/ckeditor.js"></script>',
      '<script src="' . base_url() . 'assets/plugins/components/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['list_menu'] = $this->getListMenuData();

  $data['total_user'] = $this->getTotalUser();
  $data['total_like'] = $this->getTotalLike();
  $data['total_pegawai'] = $this->getTotalPegawai();
  $data['total_upload'] = $this->getTotalUploadedFile();
  $data['pengajuan'] = array();
  $data['hak_akses'] = $this->session->userdata('hak_akses');
  echo Modules::run('template', $data);
 }

 public function getTotalUser() {
  $total = Modules::run('database/count_all', array(
              'table' => 'room_body_like',
  ));
  return $total;
 }

 public function getTotalLike() {
  $total = Modules::run('database/count_all', array(
              'table' => 'user',
  ));
  return $total;
 }

 public function getTotalPegawai() {
  $total = Modules::run('database/count_all', array(
              'table' => 'pegawai',
              'where' => 'deleted = 0'
  ));
  return $total;
 }

 public function getTotalUploadedFile() {
  $total = Modules::run('database/count_all', array(
              'table' => 'room_body_detail',
              'where' => "deleted = 0 and file is not null"
  ));
  return $total;
 }

 public function getListMenuData() {
  if ($this->hak_akses == "superadmin") {
   $data = Modules::run('database/get', array(
               'table' => 'room m',
               'field' => array('m.*'),
               'where' => "m.deleted = 0",
               'orderby' => 'm.row'
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'room r',
               'field' => array('r.*', 'rp.view'),
               'join' => array(
                   array('room_permission rp', 'rp.room = r.id')
               ),
               'where' => "r.deleted = 0",
               'orderby' => 'r.row'
   ));
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['children'] = $this->getListHeader($value['id']);
    // echo '<pre>';
    // print_r(current($value['children']));die;
    $value['body'] = !empty($value['children']) ? $this->getListBodyMenu(current($value['children'])['id']) : array();
    // echo '<pre>';
    // print_r($value['body']);die;
    $value['room_header'] = "";
    if (empty($value['body'])) {
     $value['room_header'] = current($value['children'])['id'];
    }

    if ($this->hak_akses == 'superadmin') {
     array_push($result, $value);
    } else {
     if ($value['view'] == 1) {
      array_push($result, $value);
     }
    }
   }
  }

//  echo '<pre>';
//  print_r($result);
//  die;
  return $result;
 }

 public function getListHeader($room) {
  if ($this->hak_akses == "superadmin") {
   $data = Modules::run('database/get', array(
               'table' => 'room_header rh',
               'field' => array('rh.*'),
               'join' => array(
                   array('room r', 'rh.room = r.id')
               ),
               'where' => "rh.deleted = 0 and rh.room = '" . $room . "'",
               'orderby' => 'rh.parent, rh.row'
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'room_header rh',
               'field' => array('rh.*', 'rp.view', 'rp.download'),
               'join' => array(
                   array('room r', 'rh.room = r.id'),
                   array('room_header_permission rp', 'rh.id = rp.room_header'),
               ),
               'where' => "rh.deleted = 0 and rh.room = '" . $room . "'",
               'orderby' => 'rh.parent, rh.row'
   ));
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $file = str_replace(' ', '_', $value['file']);
    $file = str_replace('_pdf', '.pdf', $file);
    $value['file_data'] = base_url() . 'files/berkas/document/' . $file;
    array_push($result, $value);
   }
  }


  $tree = Modules::run('menu/buildTree', $result);
//  echo '<pre>';
//  print_r($tree);die;
  return $tree;
 }

 public function getListBodyMenu($room_header) {

  if ($this->hak_akses == "superadmin") {
   $data = Modules::run('database/get', array(
               'table' => 'room_body rb',
               'field' => array('rb.*'),
               'join' => array(
                   array('room_header rh', 'rb.room_header = rh.id')
               ),
               'where' => "rb.deleted = 0 and rb.room_header = '" . $room_header . "'"
   ));
  } else {
   $user_id = $this->session->userdata('user_id');
   $data = Modules::run('database/get', array(
               'table' => 'room_body rb',
               'field' => array('rb.*', 'rp.view', 'rp.download'),
               'join' => array(
                   array('room_header rh', 'rb.room_header = rh.id'),
                   array('room_body_permission rp', 'rb.id = rp.room_body'),
               ),
               'where' => "rb.deleted = 0 and "
               . "rb.room_header = '" . $room_header . "' and rp.user = '" . $user_id . "'"
   ));
  }


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['total_like'] = $this->getTotalLikeRoomBody($value['id']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getTotalLikeRoomBody($id) {
  $total = Modules::run('database/count_all', array(
              'table' => 'room_body_like',
              'where' => array('room_body' => $id)
  ));

  if ($total == 0) {
   $total = '';
  }
  return $total;
 }

 public function simpanRoom() {
  $title = $this->input->post('title');
  $id = $this->input->post('id');
//  echo $title;
//  die;
  $is_valid = false;
  $this->db->trans_begin();
  try {


   $post['user'] = $this->session->userdata('user_id');
   $post['nama'] = $title;
   $post['nomer_room'] = Modules::run('no_generator/generateNoRoom');
   if ($id == "") {
    $id = Modules::run('database/_insert', 'room', $post);
   } else {
    unset($post['nomer_room']);
    Modules::run('database/_update', 'room', $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function simpanTitle() {
  $title = $this->input->post('title');
  $room = $this->input->post('room');
  $id = $this->input->post('id');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['room'] = $room;
   $post['title'] = $title;
   if ($id == '') {
    Modules::run('database/_insert', 'room_header', $post);
   } else {
    Modules::run('database/_update', 'room_header', $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function saveTitleEditBody() {
  $title = $this->input->post('title');
  $id = $this->input->post('id');
//  echo '<pre>';
//  print_r($_POST);die;
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['title'] = $title;
   Modules::run('database/_update', 'room_body_detail', $post, array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function simpanBody() {
  $data = json_decode($this->input->post('data'));
  $id = $data->id;
  $file = $_FILES;
//  echo '<pre>';
//  print_r($data);die;
  $is_save = true;
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post['room_header'] = $data->room_header;
   $post['title'] = $data->title;
   $post['room_category'] = 1;
   $post['pesan'] = $data->pesan;
   $post['tipe_upload'] = $data->tipe_upload;
   if ($data->tipe_upload == '3') {
    $post['url'] = $data->url;
   }

   if (!empty($file)) {
    $response_upload = $this->uploadData('file');
    if ($response_upload['is_valid']) {
     $post['file'] = $file['file']['name'];
    } else {
     $is_save = false;
     $message = $response_upload['response'];
    }
   }

   if ($id == "") {
    if ($is_save) {
     Modules::run('database/_insert', 'room_body', $post);
    }
   } else {
    if ($is_save) {
     Modules::run('database/_update', 'room_body', $post, array('id' => $id));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
//  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addBody() {
  $room_header = $this->input->post('room_header');
  $content['room_header'] = intval($room_header);
  $content['list_upload'] = $this->getListTipeUpload();
  echo $this->load->view('form_body', $content, true);
 }

 public function editBody() {
  $id = $this->input->post('id');
  $data = $this->getDetailBodyMenu($id);
  $content = $data;
  $room_header = $data['room_header'];
  $content['id'] = $id;
  $content['title'] = $data['title'];
  $content['room_header'] = $room_header;
  $content['list_upload'] = $this->getListTipeUpload();
  echo $this->load->view('form_body', $content, true);
 }

 public function detailBody() {
  $id = $this->input->post('id');
  $room = $this->input->post('room');
  $room_name = $this->input->post('room_name');
  $content['room'] = $room;
  $content['room_name'] = $room_name;
  $content['id'] = $id;
  $content['body'] = $this->getListBodyMenu($id);
//  echo '<pre>';
//  print_r($content);die;
//  echo '<pre>';
//  echo $id;
//  print_r($content['body']);die;
  $content['hak_akses'] = $this->session->userdata('hak_akses');
  $view_body = $this->load->view('detail_body', $content, true);
  $view_btn = $this->load->view('btn_add_body', $content, true);
  echo json_encode(array(
      'view_body' => $view_body,
      'view_btn' => $view_btn
  ));
 }

 public function getDetailBodyMenu($id_body) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body rb',
              'where' => "rb.deleted = 0 and rb.id = '" . $id_body . "'"
  ));

  return $data->row_array();
 }

 public function getDetailBodyDetailMenu($id_body) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_detail rbd',
              'field' => array('rbd.*', 'tu.tipe'),
              'join' => array(
                  array('tipe_upload tu', 'tu.id = rbd.tipe_upload')
              ),
              'where' => "rbd.deleted = 0 and rbd.id = '" . $id_body . "'"
  ));

  return $data->row_array();
 }

 public function getListTipeUpload() {
  $data = Modules::run('database/get', array(
              'table' => 'tipe_upload',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail_trans($id_body) {
  $data['view_file'] = 'trans_body';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Body Transaction";
  $data['title_content'] = 'Body Transaction';
  $data['body'] = $this->getDetailBodyMenu($id_body);
  $data['list_upload'] = $this->getListTipeUpload();
  echo Modules::run('template', $data);
 }

 public function createTable($id_body) {
  $data['view_file'] = 'create_table';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Create Table ";
  $data['title_content'] = 'Create Table ';
  $data['body'] = $this->getDetailBodyMenu($id_body);
  $data['list_upload'] = $this->getListTipeUpload();
  echo Modules::run('template', $data);
 }

 public function detailTrans($id) {
  $data = $this->getDetailBodyDetailMenu($id);
  $id_body = $data['room_body'];
  $data['view_file'] = 'detail_trans_body';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
//  $data['title'] = "Detail Body Transaction";
  $data['title_content'] = 'Detail Body Transaction';
  $data['body'] = $this->getDetailBodyMenu($id_body);
  $data['list_upload'] = $this->getListTipeUpload();
  echo Modules::run('template', $data);
 }

 public function simpanTrans() {
  $data = json_decode($this->input->post('data'));
  $file = $_FILES;

  $is_valid = false;
  $is_save = true;
  $message = "";
  $id = $this->input->post('id');
  $this->db->trans_begin();
  try {
   $post['room_body'] = $data->room_body;
   $post['title'] = $data->title;
   $post['tipe_upload'] = $data->tipe_upload;
   if (!empty($file)) {
    $response_upload = $this->uploadData('file');
    if ($response_upload['is_valid']) {
     $post['file'] = $file['file']['name'];
    } else {
     $is_save = false;
     $message = $response_upload['response'];
    }
   }
   if ($is_save) {
    $id = Modules::run('database/_insert', 'room_body_detail', $post);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/document/';
  $config['allowed_types'] = 'pdf|xls|xlsx|jpg|png|jpeg';
  $config['max_size'] = '25000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  // $this->upload->do_upload($name_of_field);


  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showFile() {
  $tipe = $this->input->post('tipe');
  $file = $this->input->post('file');

  $file = str_replace(' ', '_', $file);
  if ($tipe == 2) {
   $file = str_replace('.', '_', $file);
   $file = str_replace('_&', '', $file);
   $file = str_replace('_pdf', '.pdf', $file);
  }
  $data['file'] = $file;

  if ($tipe == 2) {
   echo $this->load->view('data_file', $data, true);
  } else {
   echo $this->load->view('data_file_image', $data, true);
  }
 }

 public function getListBodyMenuData($room_body) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_detail rbd',
              'field' => array('rbd.*', 'rb.title as judul_header'),
              'join' => array(
                  array('room_body rb', 'rbd.room_body = rb.id')
              ),
              'where' => "rbd.deleted = 0 and rbd.room_body = '" . $room_body . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDetailMenuBody($room_body) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body rb',
              'where' => "rb.deleted = 0 and rb.id = '" . $room_body . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function showDetail() {
  $room_body = $this->input->post('room_body');
  $data_room = $this->getListBodyMenuData($room_body);
  $data_header = $this->getDetailMenuBody($room_body);
  $data['judul_header'] = $data_header['title'];
  $data['detail'] = $data_room;
  $data['hak_akses'] = $this->session->userdata('hak_akses');
  echo $this->load->view('list_body_detail', $data, true);
 }

 public function delete($id) {
  $table = $this->input->post('table');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $table, array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addChildRoom() {
  $parent = $this->input->post('parent');
  $room = $this->input->post('room');
  $data['list_upload'] = $this->getListTipeUpload();
  $data['parent'] = $parent;
  $data['room'] = $room;
  echo $this->load->view('room_header_form', $data, true);
 }

 public function simpanRoomHeader() {
  $data = json_decode($this->input->post('data'));
  $file = $_FILES;

  $is_valid = false;
  $is_save = true;
  $message = "";
  $id = $this->input->post('id');
  $this->db->trans_begin();
  try {
   $post['room'] = $data->room;
   $post['parent'] = $data->parent;
   $post['title'] = $data->title;
   $post['tipe_upload'] = $data->tipe_upload;
   if (!empty($file)) {
    $response_upload = $this->uploadData('file');
    if ($response_upload['is_valid']) {
     $post['file'] = $file['file']['name'];
    } else {
     $is_save = false;
     $message = $response_upload['response'];
    }
   }
   if ($is_save) {
    $id = Modules::run('database/_insert', 'room_header', $post);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function downloadBodyDetail() {
  $id = $this->input->post('id');
  $file = $this->input->post('file');
  $file = base_url() . 'files/berkas/document/' . $file;
  $this->load->helper('download');
  force_download($file, NULL);
 }

 public function getIsExistUserLike($user, $id) {
  $sql = "select * from room_body_like where room_body = '" . $id . "' and user = '" . $user . "'";
  $data = Modules::run('database/get_custom', $sql);

  $is_ecist = 0;
  if (!empty($data)) {
   $is_ecist = 1;
  }

  return $is_ecist;
 }

 public function likeBody() {
  $id = $this->input->post('id');
  $user = $this->session->userdata('user_id');
  $is_exist = $this->getIsExistUserLike($user, $id);
  $total = '';
  if (!$is_exist) {
   $post['room_body'] = $id;
   $post['user'] = $user;
   Modules::run('database/_insert', 'room_body_like', $post);
  }

  $total = Modules::run('database/count_all', array(
              'table' => 'room_body_like',
              'where' => array('room_body' => $id)
  ));

  echo $total;
 }

 public function ubahUrutan($room, $parent_id = "") {
  $data['view_file'] = 'urutan_room_header';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Urutan";
  $data['title_content'] = 'Ubah Urutan';

  if ($parent_id == "") {
   $room_header = Modules::run('database/get', array(
               'table' => 'room_header rh',
               'where' => "rh.deleted = 0 and rh.room = '" . $room . "' "
               . "and rh.parent is null",
               'orderby' => 'row'
   ));
  } else {
   $room_header = Modules::run('database/get', array(
               'table' => 'room_header rh',
               'where' => "rh.deleted = 0 and rh.room = '" . $room . "' "
               . "and rh.parent = '" . $parent_id . "'",
               'orderby' => 'row'
   ));
  }

  $result = array();
  if (!empty($room_header)) {
   foreach ($room_header->result_array() as $value) {
    $value['children'] = $this->getChildrenRoomHeader($value['id']);
    array_push($result, $value);
   }
  }

  $data['room'] = $room;
//
//  $tree = Modules::run('menu/buildTree', $result);
  $data['result'] = $result;
//  echo '<pre>';
//  print_r($tree);die;
  echo Modules::run('template', $data);
 }

 public function ubahUrutanRoom($id) {
  $data['view_file'] = 'urutan_room';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Urutan";
  $data['title_content'] = 'Ubah Urutan';
  $room = Modules::run('database/get', array(
              'table' => 'room rh',
              'where' => "rh.deleted = 0",
              'orderby' => 'rh.row'
  ));

  $result = array();
  if (!empty($room)) {
   foreach ($room->result_array() as $value) {
    array_push($result, $value);
   }
  }

  $data['room'] = $id;
  $data['result'] = $result;
  echo Modules::run('template', $data);
 }

 public function ubahUrutanBody($id) {
  $data['view_file'] = 'urutan_body';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Urutan";
  $data['title_content'] = 'Ubah Urutan';
  $detail_room_body = Modules::run('database/get', array(
              'table' => 'room_body',
              'where' => array('id' => $id)
          ))->row_array();

  $room_body = Modules::run('database/get', array(
              'table' => 'room_body rh',
              'where' => "rh.deleted = 0 and rh.room_header = '" . $detail_room_body['room_header'] . "'",
              'orderby' => 'rh.row'
  ));

//  echo '<pre>';
//  print_r($room_body);die;
  $result = array();
  if (!empty($room_body)) {
   foreach ($room_body->result_array() as $value) {
    $value['nama'] = $value['title'];
    array_push($result, $value);
   }
  }

  $data['room_header'] = $detail_room_body['room_header'];
  $data['result'] = $result;
  echo Modules::run('template', $data);
 }

 public function getChildrenRoomHeader($room_header) {
  $data = Modules::run('database/get', array(
              'table' => 'room_header',
              'where' => "deleted = 0 and parent = '" . $room_header . "'",
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function updateHeader() {
  $data = json_decode($this->input->post('data'));
//  $room = $this->input->post('room');
//  
  $is_valid = true;
  if (!empty($data)) {
   foreach ($data as $value) {
    $post_update['row'] = $value->row;
    Modules::run('database/_update', 'room_header', $post_update, array('id' => $value->id));
   }
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function updateUrutanRoom() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
//  $room = $this->input->post('room');
//  
  $is_valid = true;
  if (!empty($data)) {
   foreach ($data as $value) {
    $post_update['row'] = $value->row;
    Modules::run('database/_update', 'room', $post_update, array('id' => $value->id));
   }
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function updateUrutanBody() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
//  $room_header = $this->input->post('room_header');
//  
  $is_valid = true;
  if (!empty($data)) {
   foreach ($data as $value) {
    $post_update['row'] = $value->row;
    Modules::run('database/_update', 'room_body', $post_update, array('id' => $value->id));
   }
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function simpanTable() {
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_tb['room_body'] = $data->room_body;
   $post_tb['nama'] = $data->nama_table;
   $table = Modules::run('database/_insert', "room_body_table", $post_tb);

   if (!empty($data->field)) {
    foreach ($data->field as $value) {
     $post_field['room_body_table'] = $table;
     $post_field['nama_field'] = $value->field;
     Modules::run('database/_insert', 'room_body_table_has_field', $post_field);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function showFormComment() {
  $room_body = $this->input->post('room_body');
  $data['room_body'] = $room_body;
  $data['title'] = $this->input->post('title');
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('form_comment', $data, true);
 }

 public function simpanComment() {
  $data = $_POST;

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $sub_com['room_body'] = $data['room_body'];
   $sub_com['title'] = $data['judul'];
   $sub_com['user'] = $this->session->userdata('user_id');
   $sub_comment = Modules::run('database/_insert', "room_body_sub_comment", $sub_com);

   $com['room_body_sub_comment'] = $sub_comment;
   $com['isi'] = $data['komentar'];
   $com['user'] = $this->session->userdata('user_id');
   $comment = Modules::run('database/_insert', 'room_body_comment', $com);

   $status_sub['room_body_sub_comment'] = $sub_comment;
   $status_sub['status'] = 'OPEN';
   Modules::run('database/_insert', 'status_sub_comment', $status_sub);

   $status_com['room_body_comment'] = $comment;
   $status_com['status'] = 'SEND';
   Modules::run('database/_insert', 'status_comment', $status_com);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getPegawai($room_body) {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'u.id as user_id',
                  'rp.id as permission_id',
                  'rp.view', 'rp.download', 'rp.room_body'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai'),
                  array('room_body_permission rp', 'u.id = rp.user', 'left'),
              ),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   $temp = "";
   foreach ($data->result_array() as $value) {
    if ($value['permission_id'] != '') {
     if ($room_body != $value['room_body']) {
      $value['permission_id'] = "";
      $value['view'] = "";
      $value['download'] = "";
     }
     if ($temp != $value['nip']) {
      array_push($result, $value);
     }
    } else {
     array_push($result, $value);
    }

    $temp = $value['nip'];
   }
  }


  return $result;
 }

 public function getPegawaiPermissionHeader($room_header) {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'u.id as user_id',
                  'rp.id as permission_id',
                  'rp.view', 'rp.download', 'rp.room_header'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai'),
                  array('room_header_permission rp', 'u.id = rp.user', 'left'),
              ),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   $temp = "";
   foreach ($data->result_array() as $value) {
    if ($value['permission_id'] != '') {
     if ($room_header != $value['room_header']) {
      $value['permission_id'] = "";
      $value['view'] = "";
      $value['download'] = "";
     }
     if ($temp != $value['nip']) {
      array_push($result, $value);
     }
    } else {
     array_push($result, $value);
    }

    $temp = $value['nip'];
   }
  }


  return $result;
 }

 public function getPegawaiPermissionRoom($room_header) {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'u.id as user_id',
                  'rp.id as permission_id',
                  'rp.view', 'rp.download', 'rp.room'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai'),
                  array('room_permission rp', 'u.id = rp.user', 'left'),
              ),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   $temp = "";
   foreach ($data->result_array() as $value) {
    if ($value['permission_id'] != '') {
     if ($room_header != $value['room']) {
      $value['permission_id'] = "";
      $value['view'] = "";
      $value['download'] = "";
     }
     if ($temp != $value['nip']) {
      array_push($result, $value);
     }
    } else {
     array_push($result, $value);
    }

    $temp = $value['nip'];
   }
  }


  return $result;
 }

 public function addPermissionBody() {
  $data = $_POST;
  $data['data_pegawai'] = $this->getPegawai($data['room_body']);

  echo $this->load->view('user_permission_view', $data, true);
 }

 public function addPermissionHeader() {
  $data = $_POST;
  $data['data_pegawai'] = $this->getPegawaiPermissionHeader($data['room_header']);

  echo $this->load->view('user_permission_header', $data, true);
 }

 public function addPermissionRoom() {
  $data = $_POST;
  $data['data_pegawai'] = $this->getPegawaiPermissionRoom($data['room']);

  echo $this->load->view('user_permission_room', $data, true);
 }

 public function savePermissionUser() {
  $data = json_decode($_POST['data']);
  $room_body = $_POST['room_body'];

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $post['user'] = $value->user_id;
     $post['room_body'] = $room_body;
     $post['view'] = $value->check_view;
     $post['download'] = $value->check_down;
     if ($value->permission_id == '') {
      Modules::run('database/_insert', 'room_body_permission', $post);
     } else {
      Modules::run('database/_update', 'room_body_permission', $post,
              array('id' => $value->permission_id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function savePermissionHeaderUser() {
  $data = json_decode($_POST['data']);
  $room_header = $_POST['room_header'];

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $post['user'] = $value->user_id;
     $post['room_header'] = $room_header;
     $post['view'] = $value->check_view;
     $post['download'] = $value->check_down;
     if ($value->permission_id == '') {
      Modules::run('database/_insert', 'room_header_permission', $post);
     } else {
      Modules::run('database/_update', 'room_header_permission', $post,
              array('id' => $value->permission_id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function savePermissionRoomUser() {
  $data = json_decode($_POST['data']);
  $room = $_POST['room'];

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if (!empty($data)) {
    foreach ($data as $value) {
     $post['user'] = $value->user_id;
     $post['room'] = $room;
     $post['view'] = $value->check_view;
     $post['download'] = $value->check_down;
     if ($value->permission_id == '') {
      Modules::run('database/_insert', 'room_permission', $post);
     } else {
      Modules::run('database/_update', 'room_permission', $post,
              array('id' => $value->permission_id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
