<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text"></i>&nbsp;&nbsp;&nbsp; <?php echo 'Form ' . ucwords($title) ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <div class="row">
       <div class="col-md-3 ">
        <b>Judul</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $title ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Tipe Upload</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $tipe ?>
       </div>        
      </div>      
      <br/>
      <?php $url = ''; ?>
      <?php if ($tipe == "Gambar") { ?>
       <?php $url = "showFile(this, 'image')"; ?>
      <?php } ?>
      <?php if ($tipe == "Dokumen") { ?>
       <?php $url = "showFile(this, 'doc')"; ?>
      <?php } ?>

      <?php if ($url != '') { ?>
       <div class="row">
        <div class="col-md-3">
         <b>File</b>
        </div>
        <div class="col-sm-5 text-success">
         <label class="hover" onclick="Dashboard.<?php echo $url ?>"><?php echo $file ?></label>
        </div>
       </div>
      <?php } ?>

     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <a class="btn btn-secondary text-white" onclick="Dashboard.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
