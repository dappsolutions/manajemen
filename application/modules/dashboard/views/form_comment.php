<input type="hidden" value="<?php echo $room_body ?>" id="room_body" class="form-control" />
<div class="row">
 <div class="col-md-12">
  <h4>Form Comment</h4>
  <hr/>
  <form class="form-horizontal">
   <div class="form-group">
    <label class="control-label col-sm-1">Dokumen</label>
    <div class="col-sm-12">
     <input class="form-control required" error="" id="dokumen" type="text" 
            placeholder="" disabled="" value="<?php echo $title ?>">
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-1">Judul</label>
    <div class="col-sm-12">
     <input class="form-control required" error="Judul" id="judul" type="text" placeholder="Masukkan Judul">
    </div>
   </div>
   <div class="form-group">
    <label class="control-label col-sm-1">Komentar</label>
    <div class="col-sm-12">
     <textarea id="komentar" class="form-control"></textarea>
    </div>        
   </div>   
  </form>
 </div>
 <div class="col-md-12 text-right">
  <button class="btn btn-warning" onclick="Dashboard.simpanComment(this)">Proses</button>
 </div>
</div>

<script>
// CKEDITOR.replace('komentar');
</script>