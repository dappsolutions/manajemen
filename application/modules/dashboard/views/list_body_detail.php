
<div class="row">
 <div class="col-md-12">  
  <h5>Detail <?php echo $judul_header ?></h5>
  <hr/>
  <div class="table-responsive border-grey-color">
   <table class="table table-borderless">
    <tbody>
     <?php if (!empty($detail)) { ?>
      <?php foreach ($detail as $v_body) { ?>
       <tr data_id='<?php echo $v_body['id'] ?>' data_name="<?php echo $v_body['title'] ?>">
        <td class="text-left"><i class="fa fa-file-text-o" style="margin-right: 16px;"></i><?php echo $v_body['title'] ?></td>
        <td class="text-right">
         <?php if ($hak_akses == 'superadmin') { ?>
          <i data-toggle="tooltip" title="Hapus" class="fa fa-trash text-right  hover" onclick="Dashboard.deleteRoom('<?php echo $v_body['id'] ?>', 'room_body_detail')"></i>
          &nbsp;
          <i data-toggle="tooltip" title="Ubah" class="fa fa-pencil text-right  hover" onclick="Dashboard.editBodyDetail(this, '<?php echo $v_body['id'] ?>')"></i>               
          &nbsp;
         <?php } ?>
         <i data-toggle="tooltip" title="Detail" class="fa fa-file-text text-right  hover" onclick="Dashboard.detailTrans(this, '<?php echo $v_body['id'] ?>')"></i>
         &nbsp;
         
         <i data-toggle="tooltip" title="Suka" class="fa fa-thumbs-o-up text-primary text-right hover" onclick="Dashboard.like(this, '<?php echo $v_body['id'] ?>')"></i> &nbsp;<label class="text-primary" id="suka_label"><?php echo $v_body['total_like'] ?></label>
         &nbsp;
         <?php if ($v_body['file'] != '') { ?>
          <?php $file = str_replace(' ', '_', $v_body['file']); ?>
          <i nama_file="<?php echo $v_body['file'] ?>" file="<?php echo base_url() . 'files/berkas/document/' . $file ?>" data-toggle="tooltip" title="Download <?php echo $v_body['file'] ?>" class="fa fa-download text-right  hover" onclick="Dashboard.downloadBodyDetail(this, '<?php echo $v_body['id'] ?>')"></i>
         <?php } ?>
        </td>
       </tr>      
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="3" class="text-center">Tidak Ada Data Ditemukan</td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>