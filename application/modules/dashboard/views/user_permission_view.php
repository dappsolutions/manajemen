<input type="hidden" value="<?php echo $room_body ?>" id="room_body_id" class="form-control" />

<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;Daftar
     </div>
    </div>

    <div class="row">
     <div class="col-md-12">
      <div class="form-group">
       <div class="app-search">
        <input class="form-control" type="search" onkeyup="Dashboard.searchInTablePegawai(this, event)" id="keyword" placeholder="Pencarian">
        <button class="app-search__button"><i class="fa fa-search"></i></button>
       </div>
      </div>
     </div>
    </div>     

    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <div class="" style="height: 300px;overflow: auto;">
        <table class="table table-bordered" id="tb_pegawai">
         <thead>
          <tr class="table-warning">
           <th class="text-center">No</th>
           <th>NIP</th>
           <th>Nama</th>     
           <th>View
            <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
             <input type="checkbox" id="head_view_check" class="form-control" onchange="Dashboard.checkAllView(this)"/>
            <?php } ?>
           </th>     
           <th>Download
            <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
             <input type="checkbox" id="head_down_check" class="form-control" onchange="Dashboard.checkDownView(this)"/>
            <?php } ?>
           </th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($data_pegawai)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($data_pegawai as $value) { ?>
            <tr permission_id="<?php echo $value['permission_id'] ?>" user_id = "<?php echo $value['user_id'] ?>">
             <td class="text-center"><?php echo $no++ ?></td>
             <td><?php echo $value['nip'] ?></td>
             <td><?php echo $value['nama'] ?></td>  
             <td>
              <?php $checked = $value['view'] == 1 ? 'checked' : '' ?>
              <input type="checkbox" <?php echo $checked ?> id="view_check" 
                     class="form-control view_check" onchange="Dashboard.checkedView(this)"/>
             </td>  
             <td>
              <?php $checked = $value['download'] == 1 ? 'checked' : '' ?>
              <input type="checkbox" <?php echo $checked ?>  id="download_check" 
                     class="form-control download_check" onchange="Dashboard.checkedDown(this)"/>
             </td>  
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr class="text-center">
            <td colspan="8">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <div class="text-right">
       <button class="btn btn-warning hover-content" onclick="Dashboard.savePermissionUser(this)">Proses</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>