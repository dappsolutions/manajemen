<div class="row">
 <div class="col-md-12 text-right">  
  <?php if ($hak_akses == 'superadmin') { ?>
   <button class='btn btn-primary' onclick="Dashboard.addRoom()">Tambah Menu</button>
  <?php } ?>
 </div>
</div>
<br/>
<br/>
<?php if (!empty($list_menu)) { ?>
 <?php foreach ($list_menu as $value) { ?>
  <div class="row user" parent_room='<?php echo $value['id'] ?>' style="margin-top: -25px;">
   <?php $room_id = $value['id'] ?>
   <div class="col-md-3"><a class="mb-2 btn btn-primary btn-block" href="#"><?php echo ucwords($value['nama']) ?> 
     <?php if ($hak_akses == 'superadmin') { ?>
      <br/>
      &nbsp;&nbsp;&nbsp;&nbsp;<i data-toggle="tooltip" title="Hapus" class="fa fa-trash text-white hover" onclick="Dashboard.deleteRoom('<?php echo $value['id'] ?>', 'room')"></i> &nbsp;&nbsp;<i data-toggle="tooltip" title="Ubah" class="fa fa-pencil text-white hover" onclick="Dashboard.addRoom('<?php echo $value['nama'] ?>', '<?php echo $value['id'] ?>')"></i>
      &nbsp;&nbsp;<i data-toggle="tooltip" title="Permission User" class="fa fa-lock text-white hover" onclick="Dashboard.addPermissionRoom(this, '<?php echo $value['id'] ?>')"></i>
      &nbsp;&nbsp;<i data_id="<?php echo $value['id'] ?>" data-toggle="tooltip" title="Ubah Urutan" onclick="Dashboard.ubahUrutanRoom(this)" class="fa fa-sort grey-text fa-lg hover"></i>
     <?php } ?>
    </a>
    <div class="tile p-0">
     <h5 class="tile-title folder-head"><i class="fa fa-angle-up hover text-blue" data-toggle="tooltip" title="Minimize / Maximaize" onclick="Dashboard.minimizeLeft(this, '<?php echo $value['id'] ?>')"></i></h5>
     <div class="tile-body">
      <ul class="nav nav-pills flex-column mail-nav list_<?php echo $value['id'] ?>">
       <?php if (!empty($value['children'])) { ?>       
        <?php echo Modules::run('menu/buildMenuRoomHeader', $value['children'], 0, $value['id']) ?>
       <?php } ?>  
       <li class="nav-item">
        <?php if ($hak_akses == 'superadmin') { ?>
         <a class="nav-link text-right">
          <i room="<?php echo $value['id'] ?>" data-toggle="tooltip" title="Ubah Urutan" onclick="Dashboard.ubahUrutan(this)" class="fa fa-sort grey-text fa-lg hover"></i>
          &nbsp;
          <i data-toggle="tooltip" title="Tambah" onclick="Dashboard.addLeftMenu(this)" class="fa fa-plus grey-text fa-lg hover"></i>
         </a>
        <?php } ?>
       </li>  
      </ul>
     </div>
    </div>
   </div>
   <div class="col-md-9">
    <div class="tile" style="height: 300px;overflow: auto;">
     <div class="mailbox-controls">
      <div class="animated-checkbox">
       <label>
        <?php // if (!empty($value['body'])) { ?>
        <i class="fa fa-check-circle-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;
        <b  id="judul-<?php echo $room_id ?>"><?php echo ucwords(current($value['children'])['title']) ?></b>
        <?php // } ?>        
       </label>
      </div>
      <div class="btn-group">
  <!--       <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-trash-o"></i></button>
       <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-reply"></i></button>
       <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-share"></i></button>-->
       <button class="btn btn-primary btn-sm" type="button">
        <i room="<?php echo $room_id ?>" data-toggle="tooltip" title="Minimize / Maximize" 
           class="fa fa-angle-down" 
           onclick="Dashboard.minimizeBody(this)"></i></button>
      </div>
     </div>
     <div class="table-responsive mailbox-messages-<?php echo $value['id'] ?>">      
      <table class="table no-border" id="tb_body-<?php echo $value['id'] ?>">
       <tbody>
        <?php if (!empty($value['body'])) { ?>
         <?php foreach ($value['body'] as $v_body) { ?>
          <?php
          $is_view = true;
          if ($hak_akses == "superadmin") {
           $is_view = true;
          } else {
           if (isset($v_body['view'])) {
            if ($v_body['view'] == 1) {
             $is_view = true;
            } else {
             $is_view = false;
            }
           }
          }
          ?>
          <?php if ($is_view) { ?>
           <tr link="<?php echo $v_body['url'] ?>" class="" data_id='<?php echo $v_body['id'] ?>' id="content" 
               onclick="Dashboard.gotoLinkDoc(this)">
            <td class="no-border">
             <div class="animated-checkbox">
              <label>
               <i data-toggle="tooltip" title="<?php echo $v_body['pesan'] == '' ? 'Tidak ada keterangan' : $v_body['pesan'] ?>" class="hover fa fa-file-text-o"></i>
               &nbsp;&nbsp;&nbsp;<?php echo $v_body['title'] ?>
              </label>
             </div>
            </td>
            <td class="mail-subject no-border">&nbsp;</td>
            <td class="no-border"><a href="#">&nbsp;</a></td>
            <td class="no-border">&nbsp;</td>
            <td class="no-border">&nbsp;</td>
            <td class="text-right no-border">
             <i data-toggle="tooltip" title="Komentar" 
                class="fa fa-comment-o text-primary text-right hover" 
                title_body="<?php echo $v_body['title'] ?>"
                onclick="Dashboard.commentDocument(this, '<?php echo $v_body['id'] ?>')"></i>
             &nbsp;
             <?php if ($hak_akses == 'superadmin') { ?>
              <i data-toggle="tooltip" title="Permission User" class="fa fa-lock text-primary text-right  hover" onclick="Dashboard.addPermissionBody(this, '<?php echo $v_body['id'] ?>')"></i>
              &nbsp;
              <i data-toggle="tooltip" title="Hapus" class="fa fa-trash text-primary text-right hover" onclick="Dashboard.deleteRoom('<?php echo $v_body['id'] ?>', 'room_body')"></i>
              &nbsp;
              <i data-toggle="tooltip" title="Ubah" class="fa fa-pencil text-primary text-right  hover" onclick="Dashboard.editBody(this, '<?php echo $v_body['id'] ?>')"></i>
              &nbsp;
             <?php } ?>            
             <i data-toggle="tooltip" title="Suka" class="fa fa-thumbs-o-up text-primary text-right hover" onclick="Dashboard.like(this, '<?php echo $v_body['id'] ?>')"></i> &nbsp;<label class="text-primary" id="suka_label"><?php echo $v_body['total_like'] ?></label>
             &nbsp;
             <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
              <i data_id="<?php echo $v_body['id'] ?>" data-toggle="tooltip" title="Ubah Urutan" onclick="Dashboard.ubahUrutanBody(this)" class="fa fa-sort text-success fa-lg hover"></i>
              &nbsp;
             <?php } ?>     
             <?php if ($v_body['file'] != '') { ?>
              <?php if (isset($v_body['download'])) { ?>                           
               <?php $file = str_replace(' ', '_', $v_body['file']); ?>
               <?php if ($v_body['view'] == 1) { ?>
                <i tipe="<?php echo $v_body['tipe_upload'] ?>" nama_file="<?php echo $v_body['file'] ?>" file="<?php echo $file ?>" onclick="Dashboard.showFileRoomHeader(this)" class="fa fa-file-archive-o text-primary text-right hover"></i>
               <?php } ?>
               <?php if ($v_body['download'] == 1) { ?>
                &nbsp;
                <i nama_file="<?php echo $v_body['file'] ?>" file="<?php echo base_url() . 'files/berkas/document/' . $file ?>" data-toggle="tooltip" title="Download <?php echo $v_body['file'] ?>" onclick="Dashboard.downloadBodyDetail(this, '<?php echo $v_body['id'] ?>')" class="fa fa-download text-primary text-right hover"></i>
               <?php } ?>
              <?php } else { ?>
               <?php $file = str_replace(' ', '_', $v_body['file']); ?>
               <i tipe="<?php echo $v_body['tipe_upload'] ?>" nama_file="<?php echo $v_body['file'] ?>" file="<?php echo $file ?>" onclick="Dashboard.showFileRoomHeader(this)" class="fa fa-file-archive-o text-primary text-right hover"></i>
               &nbsp;
               <i nama_file="<?php echo $v_body['file'] ?>" file="<?php echo base_url() . 'files/berkas/document/' . $file ?>" data-toggle="tooltip" title="Download <?php echo $v_body['file'] ?>" onclick="Dashboard.downloadBodyDetail(this, '<?php echo $v_body['id'] ?>')" class="fa fa-download text-primary text-right hover"></i>
              <?php } ?>
             <?php } ?>
            </td>
           </tr>
          <?php } ?>          
         <?php } ?>
        <?php } else { ?>    
         <tr>
          <td class="text-center no-border" colspan="4">Data Tidak Ditemukan</td>
         </tr>
        <?php } ?>
       </tbody>
      </table>
     </div>
     <div class="text-right">
  <!--      <span class="text-muted mr-2">Showing 1-15 out of 60</span>-->
      <div class="btn-group" id="add_body-<?php echo $room_id ?>">
       <?php if ($hak_akses == 'superadmin') { ?>
        <?php if (!empty($value['body'])) { ?>
         <button id="btn_add_body" id_parent="'<?php echo $value['body'][count($value['body']) - 1]['room_header'] ?>'" onclick="Dashboard.addBody(this, '<?php echo $value['body'][count($value['body']) - 1]['room_header'] ?>')" class="btn btn-warning btn-sm" type="button"><i class="fa fa-fw fa-lg fa-plus hover"></i>Tambah</button>
        <?php } else { ?>
         <button id="btn_add_body" id_parent="'<?php echo $value['room_header'] ?>'" onclick="Dashboard.addBody(this, '<?php echo $value['room_header'] ?>')" class="btn btn-warning btn-sm" type="button"><i class="fa fa-fw fa-lg fa-plus hover"></i>Tambah</button>
        <?php } ?>
       <?php } ?>
      </div>
     </div>
    </div>
   </div>
  </div>
 <?php } ?>
<?php } ?>