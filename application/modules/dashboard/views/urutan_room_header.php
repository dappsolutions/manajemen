<input type="hidden" value="<?php echo $room ?>" id="room" class="form-control" />
<div class="row">
 <div class="col-md-12">
  <div class="box box-solid box-primary">
   <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-file-text-o"></i> &nbsp;Ubah Urutan
   </div>
  </div>
  <div class="tile">
   <div class="tile-body">
    <div class="table-responsive">
     <table class="table table-bordered" id="tb_header_room">
      <thead>
       <tr class="table-warning">
        <th>Menu</th>
        <th>Anggota Menu</th>
       </tr>
      </thead>
      <tbody>
       <?php //echo Modules::run('menu/buildSortTableMenu', $result); ?>
       <?php if (!empty($result)) { ?>
        <?php foreach ($result as $value) { ?>
         <tr data_id="<?php echo $value['id'] ?>">
          <td>
           <i class="fa fa-sort fa-lg"></i>
           &nbsp;&nbsp;
           <?php echo $value['title'] ?>
          </td>
          <td>
           <?php if (!empty($value['children'])) { ?>
            <i room="<?php echo $value['room'] ?>" data_toggle="tooltip" title="Ubah Urutan" class="fa fa-sort fa-lg hover" onclick="Dashboard.ubahUrutan(this, '<?php echo $value['id'] ?>')"></i>
            &nbsp;
            Ubah Urutan
           <?php } ?>
          </td>
         </tr>
        <?php } ?>
       <?php } else { ?>
        <tr>
         <td colspan="3" class="text-center">Tidak ada data ditemukan</td>
        </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>

   <div class="tile-footer text-right">
    <button class="btn btn-warning" onclick="Dashboard.updateHeader()">Proses</button>
   </div>
  </div>
 </div>
</div>