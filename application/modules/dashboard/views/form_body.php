<input type='hidden' name='' id='id_body' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12" style="padding: 16px;">
  <div class="row">
   <div class="col-md-12">
    <h4>Form Body</h4>
    <hr/>
    <br/>
    <form class="form-horizontal">
     <div class="form-group">
      <label class="control-label col-sm-1">Judul</label>
      <div class="col-sm-12">
       <input class="form-control required" error="Judul" id="title-body" type="text" placeholder="Masukkan Judul" value="<?php echo isset($title) ? $title : '' ?>">
      </div>        
     </div>

     <div class="form-group">
      <label class="control-label col-sm-3">Tipe Upload</label>
      <div class="col-sm-12">
       <select class="form-control required" error="Upload" id='tipe_upload' onchange="Dashboard.changeTipeUpload(this)">
        <option value="no_upload">Tidak Ada Upload</option>
        <?php if (!empty($list_upload)) { ?>
         <?php foreach ($list_upload as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($tipe_upload)) { ?>
           <?php $selected = $tipe_upload == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tipe'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <?php $hide_content = 'hidden-content' ?>
     <?php if (isset($tipe_upload)) { ?>
      <?php $hide_content = $tipe_upload == 1 || $tipe_upload == 2 ? '' : 'hidden-content' ?>
     <?php } ?>
     <div class="form-group <?php echo $hide_content ?>" id='div_upload'>
      <label class="control-label col-sm-3">File Upload</label>
      <div class="col-sm-12">   
       <?php if (isset($tipe_upload)) { ?>
        <?php if ($file == '') { ?>
         <input class="form-control" type="file" id='file' value="<?php echo isset($tipe_upload) ? $file : '' ?>">
         <br/>
        <?php } else { ?>
         <input class="form-control hidden-content" type="file" id='file' value="<?php echo isset($tipe_upload) ? $file : '' ?>">
        <?php } ?>
       <?php } else { ?>       
        <input class="form-control" type="file" id='file' value="<?php echo isset($tipe_upload) ? $file : '' ?>">
       <?php } ?>       
       <?php if (isset($tipe_upload)) { ?>
        <?php if ($file != '') { ?>
         <div class="input-group">        
          <input type="text" id="" class="form-control" value="<?php echo $file ?>"/>
          <div class="input-group-append">
           <span class="input-group-text">          
            <i class="fa fa-close hover" onclick="Dashboard.changeFileUpload(this)"></i>
           </span>
          </div>
         </div>     
        <?php } ?>
       <?php } ?>
      </div>
     </div>

     <?php $hide_content = 'hidden-content' ?>
     <?php if (isset($tipe_upload)) { ?>
      <?php $hide_content = $tipe_upload == 3 ? '' : 'hidden-content' ?>
     <?php } ?>
     <div class="form-group <?php echo $hide_content ?>" id="div_url">
      <label class="control-label col-sm-3">Link Url</label>
      <div class="col-sm-12">
       <input class="form-control" type="text" id='url' placeholder="Link Url" value="<?php echo isset($url) ? $url : '' ?>">
      </div>
     </div>

     <div class="form-group">
      <label class="control-label col-sm-2">Pesan</label>
      <div class="col-sm-12">
       <input class="form-control" type="text" id='pesan' placeholder="Pesan" value="<?php echo isset($pesan) ? $pesan : '' ?>">
      </div>
     </div>
    </form>
    <!--<div class="col-sm-6">-->    
   </div>
   <div class="col-md-12 text-right">
    <button class="btn btn-warning" type="button" 
            onclick="Dashboard.simpanBody(this, '<?php echo isset($room_header) ? $room_header : '' ?>')">
     <i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan
    </button>&nbsp;&nbsp;&nbsp;
    <a class="btn btn-secondary text-white" onclick="message.closeDialog()">
     <i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
   </div>
  </div>
 </div>
</div>
