<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type="hidden" value="<?php echo $body['id'] ?>" id="room_body" class="form-control" />
<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text"></i>&nbsp;&nbsp;&nbsp; <?php echo 'Create Table  ' . ucwords($body['title']) ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-3">Nama Tabel</label>
        <div class="col-sm-5">
         <input class="form-control required" error="Nama Tabel" id="nama_tabel" type="text" placeholder="Nama Tabel" value="">
        </div>        
       </div>
       <hr/>
       <div class="form-group">
        <label class="control-label col-sm-5">Masukkan Kolom Tabel</label>
        <div class="col-sm-5">
         <table class="table table-bordered" id="tb_field">
          <thead>
           <tr class="table-warning">
            <th>Nama Kolom</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td>
             <input type="text" value="" id="nama_kolom" class="form-control" placeholder="Nama Kolom"/>
            </td>
            <td class="text-center">
             <i class="fa fa-plus fa-lg hover" onclick="Dashboard.addField(this)"></i>
            </td>
           </tr>
          </tbody>
         </table>

        </div>
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" 
              onclick="Dashboard.simpanTable('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Dashboard.back()">
       <i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
