
<div class="row">
 <div class="col-md-6 col-lg-3">
  <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
   <div class="info">
    <h4>Users</h4>
    <p><b><?php echo $total_user ?></b></p>
   </div>
  </div>
 </div>
 <div class="col-md-6 col-lg-3">
  <div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-up fa-3x"></i>
   <div class="info">
    <h4>Likes</h4>
    <p><b><?php echo $total_like ?></b></p>
   </div>
  </div>
 </div>
 <div class="col-md-6 col-lg-3">
  <div class="widget-small warning coloured-icon"><i class="icon fa fa-file fa-3x"></i>
   <div class="info">
    <h4>Uploades</h4>
    <p><b><?php echo $total_upload ?></b></p>
   </div>
  </div>
 </div>
 <div class="col-md-6 col-lg-3">
  <div class="widget-small danger coloured-icon"><i class="icon fa fa-user-o fa-3x"></i>
   <div class="info">
    <h4>Pegawai</h4>
    <p><b><?php echo $total_pegawai ?></b></p>
   </div>
  </div>
 </div>
</div>