
<div class="row">
 <div class="col-md-12 text-right">  
  <?php if ($hak_akses == 'superadmin') { ?>
   <button class='btn btn-primary' onclick="Dashboard.addRoom()">Tambah Menu</button>
  <?php } ?>
 </div>
</div>
<?php if (!empty($list_menu)) { ?>
 <?php foreach ($list_menu as $value) { ?>
  <div class="row user" parent_room='<?php echo $value['id'] ?>'>
   <div class="col-md-3 radius">    
    <div class="tile p-0">   
     <ul class="nav flex-column nav-tabs user-tabs">
      <li class="nav-item" style="border-bottom: 1px solid #ccc;"> 
       <div class="box box-solid box-primary">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
         <i class="fa fa-file-text"></i> <?php echo ucwords($value['nama']) ?>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <?php if ($hak_akses == 'superadmin') { ?>
          <i class="fa fa-trash text-white hover" onclick="Dashboard.deleteRoom('<?php echo $value['id'] ?>', 'room')"></i> &nbsp;&nbsp;<i class="fa fa-pencil text-white hover" onclick="Dashboard.addRoom('<?php echo $value['nama'] ?>', '<?php echo $value['id'] ?>')"></i>
         <?php } ?>
        </div>
       </div>
      </li>
      <?php if (!empty($value['children'])) { ?>       
       <?php $i = 0; ?>
       <?php foreach ($value['children'] as $v_head) { ?>
        <?php $active = $i == 0 ? 'active' : '' ?>
        <li room_name='<?php echo $v_head['title'] ?>' room='<?php echo $value['id'] ?>' data_id='<?php echo $v_head['id'] ?>' onclick="Dashboard.detailBody(this)" class="nav-item"><a href="head-<?php echo $v_head['id'] ?>" class="nav-link <?php echo $active ?>" data-toggle='tab'><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;&nbsp;<?php echo $v_head['title'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <?php if ($hak_akses == 'superadmin') { ?>
           <i class="fa fa-trash text-primary hover" onclick="Dashboard.deleteRoom('<?php echo $v_head['id'] ?>', 'room_header')"></i> &nbsp;&nbsp;<i class="fa fa-pencil text-primary hover" onclick="Dashboard.editLeftMenu(this)"></i>
          <?php } ?>
         </a>         
        </li>
        <?php $i += 1; ?>
       <?php } ?>      
      <?php } ?>      
      <li class="nav-item">
       <?php if ($hak_akses == 'superadmin') { ?>
        <a class="nav-link text-right"><i onclick="Dashboard.addLeftMenu(this)" class="fa fa-plus grey-text fa-lg hover"></i></a>
       <?php } ?>
      </li>
     </ul>
    </div>
   </div>
   <div class="col-md-9">    
    <div class="tab-content">     
     <?php if (!empty($value['body'])) { ?>      
      <div class="tab-pane active" id="content-<?php echo $value['id'] ?>">
       <div class="box box-solid box-primary">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
         <i class="fa fa-file-text"></i>&nbsp;&nbsp;&nbsp; <?php echo ucwords($value['children'][0]['title']) ?>
        </div>
       </div>
       <div class="timeline-post radius">        
        <div class="post-content">
         <div class="table-responsive border-grey-color">
          <table class="table">
           <tbody>
            <?php foreach ($value['body'] as $v_body) { ?>
             <tr class="hover-content" data_id='<?php echo $v_body['id'] ?>'>
              <td onclick="Dashboard.showDetail(this)" style="border:none" class="text-left"><i class="fa fa-file-text-o" style="margin-right: 16px;"></i><?php echo $v_body['title'] ?></td>              
              <td style="border:none" class="text-right">
               <?php if ($hak_akses == 'superadmin') { ?>
                <i class="fa fa-trash text-primary text-right hover" onclick="Dashboard.deleteRoom('<?php echo $v_body['id'] ?>', 'room_body')"></i>
                &nbsp;
                <i class="fa fa-pencil text-primary text-right  hover" onclick="Dashboard.editBody(this, '<?php echo $v_body['id'] ?>')"></i>
                &nbsp;
                <i class="fa fa-file-text text-warning text-right hover" onclick="Dashboard.detail_trans(this, '<?php echo $v_body['id'] ?>')"></i>
               <?php } ?>               
              </td>
             </tr>
            <?php } ?>
           </tbody>
          </table>
         </div>
        </div>
        <ul class="post-utility">      
         <?php if ($hak_akses == 'superadmin') { ?>
          <li class="comments"><i class="fa fa-fw fa-lg fa-plus hover" onclick="Dashboard.addBody(this, '<?php echo $value['body'][count($value['body']) - 1]['room_header'] ?>')"></i>  Tambah</li>
         <?php } ?>
        </ul>
       </div>
      </div>
     <?php } else { ?>
      <div class="tab-pane active" id="user-timeline">
       <div class="timeline-post">
        <h4><?php echo 'DATA' ?></h4>
        <hr/>
        <br/>
        <div class="post-content text-center">
         <p><i class="fa fa-file-text"></i>&nbsp;&nbsp;&nbsp;Tidak Ada Data Ditemukan</p>
        </div>
        <?php if (!empty($value['children'])) { ?>
         <ul class="post-utility">      
          <?php if ($hak_akses == 'superadmin') { ?>
           <li class="comments"><i class="fa fa-fw fa-lg fa-plus hover" onclick="Dashboard.addBody(this, '<?php echo!empty($value['children']) ? $value['children'][0]['id'] : '' ?>')"></i>  Tambah</li>
          <?php } ?>
         </ul>
        <?php } ?>
       </div>
      </div>
     <?php } ?>
    </div>
   </div>
  </div>
  <div class="divider">

  </div>
 <?php } ?>
<?php } ?>