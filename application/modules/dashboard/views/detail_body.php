<?php if (!empty($body)) { ?>
 <?php foreach ($body as $v_body) { ?>
  <?php
  $is_view = true;
  if ($this->session->userdata('hak_akses') == "superadmin") {
   $is_view = true;
  } else {
   if (isset($v_body['view'])) {
    if ($v_body['view'] == 1) {
     $is_view = true;
    } else {
     $is_view = false;
    }
   }
  }
  ?>
  <?php if ($is_view) { ?>
   <tr link="<?php echo $v_body['url'] ?>" class="" 
       data_id='<?php echo $v_body['id'] ?>' id="content" 
       onclick="Dashboard.gotoLinkDoc(this)">
    <td class="no-border">
     <div class="animated-checkbox">
      <label>
       <i data-toggle="tooltip" title="<?php echo $v_body['pesan'] == '' ? 'Tidak ada keterangan' : $v_body['pesan'] ?>" class="fa fa-file-text-o"></i>
       &nbsp;&nbsp;&nbsp;<?php echo $v_body['title'] ?>
      </label>
     </div>
    </td>
    <td class="mail-subject no-border">&nbsp;</td>
    <td class='no-border'><a href="#">&nbsp;</a></td>
    <td class='no-border'>&nbsp;</td>
    <td class="no-border">&nbsp;</td>
    <td class="text-right no-border">
     <i data-toggle="tooltip" title="Komentar" 
        class="fa fa-comment-o text-primary text-right hover" 
        title_body="<?php echo $v_body['title'] ?>"
        onclick="Dashboard.commentDocument(this, '<?php echo $v_body['id'] ?>')"></i>
     &nbsp;
     <?php if ($hak_akses == 'superadmin') { ?>
      <i data-toggle="tooltip" title="Permission User" class="fa fa-lock text-primary text-right  hover" onclick="Dashboard.addPermissionBody(this, '<?php echo $v_body['id'] ?>')"></i>
      &nbsp;
      <i data-toggle="tooltip" title="Hapus" class="fa fa-trash text-primary text-right hover" onclick="Dashboard.deleteRoom('<?php echo $v_body['id'] ?>', 'room_body')"></i>
      &nbsp;
      <i data-toggle="tooltip" title="Ubah" class="fa fa-pencil text-primary text-right  hover" onclick="Dashboard.editBody(this, '<?php echo $v_body['id'] ?>')"></i>
      &nbsp;
     <?php } ?>    
     <i data-toggle="tooltip" title="Suka" class="fa fa-thumbs-o-up text-primary text-right hover" onclick="Dashboard.like(this, '<?php echo $v_body['id'] ?>')"></i> &nbsp;<label class="text-primary"><?php echo $v_body['total_like'] ?></label>
     &nbsp;
     <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
      <i data_id="<?php echo $v_body['id'] ?>" data-toggle="tooltip" title="Ubah Urutan" onclick="Dashboard.ubahUrutanBody(this)" class="fa fa-sort text-success fa-lg hover"></i>
      &nbsp;
     <?php } ?>     
     <?php if ($v_body['file'] != '') { ?>
      <?php if (isset($v_body['download'])) { ?>                           
       <?php $file = str_replace(' ', '_', $v_body['file']); ?>
       <?php if ($v_body['view'] == 1) { ?>
        <i tipe="<?php echo $v_body['tipe_upload'] ?>" nama_file="<?php echo $v_body['file'] ?>" file="<?php echo $file ?>" onclick="Dashboard.showFileRoomHeader(this)" class="fa fa-file-archive-o text-primary text-right hover"></i>
       <?php } ?>
       <?php if ($v_body['download'] == 1) { ?>
        &nbsp;
        <i nama_file="<?php echo $v_body['file'] ?>" file="<?php echo base_url() . 'files/berkas/document/' . $file ?>" data-toggle="tooltip" title="Download <?php echo $v_body['file'] ?>" onclick="Dashboard.downloadBodyDetail(this, '<?php echo $v_body['id'] ?>')" class="fa fa-download text-primary text-right hover"></i>
       <?php } ?>
      <?php } else { ?>
       <?php $file = str_replace(' ', '_', $v_body['file']); ?>
       <i tipe="<?php echo $v_body['tipe_upload'] ?>" nama_file="<?php echo $v_body['file'] ?>" file="<?php echo $file ?>" onclick="Dashboard.showFileRoomHeader(this)" class="fa fa-file-archive-o text-primary text-right hover"></i>
       &nbsp;
       <i nama_file="<?php echo $v_body['file'] ?>" file="<?php echo base_url() . 'files/berkas/document/' . $file ?>" data-toggle="tooltip" title="Download <?php echo $v_body['file'] ?>" onclick="Dashboard.downloadBodyDetail(this, '<?php echo $v_body['id'] ?>')" class="fa fa-download text-primary text-right hover"></i>
      <?php } ?>
     <?php } ?>
    </td>
   </tr>
  <?php } ?>
 <?php } ?>
<?php } else { ?>    
 <tr>
  <td class="text-center no-border" colspan="4">Data Tidak Ditemukan</td>
 </tr>
<?php } ?>