<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type="hidden" value="<?php echo $parent ?>" id="parent" class="form-control" />
<input type="hidden" value="<?php echo $room ?>" id="room" class="form-control" />
<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <h4>Input</h4>
    <hr/>
    <div class="tile-body">
     <form class="form-horizontal">
      <div class="form-group">
       <label class="control-label col-sm-1">Judul</label>
       <div class="col-sm-5">
        <input class="form-control required" error="Judul" id="judul" type="text" placeholder="Masukkan Judul" value="">
       </div>        
      </div>
      <div class="form-group">
       <label class="control-label col-sm-2">Tipe Upload</label>
       <div class="col-sm-5">
        <select class="form-control required" error="Upload" id='tipe_upload' onchange="Dashboard.changeTipeUpload(this)">
         <option value="no_upload">Tidak Ada Upload</option>
         <?php if (!empty($list_upload)) { ?>
          <?php foreach ($list_upload as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['tipe'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </div>
      </div>

      <div class="form-group hidden-content" id='div_upload'>
       <label class="control-label col-sm-2">File Upload</label>
       <div class="col-sm-5">
        <input class="form-control" type="file" id='file'>
       </div>
      </div>
     </form>
    </div>
    <div class="tile-footer text-right">
     <!--<div class="col-sm-6">-->
     <button class="btn btn-warning" type="button" onclick="Dashboard.simpanRoomHeader('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="message.closeDialog()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
     <!--</div>-->      
    </div>
   </div>
  </div>
 </div>
</div>
