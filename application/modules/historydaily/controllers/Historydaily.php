<?php

class Historydaily extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $jabatan_id;
 public $struktur_id;
 public $pegawai_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 200;
  $this->akses = $this->session->userdata('hak_akses');
  $this->jabatan_id = $this->session->userdata('jabatan_id');
  $this->struktur_id = $this->session->userdata('struktur_id');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'historydaily';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/historydaily.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai_job_desk';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Historydaily";
  $data['title_content'] = 'Historydaily';
  $content = $this->getDataHistorydaily();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getJabatanHasChild() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.parent = '" . $this->struktur_id . "'"
  ));

  $is_has = false;
  if (!empty($data)) {
   $is_has = true;
  } else {
   if ($this->akses == 'superadmin') {
    $is_has = true;
   }
  }

  return $is_has;
 }

 public function getTotalDataHistorydaily($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'Superadmin') {
   $where = "pjd.deleted = 0 and jds.status = 'APPROVED'";
  } else {
   $where = "pjd.deleted = 0 and ((ut.id = '" . $user_id . "') or (u.id = '" . $user_id . "')) and (jds.status = 'APPROVED' or jds.status = 'CLOSED')";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataHistorydaily($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "pjd.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pjd.deleted = 0 and jds.status = 'APPROVED'";
  } else {
   $where = "pjd.deleted = 0 and ((ut.id = '" . $user_id . "') or (u.id = '" . $user_id . "')) and (jds.status = 'APPROVED' or jds.status = 'CLOSED')";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  $result['januari'] = array();
  $result['february'] = array();
  $result['maret'] = array();
  $result['april'] = array();
  $result['mei'] = array();
  $result['juni'] = array();
  $result['juli'] = array();
  $result['agustus'] = array();
  $result['september'] = array();
  $result['oktober'] = array();
  $result['november'] = array();
  $result['desember'] = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {    
    $month = intval(date('m', strtotime($value['tanggal'])));
    switch ($month) {
     case 1:
      array_push($result['januari'], $value);
      break;
     case 2:
      array_push($result['februari'], $value);
      break;
     case 3:
      array_push($result['maret'], $value);
      break;
     case 4:
      array_push($result['april'], $value);
      break;
     case 5:
      array_push($result['mei'], $value);
      break;
     case 6:
      array_push($result['juni'], $value);
      break;
     case 7:
      array_push($result['juli'], $value);
      break;
     case 8:
      array_push($result['agustus'], $value);
      break;
     case 9:
      array_push($result['september'], $value);
      break;
     case 10:
      array_push($result['oktober'], $value);
      break;
     case 11:
      array_push($result['november'], $value);
      break;
     case 12:
      array_push($result['desember'], $value);
      break;

     default:
      break;
    }
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataHistorydaily($keyword)
  );
 }

 public function getDetailDataHistorydaily($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pjd',
              'field' => array('pjd.*', 'p.nama as nama_pegawai',
                  'p.nip', 'pg.nama as nama_pemberi',
                  'jds.status', 'jd.jenis', 'ut.id as user_pegawai'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user ut', 'ut.pegawai = p.id'),
                  array('user u', 'pjd.giver = u.id', 'left'),
                  array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                  array('jabatan j', 'pg.jabatan = j.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
                  array('jenis_job jd', 'pjd.jenis_job = jd.id'),
              ),
              'where' => "pjd.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['term_pegawai'] = $value['nip'] . ' - ' . $value['nama'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['has_child'] = $this->getJabatanHasChild();
  $data['pegawai_id'] = $this->pegawai_id;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Historydaily";
  $data['title_content'] = 'Tambah Historydaily';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data['has_child'] = $this->getJabatanHasChild();
  $data = $this->getDetailDataHistorydaily($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Historydaily";
  $data['title_content'] = 'Ubah Historydaily';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataHistorydaily($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Historydaily";
  $data['title_content'] = "Detail Historydaily";
  $data['user_id'] = $this->session->userdata('user_id');
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getPostDataHistorydaily($value) {
  if ($value->pegawai != '') {
   $data['giver'] = $this->session->userdata('user_id');
   $data['pegawai'] = $value->pegawai;
   $data['jenis_job'] = '1';
  } else {
   $data['pegawai'] = $this->pegawai_id;
   $data['jenis_job'] = '1';
   //semua bawaha maka nanit harus dicari bawahan dari yang memberi pekerjaan lalu di insert ke pegawai
  }
  $data['keterangan'] = $value->keterangan;
  $data['tanggal'] = $value->tanggal;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataHistorydaily($data->jobdesk);
//   echo '<pre>';
//   print_r($post);
//   die;
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
    $js['pegawai_job_desk'] = $id;
    $js['status'] = 'DRAFT';
    Modules::run('database/_insert', 'job_desk_status', $js);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function doneTask() {
  $id = $this->input->post('id');
  $status = $this->input->post('status');
  $is_valid = false;
  switch ($status) {
   case 'DRAFT':
    $status = 'ONPROGRESS';
    break;
   case 'ONPROGRESS':
    $status = 'CLOSED';
    break;
   case 'CLOSED':
    $status = 'APPROVED';
    break;
   default:
    break;
  }

  $this->db->trans_begin();
  try {
   $post['pegawai_job_desk'] = $id;
   $post['status'] = $status;
   Modules::run('database/_insert', 'job_desk_status', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Historydaily";
  $data['title_content'] = 'Data Historydaily';
  $content = $this->getDataHistorydaily($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
