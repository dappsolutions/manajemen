<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">

     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" type="search" onkeyup="Historydaily.search(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>  

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="table-warning">
            <th>No</th>
            <th>NIP</th>
            <th>Nama Pegawai</th>
            <th>Jenis Tugas</th>
            <th>Pemberi Tugas</th>
            <th>Status</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $key => $v_head) { ?>
             <tr class="hover-content" data_class="<?php echo $key ?>" onclick="">
              <td colspan="7" class="bg-primary">
               <i class="fa fa-angle-down text-white"></i>
               &nbsp;<b class="text-white"><?php echo ucfirst($key) ?></b>
              </td>
             </tr>
             <?php if (!empty($v_head)) { ?>
              <?php foreach ($v_head as $value) { ?>
               <tr class="<?php echo $key ?>">
                <td><?php echo $no++ ?></td>
                <td><?php echo $value['nip'] ?></td>
                <td><?php echo $value['nama_pegawai'] ?></td>
                <td><?php echo $value['jenis'] ?></td>
                <td>
                 <?php if ($value['giver'] != '') { ?>
                  <?php echo $value['nama_pemberi'] == '' ? $value['username'] : $value['nama_pemberi'] ?>
                 <?php } ?>
                </td>
                <td><?php echo $value['status'] ?></td>
                <td class="text-center">
                 <i class="fa fa-file-text grey-text hover" onclick="Historydaily.detail('<?php echo $value['id'] ?>')"></i>
                </td>
               </tr>
              <?php } ?>
             <?php } else { ?>
              <tr class="text-center">
               <td colspan="7">Tidak Ada Data Ditemukan</td>
              </tr>
             <?php } ?>
            <?php } ?>
           <?php } else { ?>
            <tr class="text-center">
             <td colspan="7">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div> 
   </div>
  </div>
 </div>
</div>
