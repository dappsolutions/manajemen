<?php

class Grafikdaily extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $jabatan_id;
 public $struktur_id;
 public $pegawai_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->jabatan_id = $this->session->userdata('jabatan_id');
  $this->struktur_id = $this->session->userdata('struktur_id');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'grafikdaily';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/chart.js"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/grafikdaily.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai_job_desk';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Grafik Dailytask";
  $data['title_content'] = 'Grafik Dailytask';
  $content = $this->getDataGrafikdaily();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getJabatanHasChild() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.parent = '" . $this->struktur_id . "'"
  ));

  $is_has = false;
  if (!empty($data)) {
   $is_has = true;
  } else {
   if ($this->akses == 'superadmin') {
    $is_has = true;
   }
  }

  return $is_has;
 }

 public function getTotalDataGrafikdaily($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pjd.deleted = 0 and u.id = '" . $user_id . "'";
  } else {
   $where = "pjd.deleted = 0 and (u.id = '" . $user_id . "') and (jds.status != 'APPROVED' and jds.status != 'CLOSED')";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataGrafikdaily($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "pjd.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pjd.deleted = 0 and u.id = '" . $user_id . "'";
  } else {
   $where = "pjd.deleted = 0 and (u.id = '" . $user_id . "') and (jds.status != 'APPROVED' and jds.status != 'CLOSED')";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataGrafikdaily($keyword)
  );
 }

 public function getDetailDataGrafikdaily($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pjd',
              'field' => array('pjd.*', 'p.nama as nama_pegawai',
                  'p.nip', 'pg.nama as nama_pemberi',
                  'jds.status', 'jd.jenis', 'ut.id as user_pegawai'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user ut', 'ut.pegawai = p.id'),
                  array('user u', 'pjd.giver = u.id', 'left'),
                  array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                  array('jabatan j', 'pg.jabatan = j.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
                  array('jenis_job jd', 'pjd.jenis_job = jd.id'),
              ),
              'where' => "pjd.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function detail($id) {
  $data = $this->getDetailDataGrafikdaily($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Grafik Dailytask";
  $data['title_content'] = "Detail Grafik Dailytask";
  $data['user_id'] = $this->session->userdata('user_id');
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Grafikdaily";
  $data['title_content'] = 'Data Grafikdaily';
  $content = $this->getDataGrafikdaily($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getDataGrafik() {
  $sql = "select 
	pjd.id
	, pjd.tanggal as tgl_buat
	, pjd.time_target
	, jds.status
	, jds.createddate as tgl_selesai
	from pegawai_job_desk pjd
	join (select max(id) as id, pegawai_job_desk from job_desk_status GROUP by pegawai_job_desk) jss
		on jss.pegawai_job_desk = pjd.id
	join job_desk_status jds 
		on jss.id = jds.id
	where jds.status = 'CLOSED'";
  $data = Modules::run('database/get_custom', $sql);
  $result = array();
  $total_januari = 0;
  $total_januari_off = 0;
  $total_feb = 0;
  $total_feb_off = 0;
  $total_mar = 0;
  $total_mar_off = 0;
  $total_apr = 0;
  $total_apr_off = 0;
  $total_mei = 0;
  $total_mei_off = 0;
  $total_jun = 0;
  $total_jun_off = 0;
  $total_jul = 0;
  $total_jul_off = 0;
  $total_ags = 0;
  $total_ags_off = 0;
  $total_sep = 0;
  $total_sep_off = 0;
  $total_okt = 0;
  $total_okt_off = 0;
  $total_nov = 0;
  $total_nov_off = 0;
  $total_des = 0;
  $total_des_off = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    list($day, $month, $year) = explode('-', $value['tgl_buat']);

    $time_target = intval(str_replace('-', '', $value['time_target']));
    $tgl_selesai = intval(str_replace('-', '', $value['tgl_selesai']));
    switch ($month) {
     case "01":
      if ($tgl_selesai > $time_target) {
       $total_januari_off += 1;
      } else {
       $total_januari += 1;
      }
      break;
     case "02":
      if ($tgl_selesai > $time_target) {
       $total_feb_off += 1;
      } else {
       $total_feb += 1;
      }
      break;
     case "03":
      if ($tgl_selesai > $time_target) {
       $total_mar_off += 1;
      } else {
       $total_mar += 1;
      }
      break;
     case "04":
      if ($tgl_selesai > $time_target) {
       $total_apr_off += 1;
      } else {
       $total_apr += 1;
      }
      break;
     case "05":
      if ($tgl_selesai > $time_target) {
       $total_mei_off += 1;
      } else {
       $total_mei += 1;
      }
      break;
     case "06":
      if ($tgl_selesai > $time_target) {
       $total_jun_off += 1;
      } else {
       $total_jun += 1;
      }
      break;
     case "07":
      if ($tgl_selesai > $time_target) {
       $total_jul_off += 1;
      } else {
       $total_jul += 1;
      }
      break;
     case "08":
      if ($tgl_selesai > $time_target) {
       $total_ags_off += 1;
      } else {
       $total_ags += 1;
      }
      break;
     case "09":
      if ($tgl_selesai > $time_target) {
       $total_sep_off += 1;
      } else {
       $total_sep += 1;
      }
      break;
     case "10":
      if ($tgl_selesai > $time_target) {
       $total_okt_off += 1;
      } else {
       $total_okt += 1;
      }
      break;
     case "11":
      if ($tgl_selesai > $time_target) {
       $total_nov_off += 1;
      } else {
       $total_nov += 1;
      }
      break;
     case "12":
      if ($tgl_selesai > $time_target) {
       $total_des_off += 1;
      } else {
       $total_des += 1;
      }
      break;
     default:
      break;
    }
   }
  }
  $result['ontime'] = array($total_januari, $total_feb, $total_mar, $total_apr,
      $total_mei, $total_jun, $total_jul, $total_ags, $total_sep, $total_okt,
      $total_nov, $total_des);
  $result['offtime'] = array($total_januari_off, $total_feb_off, $total_mar_off,
      $total_apr_off, $total_mei_off, $total_jun_off, $total_jul_off, $total_ags_off,
      $total_sep_off, $total_okt_off, $total_nov_off, $total_des_off);
  echo json_encode($result);
 }

}
