<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Grafik' ?>
     </div>
    </div>
    <div class="tile">
     <h3 class="tile-title">Grafik Dailytask Pegawai</h3>
     <br/>
     <div class="text-center">
      <button class="btn btn-blue">Ontime</button>
      &nbsp;
      <button class="btn btn-orange">Offtime</button>
     </div>
     <div class="tile-body">

      <div class="row">
       <div class="col-md-12">
        <div class="embed-responsive embed-responsive-16by9">
         <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
        </div>
       </div>
      </div>
     </div>

     <!--     <div class="tile-footer text-right">
           <i class="fa fa-plus-circle fa-2x hover"></i>
          </div>-->
    </div> 
   </div>
  </div>
 </div>
</div>