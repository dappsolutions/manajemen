<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/main.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css"> 
  <!-- Font-icon css-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/fontawesome/css/font-awesome.min.css">
  <title>Login - Manajemen</title>
 </head>
 <body>
  <div class="loader">

  </div>
  <section class="material-half-bg">
   <div class="cover"></div>
  </section>
  <section class="login-content">
   <div class="logo">
    <h1>Aplikasi Manajemen</h1>
   </div>
   <div class="login-box">
    <form class="login-form" action="">
     <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
     <div class="form-group">
      <label class="control-label">USERNAME</label>
      <input class="form-control required" error="Username" type="text" placeholder="Email" autofocus id="username">
     </div>
     <div class="form-group">
      <label class="control-label">PASSWORD</label>
      <input class="form-control required" error="Password" type="password" placeholder="Password" id="password">
     </div>
     <div class="form-group btn-container">
      <button class="btn btn-warning btn-block" onclick="Login.sign_in(this, event)"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
     </div>
    </form>
   </div>
  </section>
  <!-- Essential javascripts for application to work-->
  <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/main.js"></script>
  <!-- The javascript plugin to display page loading on top-->
  <script src="js/plugins/pace.min.js"></script>

  <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/url.js"></script>
  <script src="<?php echo base_url() ?>assets/js/message.js"></script>
  <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
  <script src="<?php echo base_url() ?>assets/js/controllers/login.js"></script>
  <script type="text/javascript">
   // Login Page Flipbox control
   $('.login-content [data-toggle="flip"]').click(function () {
    $('.login-box').toggleClass('flipped');
    return false;
   });
  </script>
 </body>
</html>