<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <!--   <div class="col-md-3">
       <div class="tile">
        <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;<?php echo $title_content ?></h5>
       </div>
      </div>-->
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <div class="row">
       <div class="col-md-3">
        <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($module) ?>
       </div>
<!--       <div class="col-md-9 text-right">
        <i class="fa fa-pencil-square-o hover-content" onclick=""></i> 
       </div>-->
      </div>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <br/>      
      <?php if (isset($nip)) { ?>
       <div class="row">
        <div class="col-md-3 ">
         <b>NIP</b>
        </div> 
        <div class="col-sm-5 text-success">
         <?php echo $nip ?>
        </div>        
       </div>
       <br/>
      <?php } ?>      
      <?php if (isset($nama)) { ?>
       <div class="row">
        <div class="col-md-3 ">
         <b>Nama Pegawai</b>
        </div> 
        <div class="col-sm-5 text-success">
         <?php echo $nama ?>
        </div>        
       </div>
       <br/>
      <?php } ?>
      <?php if (isset($email)) { ?>
       <div class="row">
        <div class="col-md-3 ">
         <b>Email</b>
        </div> 
        <div class="col-sm-5 text-success">
         <?php echo $email ?>
        </div>        
       </div>
       <br/>
      <?php } ?>
      <?php if (isset($nama_ultg)) { ?>
       <div class="row">
        <div class="col-md-3 ">
         <b>ULTG</b>
        </div> 
        <div class="col-sm-5 text-success">
         <?php echo $nama_ultg ?>
        </div>        
       </div>
       <br/>
      <?php } ?>
      <?php if (isset($jabatan)) { ?>
       <div class="row">
        <div class="col-md-3 ">
         <b>Jabatan</b>
        </div> 
        <div class="col-sm-5 text-success">
         <?php echo $jabatan ?>
        </div>        
       </div>
       <br/>
      <?php } ?>
      <div class="row">
       <div class="col-md-3 ">
        <b>Username</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $username ?>
       </div>        
      </div>
      <br/>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Profile.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
