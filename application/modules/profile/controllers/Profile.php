<?php

class Profile extends MX_Controller {

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/profile.js"></script>'
  );

  return $data;
 }

 public function getModuleName() {
  return 'profile';
 }

 public function getFormChangePassword() {
  $data['username'] = $this->session->userdata('username');
  $data['title_content'] = 'Form Ganti Password';
  echo $this->load->view('form_change_password', $data, true);
 }

 public function simpanPassword() {

  $username = $this->input->post('username');
  $password_lama = $this->input->post('password_lama');
  $password_baru = $this->input->post('password_baru');

  $user_id = $this->session->userdata('user_id');


  $is_valid = false;
  $message = "";
  $valid_user = $this->getDataUserIsValid($username, $password_lama);
  if ($valid_user) {
   //update password
   if ($password_baru == $password_lama) {
    $is_valid = false;
    $message = "Password Tidak Boleh Sama";
   } else {
    $password_baru = $password_baru;
    Modules::run('database/_update', 'user', array('password' => $password_baru), array('id' => $user_id));
    $is_valid = true;
   }
  } else {
   $message = "Password Tidak Valid";
  }

  echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
 }

 public function getDataUserIsValid($username, $password) {
  $data = Modules::run('database/get', array(
              'table' => 'user',
              'where' => array('username' => $username, 'password' => $password)
  ));

  $is_valid = false;
  if (!empty($data)) {
   $is_valid = true;
  }

  return $is_valid;
 }

 public function getDetailDataPegawai($id) {
  $hak_akses = $this->session->userdata('hak_akses');
  if ($hak_akses != 'superadmin') {
   $data = Modules::run('database/get', array(
               'table' => 'pegawai p',
               'field' => array('p.*', 'u.nama as nama_ultg', 'j.jabatan',
                   'j.id as jabatan_id', 'us.username'),
               'join' => array(
                   array('ultg u', 'u.id = p.ultg'),
                   array('jabatan j', 'j.id = p.jabatan'),
                   array('user us', 'us.pegawai = p.id'),
               ),
               'where' => "us.id = '" . $id . "'"
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'user',
               'where' => "id = '" . $id . "'"
   ));
  }

  $data = $data->row_array();
  return $data;
 }

 public function profile_detail() {
  $id = $this->session->userdata('user_id');

  $data = $this->getDetailDataPegawai($id);
//  echo '<pre>';
//  print_r($data);
//  die;
  $data['view_file'] = 'profile';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Profile";
  $data['title_content'] = "Detail Profil";
  echo Modules::run('template', $data);
 }

}
