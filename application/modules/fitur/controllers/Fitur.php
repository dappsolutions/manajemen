<?php

class Fitur extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $user_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->user_id = $this->session->userdata('user_id');
 }

 public function getModuleName() {
  return 'fitur';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/fitur.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'feature';
 }

 public function getRootModule() {
  return "master";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Fitur";
  $data['title_content'] = 'Fitur';
  $content = $this->getDataFitur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getTotalDataFitur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('f.name', $keyword),
       array('pg.nama', $keyword),
   );
  }

  $where = "f.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "f.deleted = 0";
  } else {
   $where = "fs.user = '" . $this->user_id . "'";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' f',
                'field' => array('distinct(fs.user) user', 'pg.nama as nama_pegawai', 'j.jabatan'),
                'join' => array(
                    array('feature_set fs', 'fs.feature = f.id'),
                    array('user u', 'fs.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                    array('jabatan j', 'pg.jabatan = j.id'),
                    array('ultg ul', 'pg.ultg = ul.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' f',
                'field' => array('distinct(fs.user) user', 'pg.nama as nama_pegawai', 'j.jabatan'),
                'join' => array(
                    array('feature_set fs', 'fs.feature = f.id'),
                    array('user u', 'fs.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                    array('jabatan j', 'pg.jabatan = j.id'),
                    array('ultg ul', 'pg.ultg = ul.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  return $total;
 }

 public function getDataFitur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('f.name', $keyword),
       array('pg.nama', $keyword),
   );
  }

  $where = "f.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "f.deleted = 0";
  } else {
   $where = "fs.user = '" . $this->user_id . "'";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' f',
                'field' => array('distinct(fs.user) user',
                    'pg.nama as nama_pegawai',
                    'j.jabatan', 'ul.nama as unit'),
                'join' => array(
                    array('feature_set fs', 'fs.feature = f.id'),
                    array('user u', 'fs.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                    array('jabatan j', 'pg.jabatan = j.id'),
                    array('ultg ul', 'pg.ultg = ul.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' f',
                'field' => array('distinct(fs.user) user', 'pg.nama as nama_pegawai',
                    'j.jabatan', 'ul.nama as unit'),
                'join' => array(
                    array('feature_set fs', 'fs.feature = f.id'),
                    array('user u', 'fs.user = u.id'),
                    array('pegawai pg', 'u.pegawai = pg.id'),
                    array('jabatan j', 'pg.jabatan = j.id'),
                    array('ultg ul', 'pg.ultg = ul.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataFitur($keyword)
  );
 }

 public function getDetailDataFitur($id) {
  $data = Modules::run('database/get', array(
              'table' => 'feature_set fs',
              'field' => array('fs.*'),
              'where' => "fs.user = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getDataUser($add = "") {
  $where = 'p.deleted = 0';
  if ($add != '') {
   $where = "p.deleted = 0 and fs.user is null";
  }
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('u.*', 'fs.user', 'p.nip'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai'),
                  array('(select max(id) as id, user from feature_set group by user) fs', 'u.id = fs.user', 'left'),
              ),
              'where' => $where,
	      'limit' => 2000
  ));
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['term_pegawai'] = $value['username'] . ' - ' . $value['nip'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getHasFeatureUser($id, $feature) {
  $data = Modules::run('database/get', array(
              'table' => 'feature_set ',
              'where' => "user = '" . $id . "' and feature = '" . $feature . "'"
  ));

  $is_has = 0;
  if (!empty($data)) {
   $is_has = 1;
  }
  return $is_has;
 }

 public function getListMasterMenu($id = "") {
  $data = Modules::run('database/get', array(
              'table' => 'feature f',
              'where' => "f.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['has'] = 0;
    if ($id != '') {
     $value['has'] = $this->getHasFeatureUser($id, $value['id']);
    }
    $value['user_input'] = $id;
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getHasFeatureDashboardUser($user, $room_header) {
  $data = Modules::run('database/get', array(
              'table' => 'room_header_fitur rhf',
              'where' => "rhf.deleted = 0 and rhf.user = '" . $user . "' "
              . "and rhf.room_header = '" . $room_header . "'"
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }

  return $is_exist;
 }

 public function getListMenuDasboard($user = '') {
  $where = "rh.deleted = 0";
  if ($user != '') {
   $where = "rh.deleted = 0";
  }
  $data = Modules::run('database/get', array(
              'table' => 'room_header rh',
              'field' => array('rh.*'),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['has'] = 0;
    if ($user != '') {
     $value['has'] = $this->getHasFeatureDashboardUser($user, $value['id']);
    }
    $value['user_input'] = $user;
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Fitur";
  $data['title_content'] = 'Tambah Fitur';
  $data['list_pegawai'] = $this->getDataUser("add");
  $data['list_master'] = $this->getListMasterMenu();
  $data['list_dashboard'] = $this->getListMenuDasboard();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataFitur($id);
//  echo '<pre>';
//  print_r($data);
//  die;  
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Fitur";
  $data['title_content'] = 'Ubah Fitur';
  $data['list_pegawai'] = $this->getDataUser();
  $data['list_master'] = $this->getListMasterMenu($id);
  $data['list_dashboard'] = $this->getListMenuDasboard($id);
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataFitur($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Fitur";
  $data['title_content'] = "Detail Fitur";
  $data['user_id'] = $id;
  $data['list_dashboard'] = $this->getListMenuDasboard($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataFitur($value, $user) {
  $data['user'] = $user;
  $data['feature'] = $value->menu;
  return $data;
 }

 public function getPostDataDashboard($value, $user) {
  $data['user'] = $user;
  $data['room_header'] = $value->menu;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

//  echo '<pre>';
//  print_r($data);
//  die;
  $this->db->trans_begin();
  $user = $data->fitur->user;
  try {
   if ($id == '') {
    if (!empty($data->fitur->menu)) {
     foreach ($data->fitur->menu as $value) {
      $post = $this->getPostDataFitur($value, $data->fitur->user);
      Modules::run('database/_insert', 'feature_set', $post);
     }
    }
    if (!empty($data->fitur->dashboard)) {
     foreach ($data->fitur->dashboard as $value) {
      $post = $this->getPostDataDashboard($value, $data->fitur->user);
      Modules::run('database/_insert', 'room_header_fitur', $post);
     }
    }
   } else {
    //update
    if (!empty($data->fitur->menu)) {
     foreach ($data->fitur->menu as $value) {
      $post = $this->getPostDataFitur($value, $data->fitur->user);
      if ($value->is_deleted == '1') {
       Modules::run('database/_delete', 'feature_set', array('user' => $user, 'feature' => $value->menu));
      }
      if ($value->is_update == '1') {
       Modules::run('database/_delete', 'feature_set', array('user' => $value->user_input, 'feature' => $value->menu));
       Modules::run('database/_insert', 'feature_set', $post);
      }

      if ($value->is_update == '0') {
       Modules::run('database/_insert', 'feature_set', $post);
      }
     }
    }

    if (!empty($data->fitur->dashboard)) {
     foreach ($data->fitur->dashboard as $value) {
      $post = $this->getPostDataDashboard($value, $data->fitur->user);
      if ($value->is_deleted == '1') {
       Modules::run('database/_delete', 'room_header_fitur', array('user' => $user, 'room_header' => $value->menu));
      }
      if ($value->is_update == '1') {
       Modules::run('database/_delete', 'room_header_fitur', array('user' => $value->user_input, 'room_header' => $value->menu));
       Modules::run('database/_insert', 'room_header_fitur', $post);
      }

      if ($value->is_update == '0') {
       Modules::run('database/_insert', 'room_header_fitur', $post);
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $user));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Fitur";
  $data['title_content'] = 'Data Fitur';
  $content = $this->getDataFitur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function detailFitur() {
  $user = $this->input->post('user');
  $content['user_id'] = $user;
  echo $this->load->view('detail_fitur', $content, true);
 }

}
