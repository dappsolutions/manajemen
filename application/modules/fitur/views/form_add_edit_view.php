<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i onclick="Fitur.main()" class="hover fa fa-file-text-o"></i>&nbsp;&nbsp;<?php echo ucfirst('Fitur') ?>
     <hr/>
    </div>
   </div>
   <div class="col-md-9">
    <div class="tile">     
     <div class="tile-body">
      <h4>Form</h4>
      <br/>
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-1">User</label>
        <div class="col-sm-6">
         <select id="user" class="form-control required" error="User">
          <option value="">Pilih User</option>
          <?php if (!empty($list_pegawai)) { ?>
           <?php foreach ($list_pegawai as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($user)) { ?>
             <?php $selected = $user == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['term_pegawai'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>
      </form>
      <br/>

      <div class="content_menu">
       <h5>Daftar Menu</h5>
       <hr/>
       <div class="table-responsive">
        <table class="table table-bordered" id="tb_menu">
         <thead>
          <tr class="table-warning">
           <th class="text-center">No</th>
           <th>Menu</th>
           <th class="text-center">
            Action
            <input type="checkbox" value="" id="check_all" class="form-control" onchange="Fitur.checkAll(this)" />
           </th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($list_master)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($list_master as $value) { ?>
            <tr user_input="<?php echo $value['user_input'] ?>" has="<?php echo $value['has'] ?>" data_id="<?php echo $value['id'] ?>">
             <td class="text-center"><?php echo $no++ ?></td>
             <td><?php echo $value['name'] ?></td>
             <td>
              <input type="checkbox" value="" id="check" class="form-control check_child" onchange="Fitur.check(this)" <?php echo $value['has'] == '1' ? 'checked' : '' ?>/>
             </td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>

       </div>
      </div>

      <div class="content_menu">
       <h5>Daftar Menu Dashboard</h5>
       <hr/>
       <div class="table-responsive">
        <table class="table table-bordered" id="tb_menu_dashboard">
         <thead>
          <tr class="table-warning">
           <th class="text-center">No</th>
           <th>Menu Dashboard</th>
           <th class="text-center">
            Action
            <input type="checkbox" value="" id="check_all_menu" class="form-control" onchange="Fitur.checkAllMenu(this)" />
           </th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($list_dashboard)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($list_dashboard as $value) { ?>
            <tr user_input="<?php echo $value['user_input'] ?>" has="<?php echo $value['has'] ?>" data_id="<?php echo $value['id'] ?>">
             <td class="text-center"><?php echo $no++ ?></td>
             <td><?php echo $value['title'] ?></td>
             <td>
              <input type="checkbox" value="" id="check_menu" class="form-control check_child_menu" onchange="Fitur.checkMenu(this)" <?php echo $value['has'] == '1' ? 'checked' : '' ?>/>
             </td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>

       </div>
      </div>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="Fitur.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Fitur.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
