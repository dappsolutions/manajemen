<div class="row">
 <div class="col-md-12">
  <div class="box box-solid box-primary">
   <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
   </div>
  </div>
  <div class="tile">
   <div class="table-responsive">
    <table class="table table-bordered">
     <tbody>
      <?php echo Modules::run('menu/detailMenu', $user_id) ?>
     </tbody>
    </table>   
   </div>
   <h5>Daftar Menu Dashboard</h5>
   <hr/>
   <div class="table-responsive">
    <table class="table table-bordered">
     <tbody>
      <?php if (!empty($list_dashboard)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($list_dashboard as $value) { ?>
        <?php if ($value['has']) { ?>
         <tr user_input="<?php echo $value['user_input'] ?>" has="<?php echo $value['has'] ?>" data_id="<?php echo $value['id'] ?>">
          <td class="text-center"><?php echo $no++ ?></td>
          <td><?php echo $value['title'] ?></td>
         </tr>
        <?php } ?>
       <?php } ?>
      <?php } ?>
     </tbody>
    </table>   
   </div>
   <div class="tile-footer text-right">
    <!--<div class="col-sm-6">-->
    &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Fitur.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
    <!--</div>-->      
   </div>
  </div>  
 </div>
</div>