<?php

class Template extends MX_Controller {

 public function index($data) {
//  echo '<pre>';
//  print_r($_SESSION);die;
  $data['notif_total'] = $this->getNotification();
  $data['total_user'] = Modules::run('dashboard/getTotalUser');
  $data['total_like'] = Modules::run('dashboard/getTotalLike');
  $data['total_pegawai'] = Modules::run('dashboard/getTotalPegawai');
  $data['total_upload'] = Modules::run('dashboard/getTotalUploadedFile');
  echo $this->load->view('template_view', $data, true);
 }

 public function getNotification() {
  $user_id = $this->session->userdata('user_id');
  $total = Modules::run('database/count_all', array(
              'table' => 'pegawai_job_desk pjd',
              'field' => array('pjd.*'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user u', 'p.id = u.pegawai'),
                  array('user ug', 'pjd.giver = ug.id', 'left'),
                  array('pegawai pgg', 'ug.pegawai = pgg.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
              ),
              'where' => "pjd.deleted = 0 and ((u.id = '" . $user_id . "' and jds.status = 'DRAFT') or (ug.id = '" . $user_id . "' and jds.status = 'CLOSED'))"
  ));
  return $total;
 }

}
