<aside class="app-sidebar">
 <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo base_url() ?>assets/images/users/user.png" alt="User Image" width="50" height="50">
  <div>
   <?php if ($this->session->userdata('nip') == '') { ?>
    <p class="app-sidebar__user-name"><?php echo strtoupper($this->session->userdata('username')) ?></p>
    <p class="app-sidebar__user-designation"><?php echo strtoupper($this->session->userdata('hak_akses')) ?></p>
   <?php } else { ?>
    <p class="app-sidebar__user-name"><?php echo strtoupper($this->session->userdata('nip')) ?></p>
    <p class="app-sidebar__user-designation"><?php echo strtoupper($this->session->userdata('nama_pegawai')) ?></p>
   <?php } ?>
  </div>
 </div>
 <ul class="app-menu">
  <?php echo Modules::run('menu/getAksesMenu', $this->session->userdata('user_id')) ?>
  <li>
   <a class="app-menu__item" href="<?php echo base_url() . 'login/sign_out' ?>">
    <i class="app-menu__icon fa fa-sign-out "></i> <span class="app-menu__label">Sign Out</span>
   </a>
  </li>
 </ul>
</aside>