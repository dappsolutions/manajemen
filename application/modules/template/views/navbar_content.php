<header class="app-header"><a class="app-header__logo" href="<?php echo base_url() . 'dashboard' ?>">Manajemen</a>
 <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
 <!-- Navbar Right Menu-->
 <ul class="app-nav">
  <li class="app-nav__item">
   <?php echo strtoupper($title_content) ?>
  </li>
  <li class="app-nav__item">
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="app-nav__item">
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="app-nav__item">
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="app-nav__item">
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="app-nav__item">
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <li class="app-nav__item">
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </li>
  <!--  <li class="app-search">
     <input class="app-search__input" type="search" placeholder="Search">
     <button class="app-search__button"><i class="fa fa-search"></i></button>
    </li>-->
  <!--Notification Menu-->
  <li class="dropdown">
   <a class="app-nav__item" href="#" data-toggle="dropdown" 
      aria-label="Show notifications">
    <i class="fa fa-users fa-lg"></i> &nbsp;<?php echo $total_user ?>
   </a>
  </li>
  <li class="dropdown">
   <a class="app-nav__item" href="#" data-toggle="dropdown" 
      aria-label="Show notifications">
    <i class="fa fa-thumbs-up fa-lg"></i> &nbsp;<?php echo $total_like ?>
   </a>
  </li>
  <li class="dropdown">
   <a class="app-nav__item" href="#" data-toggle="dropdown" 
      aria-label="Show notifications">
    <i class="fa fa-file fa-lg"></i> &nbsp;<?php echo $total_upload ?>
   </a>
  </li>
  <li class="dropdown">
   <a class="app-nav__item" href="#" data-toggle="dropdown" 
      aria-label="Show notifications">
    <i class="fa fa-user-o fa-lg"></i> &nbsp;<?php echo $total_pegawai ?>
   </a>
  </li>
  <li class="dropdown">
   <a class="app-nav__item" href="#" data-toggle="dropdown" 
      aria-label="Show notifications">
    <i class="fa fa-bell-o fa-lg"></i>
   </a>
   <ul class="app-notification dropdown-menu dropdown-menu-right">    
    <li class="app-notification__title">
     <?php if (isset($notif_total)) { ?>
      <?php if ($notif_total != '' && $notif_total != 0) { ?>
       <?php echo 'Ada ' . $notif_total . ' pemberitahuan baru.' ?>
      <?php } else { ?>
       <?php echo 'Belum ada pemberitahuan.' ?>
      <?php } ?>
     <?php } ?>      
    </li>
    <div class="app-notification__content">
     <?php if (isset($notif_total)) { ?>
      <?php if ($notif_total != '' && $notif_total != 0) { ?>
       <li>
        <a class="app-notification__item" href="<?php echo base_url() . 'dailytask' ?>">
         <span class="app-notification__icon">
          <span class="fa-stack fa-lg">
           <i class="fa fa-circle fa-stack-2x text-primary"></i>
           <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
          </span>
         </span>
         <div>
          <p class="app-notification__message">Daily Task</p>
          <p class="app-notification__meta">Baru</p>
         </div>
        </a>
       </li>
      <?php } ?>
     <?php } ?>
    </div>
    <!--    <li class="app-notification__footer"><a href="#">See all notifications.</a></li>-->
   </ul>
  </li>
  <!-- User Menu-->
  <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
   <ul class="dropdown-menu settings-menu dropdown-menu-right">
    <li><a class="dropdown-item" onclick="Template.changePassword(this, event)"><i class="fa fa-cog fa-lg"></i> Ganti Password</a></li>
    <li><a class="dropdown-item" href="<?php echo base_url() . 'profile/profile_detail' ?>"><i class="fa fa-user fa-lg"></i> User Profile</a></li>
    <li><a class="dropdown-item" href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
   </ul>
  </li>
 </ul>
</header>