<!DOCTYPE html>
<html lang="en">
 <?php echo $this->load->view('head_content'); ?>
 <body class="app sidebar-mini rtl">

  <?php echo $this->load->view('navbar_content'); ?>
  <!-- Sidebar menu-->

  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <?php echo $this->load->view('sidebar_content'); ?>    
  <div class="loader">

  </div>
  <main class="app-content">
   <?php echo $this->load->view('title_content'); ?>
   <?php echo $this->load->view($module . '/' . $view_file); ?>
  </main>
  <?php echo $this->load->view('script_content'); ?>  
  <?php
  if (isset($header_data)) {
   foreach ($header_data as $v_head) {
    echo $v_head;
   }
  }
  ?>
 </body>
</html>