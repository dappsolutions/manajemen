<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i onclick="Inbox.main()" class="fa fa-file-text-o hover"></i>&nbsp;&nbsp;<?php echo ucfirst($title_content) ?>
     <hr/>
    </div>
   </div>
   <div class="col-md-9">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Daftar' ?>
     </div>
    </div>
    <div class="tile">
     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" type="search" onkeyup="Inbox.search(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>    

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="table-warning">
            <th>#</th>
            <th>Username</th>
            <th>Judul</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['username'] ?></td>
              <td><?php echo $value['title'] ?></td>
              <?php $class_status = $value['status'] == 'CLOSED' ? 'bg-success text-white' : '' ?>
              <?php $class_status = $value['status'] == 'OPEN' ? 'bg-warning' : $value['status'] == 'REPLIED' ? 'bg-primary text-white' : $value['status'] == 'CLOSED' ? 'bg-danger text-white' : 'bg-info text-white' ?>
              <td class="text-center <?php echo $class_status ?>">
               <?php echo $value['status'] ?>
              </td>
              <td class="text-center">
               <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
                <i class="fa fa-trash grey-text hover" onclick="Inbox.delete('<?php echo $value['id'] ?>')"></i>
                &nbsp;
               <?php } ?>
               <?php if ($value['status'] != 'CLOSED') { ?>
                <i data-toggle="tooltip" title="Balas Komentar" class="fa fa-reply grey-text  hover" onclick="Inbox.reply('<?php echo $value['id'] ?>')"></i>                
                &nbsp;
                <i data-toggle="tooltip" title="Tutup Komentar" class="fa fa-close grey-text  hover" onclick="Inbox.closeCom('<?php echo $value['id'] ?>')"></i>
               <?php } else { ?>               
                <i data-toggle="tooltip" title="Detail Komentar" class="fa fa-file-text grey-text  hover" onclick="Inbox.reply('<?php echo $value['id'] ?>')"></i>
                &nbsp;
               <?php } ?>               
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr class="text-center">
             <td colspan="6">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div> 
   </div>
  </div>
 </div>
</div>