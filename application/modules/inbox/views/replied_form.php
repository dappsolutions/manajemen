<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-4">
  <div class="box box-solid box-primary">
   <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-bars"></i> <?php echo ucwords($title_content) ?>
   </div>
  </div>
  <div class="tile">
   <i onclick="Inbox.main()" class="fa fa-file-text-o hover"></i>&nbsp;&nbsp;<?php echo ucfirst($title_content) ?>
   <hr/>
  </div>
 </div>
 <div class="col-md-8">
  <div class="tile">
   <h3 class="tile-title">Percakapan</h3>
   <hr/>
   <div class="messanger">
    <div class="messages">
     <?php if (!empty($list_inbox)) { ?>
      <?php foreach ($list_inbox as $key => $value) { ?>     
       <?php $user = $value['user'] ?>
       <?php if ($user_id == $user) { ?>
        <div class="message me" refer_id="">
         <p class="info">
          <?php echo $value['isi'] ?>
         </p>
        </div>
        <div class="username text-right" style="margin-top: -12px;">
         <?php echo $value['username'] ?>
        </div>
       <?php } else { ?>
        <div class="message" refer_id="<?php echo $value['id'] ?>">
         <p class="info">
          <?php echo $value['isi'] ?>
         </p>
        </div>
        <div class="username" style="margin-top: -12px;">
         <?php echo $value['username'] ?>
        </div>
       <?php } ?>       
      <?php } ?>
     <?php } ?>     
     <!--     <div class="message me">
           <p class="info">Hi<br>Good Morning</p>
          </div>
          <div class="username text-right" style="margin-top: -12px;">
           User
          </div>-->
    </div>
    <?php if ($status != 'CLOSED') { ?>
     <div class="sender">
      <input type="text" id="komentar" placeholder="Send Message">
      <button class="btn btn-primary" type="button" onclick="Inbox.sendKomentar(this)">
       <i class="fa fa-lg fa-fw fa-paper-plane"></i>
      </button>
     </div>
    <?php } ?>
   </div>
  </div>
 </div>
</div>
