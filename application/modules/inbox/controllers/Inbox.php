<?php

class Inbox extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'inbox';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/inbox.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'room_body_sub_comment';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Inbox";
  $data['title_content'] = 'Inbox';
  $content = $this->getDataInbox();
//  echo '<pre>';
//  print_r($content);die;
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataInbox($keyword = '') {
  $user = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.title', $keyword),
       array('u.username', $keyword),
   );
  }

  $where = "t.deleted = 0 and t.user = '" . $user . "'";
  if ($this->akses == 'Superadmin') {
   $where = "t.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'u.username', 'sc.status'),
                'join' => array(
                    array('user u', 't.user = u.id'),
                    array('(select max(id) id, room_body_sub_comment from status_sub_comment group by room_body_sub_comment) st',
                        'st.room_body_sub_comment = t.id'),
                    array('status_sub_comment sc', 'sc.id = st.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'u.username', 'sc.status'),
                'join' => array(
                    array('user u', 't.user = u.id'),
                    array('(select max(id) id, room_body_sub_comment from status_sub_comment group by room_body_sub_comment) st',
                        'st.room_body_sub_comment = t.id'),
                    array('status_sub_comment sc', 'sc.id = st.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataInbox($keyword = '') {
  $user = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.title', $keyword),
       array('u.username', $keyword),
   );
  }

  $where = "t.deleted = 0 and t.user = '" . $user . "'";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'u.username', 'sc.status'),
                'join' => array(
                    array('user u', 't.user = u.id'),
                    array('(select max(id) id, room_body_sub_comment from status_sub_comment group by room_body_sub_comment) st',
                        'st.room_body_sub_comment = t.id'),
                    array('status_sub_comment sc', 'sc.id = st.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*', 'u.username', 'sc.status'),
                'join' => array(
                    array('user u', 't.user = u.id'),
                    array('(select max(id) id, room_body_sub_comment from status_sub_comment group by room_body_sub_comment) st',
                        'st.room_body_sub_comment = t.id'),
                    array('status_sub_comment sc', 'sc.id = st.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataInbox($keyword)
  );
 }

 public function getDetailDataInbox($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'sc.status'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('(select max(id) id, room_body_sub_comment from status_sub_comment group by room_body_sub_comment) st',
                      'st.room_body_sub_comment = t.id'),
                  array('status_sub_comment sc', 'sc.id = st.id')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Inbox";
  $data['title_content'] = 'Tambah Inbox';
  echo Modules::run('template', $data);
 }

 public function getListIsiInbox($id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_comment rbc',
              'field' => array('rbc.*', 'u.username'),
              'join' => array(
                  array('user u', 'rbc.user = u.id')
              ),
              'where' => "rbc.room_body_sub_comment = '" . $id . "' and rbc.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function reply($id) {
  $data = $this->getDetailDataInbox($id);
//  echo '<pre>';
//  print_r($data);
//  die;
  $data['view_file'] = 'replied_form';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Balas Inbox";
  $data['title_content'] = 'Balas Inbox';
  $data['list_inbox'] = $this->getListIsiInbox($id);
  $data['user_id'] = $this->session->userdata('user_id');

  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataInbox($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Inbox";
  $data['title_content'] = "Detail Inbox";
  echo Modules::run('template', $data);
 }

 public function getPostDataInbox($value) {
  $data['jabatan'] = $value->jabatan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $post = $this->getPostDataInbox($data->jabatan);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Inbox";
  $data['title_content'] = 'Data Inbox';
  $content = $this->getDataInbox($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function sendKomentar() {
  $data = $_POST;
  $user = $this->session->userdata('user_id');
  $hak_akses = $this->session->userdata('hak_akses');

  $is_valid = false;
  $this->db->trans_begin();
  try {

   $post_com['isi'] = $data['komentar'];
   $post_com['user'] = $user;
   $post_com['room_body_sub_comment'] = $data['room_sub'];
   $post_com['refer'] = $data['refer_id'];
   $comment = Modules::run('database/_insert', 'room_body_comment', $post_com);

   $status_com['room_body_comment'] = $comment;
   $status_com['status'] = 'SEND';
   Modules::run('database/_insert', 'status_comment', $status_com);

   $status_sub['room_body_sub_comment'] = $data['room_sub'];
   $status_sub['status'] = $hak_akses == 'superadmin' ? 'REPLIED' : 'FEEDBACK';
   Modules::run('database/_insert', 'status_sub_comment', $status_sub);

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function closeCom($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {

   $status_sub['room_body_sub_comment'] = $id;
   $status_sub['status'] = 'CLOSED';
   Modules::run('database/_insert', 'status_sub_comment', $status_sub);

   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
