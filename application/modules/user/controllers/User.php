<?php

class User extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'user';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/user.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'user';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - User";
  $data['title_content'] = 'User';
  $content = $this->getDataUser();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataUser($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('p.nama', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*',
                    'p.nama as nama_pegawai', 'p.nip', 'j.jabatan'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id', 'left'),
                    array('jabatan j', 'p.jabatan = j.id', 'left'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*',
                    'p.nama as nama_pegawai', 'p.nip', 'j.jabatan'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id', 'left'),
                    array('jabatan j', 'p.jabatan = j.id', 'left'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataUser($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('p.nama', $keyword),
   );
  }

  $where = "t.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "t.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*',
                    'p.nama as nama_pegawai', 'p.nip', 'j.jabatan'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id', 'left'),
                    array('jabatan j', 'p.jabatan = j.id', 'left'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' t',
                'field' => array('t.*',
                    'p.nama as nama_pegawai', 'p.nip', 'j.jabatan'),
                'join' => array(
                    array('pegawai p', 't.pegawai = p.id', 'left'),
                    array('jabatan j', 'p.jabatan = j.id', 'left'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataUser($keyword)
  );
 }

 public function getDetailDataUser($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*',
                  'p.nama as nama_pegawai', 'p.nip', 'j.jabatan'),
              'join' => array(
                  array('pegawai p', 't.pegawai = p.id', 'left'),
                  array('jabatan j', 'p.jabatan = j.id', 'left'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getDataPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai',
              'where' => 'deleted = 0',
              'limit' => 2000
  ));
//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['term_pegawai'] = $value['nip'] . ' - ' . $value['nama'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah User";
  $data['title_content'] = 'Tambah User';
  $data['list_pegawai'] = $this->getDataPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataUser($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah User";
  $data['title_content'] = 'Ubah User';
  $data['list_pegawai'] = $this->getDataPegawai();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataUser($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail User";
  $data['title_content'] = "Detail User";
  echo Modules::run('template', $data);
 }

 public function getPostDataUser($value) {
  $data['username'] = $value->username;
  $data['password'] = $value->password;
  $data['pegawai'] = $value->pegawai;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $post = $this->getPostDataUser($data->user);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data User";
  $data['title_content'] = 'Data User';
  $content = $this->getDataUser($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
