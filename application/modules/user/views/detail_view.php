<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <!--   <div class="col-md-3">
       <div class="tile">
        <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;<?php echo $title_content ?></h5>
       </div>
      </div>-->
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <div class="row">
       <div class="col-md-3 ">
        <b>Username</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $username ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Password</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $password ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Pegawai</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $nip . ' - ' . $nama_pegawai ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Jabatan</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $jabatan == '' ? 'Superadmin' : $jabatan ?>
       </div>        
      </div>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="User.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
