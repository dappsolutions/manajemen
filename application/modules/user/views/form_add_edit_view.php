<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i onclick="User.main()" class="hover fa fa-file-text-o"></i>&nbsp;&nbsp;<?php echo $module ?>
     <hr/>
    </div>>
   </div>
   <div class="col-md-9">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Form' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-1">Username</label>
        <div class="col-sm-6">
         <input class="form-control required" error="Username" id="username" type="text" placeholder="Username" value="<?php echo isset($username) ? $username : '' ?>">
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-3">Password</label>
        <div class="col-sm-6">
         <input class="form-control required" error="Password" id="password" type="text" placeholder="Password" value="<?php echo isset($password) ? $password : '' ?>">
        </div>        
       </div>
       <div class="form-group">
        <label class="control-label col-sm-1">Pegawai</label>
        <div class="col-sm-6">
         <select id="pegawai" class="form-control required" error="Pegawai">
          <option value="">Pilih Pegawai</option>
          <?php if (!empty($list_pegawai)) { ?>
           <?php foreach ($list_pegawai as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($pegawai)) { ?>
             <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['term_pegawai'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="User.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="User.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
