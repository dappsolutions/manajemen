<?php

class Menu extends MX_Controller {

 public $hak_akses;

 public function __construct() {
  parent::__construct();
  $this->hak_akses = $this->session->userdata('hak_akses');
 }

 public function getAksesMenu($user_id) {
  $sql = "select fs.user, 
				fs.feature as id,f.name, f.url
				, f.parent
    , fp.id as parent_feature
    , f.icon
				from feature_set fs
				join feature f
					on fs.feature = f.id
				join `user` u
					on u.id = fs.`user`
    left join feature fp
     on fp.id = f.parent 
     where u.id = '" . $user_id . "'
					order by f.row, f.parent";

  $data = $this->db->query($sql);
//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  $tree = $this->buildTree($result);
  $menu = $this->buildMenu($tree);
  echo $menu;
 }

 function buildMenuRoomHeader($arr, $is_parent = 0, $room, $head = "") {
  if (!empty($arr)) {
   foreach ($arr as $value) {

    if (!empty($value['children'])) {
     $style_margin = "";
     if ($value['parent'] != '') {
      $point = 16 * $value['id'] / 10;
      $style_margin = "margin-left:" . $point . "px;";
     }

     $hidden_child = "";
     if ($head != '') {
      $hidden_child = "hidden-content";
     }

     $style_margin .= "margin-top:-8px;";
     echo '
					<li data_id="' . $value['id'] . '"'
     . ' parent="' . $head . '" style="' . $style_margin . '" '
     . 'class="nav-item hidden-content content-left-menu left_menu_' . $room . ' ' . $hidden_child . '" '
     . 'room_name="' . $value['title'] . '" '
     . 'room="' . $room . '" data_id="' . $value['id'] . '">
						<a class="nav-link"><i data-toggle="tooltip" title="Maximize/Minimize" 
      class="fa fa-check-circle hover" 
      onclick="Dashboard.showChild(this)"></i>&nbsp;
      <i class="fa fa-file-text-o hover" data-toggle="tooltip" title="' . $value['title'] . '"
      onclick="Dashboard.detailBody(this)"></i>
      &nbsp;&nbsp;&nbsp;' . substr($value['title'], 0, 20) . '
      </a>';
     echo '
					</li>
				';
     if ($this->hak_akses == 'superadmin') {
      echo '<li parent="' . $head . '" room="' . $room . '" '
      . 'room_name="' . $value['title'] . '" '
      . 'data_id="' . $value['id'] . '" 
               class="nav-item content-left-menu left_menu_' . $room . ' hidden-content text-right ' . $hidden_child . '">
           <a class="nav-link">
            <i parent="' . $value['id'] . '" room="' . $room . '" 
             class="fa fa-lock text-primary hover" data-toggle="tooltip" 
             title="Permission User" onclick="Dashboard.addPermissionHeader(this, ' . $value['id'] . ')"></i> 
            &nbsp;&nbsp;
            <i parent="' . $value['id'] . '" room="' . $room . '" 
             class="fa fa-plus text-primary hover" data-toggle="tooltip" 
             title="Tambah" onclick="Dashboard.addChildRoom(this)"></i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Hapus" 
            class="fa fa-trash text-primary hover" 
            onclick=\'Dashboard.deleteRoom("' . $value['id'] . '", "room_header")\'></i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Ubah" 
            class="fa fa-pencil text-primary hover" 
            onclick="Dashboard.editLeftMenu(this)"></i>
           </a>
          </li>
          <li parent="' . $head . '" class="' . $hidden_child . ' hidden-content content-left-menu"><hr/></li>';
     }
     $this->buildMenuRoomHeader($value['children'], 1, $room, $value['id']);
    } else {
     $style_margin = "margin-left:16px;";
     if ($head != '') {
      if ($is_parent == 1) {
       $point = 28;
//       $point +=20;
      } else {

       $point = 16;
      }
//      $point +=20;
      $style_margin = "margin-left:" . $point . "px;";
     }
     $file_action = "";
     if ($is_parent != 1) {
      $point = 16 * $value['id'] / 10;
      if ($value['parent'] != '') {
       $style_margin = "margin-left:" . $point . "px;";
      } else {
       $style_margin = "";
      }
     } else {
      if ($value['file'] != '') {
       $file = str_replace(' ', '_', $value['file']);
       $file = base_url() . 'files/berkas/document/' . $file;
       $file_action .= '&nbsp;&nbsp;<i data-toggle="tooltip" '
               . 'title="Lihat ' . $value['file'] . '" '
               . 'file="' . $value['file'] . '" '
               . 'tipe="' . $value['tipe_upload'] . '" '
               . 'class="fa fa-file-archive-o text-primary hover" '
               . 'onclick="Dashboard.showFileRoomHeader(this)"></i>';
       $file_action .= '&nbsp;&nbsp;<i nama_file="' . $value['file'] . '" '
               . 'file="' . $file . '" data-toggle="tooltip" '
               . 'title="Download ' . $value['file'] . '" '
               . 'class="fa fa-download text-primary hover" '
               . 'onclick="Dashboard.downloadBodyDetail(this, ' . $value['id'] . ')"></i>';
      }
     }
     $hidden_child = "";
     if ($head != '') {
      $hidden_child = "hidden-content";
     }

     $style_margin .= "margin-top:-8px;";

     echo '
					<li parent="' . $head . '"  style="' . $style_margin . '" '
     . 'class="nav-item hidden-content content-left-menu left_menu_' . $room . ' ' . $hidden_child . '" '
     . 'room_name="' . $value['title'] . '" '
     . 'room="' . $room . '" '
     . 'data_id="' . $value['id'] . '">
						<a class="nav-link"><i class="fa fa-file-text-o hover" 
      onclick="Dashboard.detailBody(this)" data-toggle="tooltip" title="' . $value['title'] . '"></i>
      &nbsp;&nbsp;&nbsp;' . substr($value['title'], 0, 20) . '
      </a>
					</li>
					';

     if ($this->hak_akses == 'superadmin') {
      if ($is_parent != 1) {
       echo '<li file="' . $value['file'] . '" room="' . $room . '" '
       . 'room_name="' . $value['title'] . '" '
       . 'data_id="' . $value['id'] . '" '
       . 'parent=""
        class="nav-item text-right content-left-menu left_menu_' . $room . ' hidden-content" style="margin-top:-8px;">
           <a class="nav-link">
           <i parent="' . $value['id'] . '" room="' . $room . '" 
             class="fa fa-lock text-primary hover" data-toggle="tooltip" 
             title="Permission User" onclick="Dashboard.addPermissionHeader(this, ' . $value['id'] . ')"></i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Tambah" parent="' . $value['id'] . '" '
       . 'room="' . $room . '" class="fa fa-plus text-primary hover" 
                onclick="Dashboard.addChildRoom(this)"></i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Hapus" 
            class="fa fa-trash text-primary hover" 
            onclick=\'Dashboard.deleteRoom("' . $value['id'] . '", "room_header")\'>
             </i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Ubah" 
            class="fa fa-pencil text-primary hover" 
            onclick="Dashboard.editLeftMenu(this)"></i>';
//       echo $file_action;
       echo '</a></li>';
//          <li class="hidden-content content-left-menu"><hr/></li>';
      } else {
       //yang punya anak terakhir
       if ($style_margin != '') {
        echo '<li parent="' . $head . '" file="' . $value['file'] . '" '
        . 'room="' . $room . '" '
        . 'room_name="' . $value['title'] . '" '
        . 'data_id="' . $value['id'] . '" '
        . 'class="nav-item content-left-menu left_menu_' . $room . ' hidden-content '
        . 'text-right ' . $hidden_child . '">
           <a class="nav-link">
           <i parent="' . $value['id'] . '" room="' . $room . '" 
             class="fa fa-lock text-primary hover" data-toggle="tooltip" 
             title="Permission User" onclick="Dashboard.addPermissionHeader(this, ' . $value['id'] . ')"></i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Tambah" 
            parent="' . $value['id'] . '" room="' . $room . '" 
             class="fa fa-plus text-primary hover" 
             onclick="Dashboard.addChildRoom(this)"></i>              
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Hapus" 
            class="fa fa-trash text-primary hover" 
            onclick=\'Dashboard.deleteRoom("' . $value['id'] . '", "room_header")\'>
             </i> 
            &nbsp;&nbsp;
            <i data-toggle="tooltip" title="Ubah" 
            class="fa fa-pencil text-primary hover" 
            onclick="Dashboard.editLeftMenu(this)"></i>';
        echo $file_action;
        echo '</a></li>
          <li parent="' . $head . '" class="' . $hidden_child . ' hidden-content content-left-menu"><hr/></li>';
       }
      }
     } else {
      if ($is_parent == 1) {
       echo '<li parent="' . $head . '" file="' . $value['file'] . '" '
       . 'room="' . $room . '" '
       . 'room_name="' . $value['title'] . '" '
       . 'data_id="' . $value['id'] . '" 
                class="nav-item text-right hidden-content 
                content-left-menu left_menu_' . $room . ' ' . $hidden_child . '" 
                 style="margin-top:-8px;">
           <a class="nav-link">';
       echo $file_action;
//       echo '&nbsp;&nbsp;&nbsp;<i data-toggle="tooltip" '
//       . 'title="Ubah Urutan" class="fa fa-sort '
//       . 'fa-lg text-primary hover" room="' . $room . '" '
//               . 'onclick="Dashboard.ubahUrutan(this, ' . $value['id'] . ')"></i>';
       echo '</a></li>';
//          <li class="content-left-menu hidden-content ' . $hidden_child . '" parent="' . $head . '"><hr/></li>';
      }
     }
    }
   }
  }
 }

 public function detailMenu($user_id) {
  $sql = "select fs.user, 
				fs.feature as id,f.name, f.url
				, f.parent
    , fp.id as parent_feature
    , f.icon
				from feature_set fs
				join feature f
					on fs.feature = f.id
				join `user` u
					on u.id = fs.`user`
    left join feature fp
     on fp.id = f.parent 
     where u.id = '" . $user_id . "'
					order by f.id";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  $tree = $this->buildTree($result);
//  echo '<pre>';
//  print_r($tree);die;
  $menu = $this->buildTableMenu($tree);
//  echo '<pre>';
//  print_r($data);
//  echo $menu;
 }

 function buildTree(array $elements, $parentId = 0) {
  $branch = array();

  foreach ($elements as $element) {
   if ($element['parent'] == $parentId) {
    $children = $this->buildTree($elements, $element['id']);
    if ($children) {
//              print_r($children);die;
     $element['children'] = $children;
     $element['jumlah_anak'] = count($children);
    }
    $branch[$element['id']] = $element;
   }
  }

  return $branch;
 }

 function buildTableMenu($arr, $is_parent = 0) {
  foreach ($arr as $value) {
   if (!empty($value['children'])) {
    echo '
      <tr>
       <td>' . $value['name'] . '</td>
      <tr>
				';
    $this->buildTableMenu($value['children'], 1);
   } else {
    $class = '';
    if ($is_parent == 1) {
     $class = '&nbsp;&nbsp;<i class="fa fa-file-text"></i>&nbsp;&nbsp;';
    }
    echo '
					<tr>
						<td>' . $class . $value['name'] . '</td>
					</tr>
					';
   }
  }
 }

 function buildSortTableMenu($arr, $is_parent = 0, $nama_parent = "", $parent_id = "") {
  foreach ($arr as $value) {
   if (!empty($value['children'])) {
    if ($nama_parent == '') {
     echo '
      <tr parent="" data_id="' . $value['id'] . '">
       <td><i class="fa fa-sort fa-lg"></i>&nbsp;' . $value['title'] . '</td>
      <tr>
				';
    } else {
     echo '
      <tr parent="' . $parent_id . '" data_id="' . $value['id'] . '">
       <td><i class="fa fa-sort fa-lg"></i>&nbsp;' . '<b>[' . $nama_parent . ']</b>' . ' ' . $value['title'] . '</td>
      <tr>
				';
    }
    $this->buildSortTableMenu($value['children'], 1, $value['title'], $value['id']);
   } else {
    $class = '';
    if ($is_parent == 1) {
     $class = '';
    }

    if ($nama_parent == '') {
     echo '
					<tr parent="" data_id="' . $value['id'] . '">
						<td><i class="fa fa-sort fa-lg"></i>&nbsp;' . $class . $value['title'] . '</td>
					</tr>
					';
    } else {
     echo '
					<tr parent="' . $parent_id . '" data_id="' . $value['id'] . '">
						<td><i class="fa fa-sort fa-lg"></i>&nbsp;' . $class . '<b>[' . $nama_parent . ']</b>' . ' ' . $value['title'] . '</td>
					</tr>
					';
    }
   }
  }
 }

 function buildMenu($arr, $is_parent = 0) {
  foreach ($arr as $value) {

   if (!empty($value['children'])) {
    echo '
					<li class="treeview">
						<a class="app-menu__item" href="#" data-toggle="treeview">
							<i class="app-menu__icon ' . $value['icon'] . '"></i> <span class="app-menu__label">' . $value['name'] . '</span>
							<i class="treeview-indicator fa fa-angle-right"></i></a>
						</a>
						<ul class="treeview-menu">
				';
    $this->buildMenu($value['children'], 1);
    echo '
						</ul>
					</li>
				';
   } else {
    $class = 'class="app-menu__item"';
    if ($is_parent == 1) {
     $class = 'class="treeview-item';
     echo '
					<li>
						<a class="treeview-item" href="' . base_url() . '' . $value['url'] . '">
							<i class="icon ' . $value['icon'] . '"></i>' . $value['name'] . '
						</a>
					</li>
					';
    } else {
     echo '
					<li>
						<a ' . $class . ' href="' . base_url() . '' . $value['url'] . '">
							<i class="app-menu__icon ' . $value['icon'] . '"></i> <span class="app-menu__label">' . $value['name'] . '</span>
						</a>
					</li>
					';
    }
   }
  }
 }

}
