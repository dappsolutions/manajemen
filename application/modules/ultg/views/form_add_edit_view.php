<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i onclick="Ultg.main()" class="hover fa fa-file-text-o"></i>&nbsp;&nbsp;<?php echo ucfirst($module) ?>
     <hr/>
    </div>
   </div>
   <div class="col-md-9">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Form' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-1">ULTG</label>
        <div class="col-sm-5">
         <input class="form-control required" error="Ultg" id="ultg" type="text" placeholder="Masukkan Ultg" value="<?php echo isset($nama) ? $nama : '' ?>">
        </div>        
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="Ultg.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Ultg.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
