<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo strtoupper($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i onclick="Ultg.main()" class="hover fa fa-file-text-o"></i>&nbsp;&nbsp;<?php echo strtoupper($title_content) ?>
     <hr/>
    </div>
   </div>
   <div class="col-md-9">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Daftar' ?>
     </div>
    </div>
    <div class="tile">
     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" type="search" onkeyup="Ultg.search(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>    

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="table-warning">
            <th>#</th>
            <th>UlTG</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['nama'] ?></td>
              <td class="text-center">
               <i class="fa fa-trash grey-text hover" onclick="Ultg.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-pencil grey-text hover" onclick="Ultg.ubah('<?php echo $value['id'] ?>')"></i>              
               &nbsp;
               <i class="fa fa-file-text grey-text hover" onclick="Ultg.detail('<?php echo $value['id'] ?>')"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr class="text-center">
             <td colspan="3">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div> 
   </div>
  </div>
 </div>
</div>
<a href="#" class="float" onclick="Ultg.add()">
 <i class="fa fa-plus my-float fa-lg"></i>
</a>