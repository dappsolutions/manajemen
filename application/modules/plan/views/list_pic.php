<input type="hidden" value="<?php echo $index_content ?>" id="index_content" class="form-control" />

<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;Daftar
     </div>
    </div>

    <div class="row">
     <div class="col-md-12">
      <div class="form-group">
       <div class="app-search">
        <input class="form-control" type="search" onkeyup="Plan.searchInTablePegawai(this, event)" id="keyword" placeholder="Pencarian">
        <button class="app-search__button"><i class="fa fa-search"></i></button>
       </div>
      </div>
     </div>
    </div>     

    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <div class="" style="height: 300px;overflow: auto;">
        <table class="table table-bordered" id="tb_pegawai">
         <thead>
          <tr class="table-warning">
           <th class="text-center">No</th>
           <th>NIP</th>
           <th>Nama</th>      
           <th>Action</th>      
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($data_pegawai)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($data_pegawai as $value) { ?>
            <tr user_id = "<?php echo $value['user_id'] ?>">
             <td class="text-center"><?php echo $no++ ?></td>
             <td><?php echo $value['nip'] ?></td>
             <td><?php echo $value['nama'] ?></td>
             <td>
              <button class="btn btn-info hover-content" onclick="Plan.exeChoosePIC(this)">Pilih</button>
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr class="text-center">
            <td colspan="8">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <div class="text-right">
       <button class="btn btn-warning hover-content" onclick="Plan.savePermissionUser(this)">Proses</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>