<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <!--   <div class="col-md-3">
       <div class="tile">
        <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;<?php echo $title_content ?></h5>
       </div>
      </div>-->
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Form Data' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <div class="row">
       <div class="col-md-3 ">
        <b>Nama Workplan</b>
       </div> 
       <div class="text-success">
        <?php echo $title_wp ?>
       </div>        
      </div>
      <hr/>

      <div class="row">
       <div class="col-md-3 ">
        <b>Daftar Item</b>
       </div>        
      </div>
      <br/>

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_field">
          <thead>
           <tr class="table-warning">
            <th rowspan="2">Activities</th>
            <th rowspan="2">Deadline</th>
            <th class="text-center" rowspan="2">PIC</th>
            <th class="text-center" rowspan="2">Status <br/>(Plan/Real)</th>
            <th class="text-center" id='th_timeline' colspan="<?php echo $colspan_timeline ?>">Timeline (weekly)</th>
            <!--<th class="text-center" colspan="2" rowspan="3">Action</th>-->
           </tr>           
           <tr>
            <?php if (!empty($list_month)) { ?>
             <?php foreach ($list_month as $v_m) { ?>
              <td class="text-center bulan_column" colspan="4">
               <select disabled="" class="form-control" id='bulan' error="Bulan">
                <option value="">Pilih Bulan</option>
                <?php if (!empty($list_bulan)) { ?>
                 <?php foreach ($list_bulan as $key => $value) { ?>
                  <?php $selected = $key == $v_m['bulan'] ? 'selected' : '' ?>                
                  <option <?php echo $selected ?> value="<?php echo $key ?>"><?php echo $value ?></option>
                 <?php } ?>
                <?php } ?>
               </select>
  <!--               <i style="margin-top: 12px;" class="fa fa-trash fa-lg hover-content" 
                  onclick="Plan.removeHeader(this)"></i>-->
              </td>
             <?php } ?>            
            <?php } ?>            
           </tr>
           <tr>
            <td colspan="4"></td>
            <?php if (!empty($list_month)) { ?>
             <?php foreach ($list_month as $v_m) { ?>              
              <td class="text-center week">1</td>
              <td class="text-center week">2</td>
              <td class="text-center week">3</td>
              <td class="text-center week">4</td>
             <?php } ?>            
            <?php } ?>            
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($list_item)) { ?>
            <?php foreach ($list_item as $value) { ?>
             <tr no="1" data_id="" id="tr_plan_1" class="plan">
              <td rowspan="2">
               <input readonly="" type="text" value="<?php echo $value['title'] ?>" id="nama_item" class="form-control" placeholder="Activities"/>
              </td>
              <td rowspan="2"> 
               <input type="text" value="<?php echo $value['tanggal'] ?>" readonly="" id="tanggal" class="form-control" placeholder="Deadline"/>
              </td>            
              <td rowspan="2" class="text-center">
               <?php echo $value['nama_pegawai'] ?>
               <!--               <button class="btn btn-primary" onclick="Plan.choosePIC(this, event)">PIC</button>-->
              </td>
              <td>Plan</td>
              <?php if (!empty($value['list_month'])) { ?>
               <?php foreach ($value['list_month'] as $v_plan) { ?>
                <?php foreach ($v_plan['list_week']['plan'] as $v_p) { ?>
                 <td class="week text-center" style="font-size: 12px;" data_id="<?php echo $v_p['id'] ?>">
                  <?php if ($v_p['tanggal'] == '') { ?>
                   <i class="fa fa-calendar fa-lg hover-content" style="font-size: 14px;" onclick="Plan.showCalendar(this)"></i>
                  <?php } else { ?>
                   <?php echo $v_p['tanggal'] ?>
                  <?php } ?>
                 </td>
                <?php } ?>              
               <?php } ?>              
              <?php } ?>              
  <!--              <td class="text-center" rowspan="2">
  <i style="margin-top: 12px;" class="fa fa-minus fa-lg hover" onclick="Plan.removeField(this)"></i>
  </td>-->
             </tr>
             <tr id="tr_real" class="real">
              <td>Real</td>
              <?php if (!empty($value['list_month'])) { ?>
               <?php foreach ($value['list_month'] as $v_plan) { ?>
                <?php foreach ($v_plan['list_week']['real'] as $v_real) { ?>
                 <td class="week text-center" style="font-size: 12px;" data_id="<?php echo $v_real['id'] ?>">
                  <?php if ($v_real['tanggal'] == '') { ?>
                   <i class="fa fa-calendar fa-lg hover-content" style="font-size: 14px;" onclick="Plan.showCalendar(this)"></i>
                  <?php } else { ?>
                   <?php echo $v_real['tanggal'] ?>
                  <?php } ?>
                 </td>
                <?php } ?>              
               <?php } ?>              
              <?php } ?>    
             </tr>
            <?php } ?>
           <?php } ?>                      
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Plan.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>

 <!-- <div class="col-md-3">
   <div class="tile">
    <div class="tile-body">
     <div class="row">
      <div class="col-md-12">
       <select class="form-control required" id="pegawai" error="Pegawai">
        <option value="">Pilih Pegawai</option>
 <?php if (!empty($list_pegawai)) { ?>
  <?php foreach ($list_pegawai as $value) { ?>
   <?php $selected = '' ?>
                                                          <option <?php echo $selected ?> value="<?php echo $value['user_id'] ?>"><?php echo $value['nip'] . ' -' . $value['nama'] ?></option>
  <?php } ?>
 <?php } ?>
       </select>            
      </div>
     </div>
     <br/>
     <div class="row">
      <div class="col-md-12 text-right">
       <button class="btn btn-warning" onclick="Plan.simpanPegawai(this)">Tambah</button>
      </div>
     </div>
    </div>
   </div>
 
   <div class="tile" style="margin-top: -16px;">
    Entri Pegawai
    <hr/>
    <div class="tile-body">
     <table class="table table-bordered" id="tb_pegawai">
      <thead>
       <tr class="table-warning">
        <th>Nama Pegawai</th>
        <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody>
 <?php if ($list_entriuser) { ?>
  <?php foreach ($list_entriuser as $value) { ?>
                                                         <tr  data_id="<?php echo $value['id'] ?>">
                                                          <td>
   <?php echo $value['nama_pegawai'] ?>
                                                          </td>         
                                                          <td class="text-center">
                                                           <i class="fa fa-trash hover fa-lg" onclick="Plan.deletePegawai('<?php echo $value['id'] ?>')"></i>
                                                          </td>         
                                                         </tr>
  <?php } ?>
 <?php } else { ?>
                                <tr>
                                 <td colspan="3" class="text-center">Tidak ada data ditemukan</td>
                                </tr>
 <?php } ?>
   </tbody>
  </table>
 </div>
</div>
</div>-->
</div>
