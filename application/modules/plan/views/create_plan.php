<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text"></i>&nbsp;&nbsp;&nbsp; <?php echo 'Create Workplan  ' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-3">Judul Workplan</label>
        <div class="col-sm-5">
         <input class="form-control required" error="Nama Workplan" id="title" type="text" placeholder="Nama Workplan" value="">
        </div>        
       </div>
       <hr/>
       <div class="form-group">
        <label class="control-label col-sm-5">Masukkan Detail Workplan</label>
        <div class="col-sm-12">
         <div class="table-responsive">
          <table class="table table-bordered" id="tb_field">
           <thead>
            <tr class="table-warning">
             <th rowspan="2">Activities</th>
             <th rowspan="2">Deadline</th>
             <th class="text-center" rowspan="2">PIC</th>
             <th class="text-center" rowspan="2">Status <br/>(Plan/Real)</th>
             <th class="text-center" id='th_timeline' colspan="4">Timeline (weekly)</th>
             <th class="text-center" colspan="2" rowspan="3">Action</th>
            </tr>
            <tr>
             <td class="text-center bulan_column" colspan="4">
              <select class="form-control" id='bulan' error="Bulan">
               <option value="">Pilih Bulan</option>
               <?php if (!empty($list_bulan)) { ?>
                <?php foreach ($list_bulan as $key => $value) { ?>
                 <option value="<?php echo $key ?>"><?php echo $value ?></option>
                <?php } ?>
               <?php } ?>
              </select>
              <i style="margin-top: 12px;" class="fa fa-plus fa-lg hover-content" 
                 onclick="Plan.addHeader(this)"></i>
             </td>
            </tr>
            <tr>
             <td colspan="4"></td>
             <td class="text-center week">1</td>
             <td class="text-center week">2</td>
             <td class="text-center week">3</td>
             <td class="text-center week">4</td>
            </tr>
           </thead>
           <tbody>
            <tr no="1" data_id="" id="tr_plan_1" class="plan">
             <td rowspan="2">
              <input type="text" value="" id="nama_item" class="form-control" placeholder="Activities"/>
             </td>
             <td rowspan="2"> 
              <input type="text" value="" readonly="" id="tanggal" class="form-control" placeholder="Deadline"/>
             </td>            
             <td rowspan="2" class="text-center">
              <button class="btn btn-primary" onclick="Plan.choosePIC(this, event)">PIC</button>
             </td>
             <td>Plan</td>
             <td class="week">&nbsp;</td>
             <td class="week">&nbsp;</td>
             <td class="week">&nbsp;</td>
             <td class="week">&nbsp;</td>
             <td class="text-center" rowspan="2">
              <i style="margin-top: 12px;" class="fa fa-plus fa-lg hover" onclick="Plan.addField(this)"></i>
             </td>
            </tr>
            <tr id="tr_real" class="real">
             <td>Real</td>
             <td class="week">&nbsp;</td>
             <td class="week">&nbsp;</td>
             <td class="week">&nbsp;</td>
             <td class="week">&nbsp;</td>
            </tr>
           </tbody>
          </table>
         </div>

        </div>
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" 
              onclick="Plan.simpanPlan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Plan.back()">
       <i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
