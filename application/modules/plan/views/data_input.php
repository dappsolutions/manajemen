<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Daftar ' . ucfirst($table_name) ?>
     </div>
    </div>
    <div class="tile">

     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" table="<?php echo $table_id ?>" type="search" onkeyup="Basedata.searchData(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>  

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="table-warning">
            <?php if (!empty($kolom)) { ?>           
             <?php foreach ($kolom as $v_head) { ?>
              <th><?php echo ucfirst($v_head['nama_field']) ?></th>
             <?php } ?>
            <?php } ?>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($value_data)) { ?>            
            <?php foreach ($value_data as $key => $value) { ?>
             <tr>
              <?php foreach ($value as $v_detail) { ?>
               <td>
                &nbsp;
                <?php echo $v_detail['isi'] ?>
               </td>               
              <?php } ?>
              <td class="text-center">
               <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
                <i class="fa fa-pencil grey-text hover" 
                   onclick="Basedata.ubahData('<?php echo $key ?>')"></i>
                &nbsp;
               <?php } ?>               
               <i class="fa fa-file-text grey-text hover" 
                  onclick="Basedata.detailData('<?php echo $key ?>')"></i>
               &nbsp;
               <?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
                <i data-toggle="tooltip" title="Hapus Data"  class="fa fa-trash grey-text hover" 
                   onclick="Basedata.hapusData('<?php echo $key ?>')"></i>
               <?php } ?>               
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr class="text-center">
             <td colspan="7">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php // echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>

     <!--     <div class="tile-footer text-right">
           <i class="fa fa-plus-circle fa-2x hover"></i>
          </div>-->
    </div> 
   </div>
  </div>
 </div>
</div>

<?php if ($has_access == 1) { ?>
 <a href="#" class="float" onclick="Basedata.addValue('<?php echo $table_id ?>')">
  <i class="fa fa-plus my-float fa-lg"></i>
 </a>
 <?php
}?>