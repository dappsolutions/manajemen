<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text"></i>&nbsp;&nbsp;&nbsp; <?php echo ucfirst($title_content) ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-3">Nama Work Plan</label>
        <div class="col-sm-5">
         <input class="form-control required" error="Nama Workplan" id="title" 
                type="text" placeholder="Nama Workplan" value="<?php echo isset($title_wp) ? $title_wp : '' ?>">
        </div>        
       </div>
       <hr/>
       <div class="form-group">
        <label class="control-label col-sm-5">Masukkan Item Workplan</label>
        <div class="col-sm-12">
         <table class="table table-bordered" id="tb_field">
          <thead>
           <tr class="table-warning">
            <th>Item</th>
            <th>Tanggal</th>
            <th>Keterangan</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if ($list_item) { ?>
            <?php foreach ($list_item as $value) { ?>
             <tr  data_id="<?php echo $value['id'] ?>">
              <td>
               <input type="text" value="<?php echo $value['title'] ?>" id="nama_item" class="form-control" placeholder="Nama Item"/>
              </td>
              <td>
               <input type="text" value="<?php echo $value['tanggal'] ?>" id="tanggal" class="form-control" placeholder="Tanggal"/>
              </td>
              <td>
               <textarea class="form-control" id="keterangan"><?php echo $value['keterangan'] ?></textarea>
              </td>
              <td class="text-center">
               <i class="fa fa-minus fa-lg hover" onclick="Plan.removeFieldData(this)"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } ?>
           <tr data_id="">
            <td>
             <input type="text" value="" id="nama_item" class="form-control" placeholder="Nama Item"/>
            </td>
            <td>
             <input type="text" value="" id="tanggal" class="form-control" placeholder="Tanggal"/>
            </td>
            <td>
             <textarea class="form-control" id="keterangan"></textarea>
            </td>
            <td class="text-center">
             <i class="fa fa-plus fa-lg hover" onclick="Plan.addField(this)"></i>
            </td>
           </tr>
          </tbody>
         </table>

        </div>
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" 
              onclick="Plan.simpanPlan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Plan.back()">
       <i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
