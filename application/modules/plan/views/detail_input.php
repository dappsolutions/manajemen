<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-bars"></i> <?php echo 'Detail Data' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <?php if (!empty($form)) { ?>
       <?php foreach ($form as $value) { ?>
        <div class="row">
         <div class="col-md-3 ">
          <b><?php echo ucfirst($value['nama_field']) ?></b>
         </div> 
         <div class="text-success">
          <?php echo $value['isi'] ?>
         </div>        
        </div>
        <br/>
       <?php } ?>
      <?php } ?>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Basedata.backData('<?php echo $table_id ?>')">
       <i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
