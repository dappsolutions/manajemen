<div class="row"> 
 <div class="clearfix"></div>
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Daftar' ?>
     </div>
    </div>
    <div class="tile">

     <div class="tile-body">
      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
         <div class="app-search">
          <input class="form-control" type="search" onkeyup="Plan.search(this, event)" id="keyword" placeholder="Pencarian">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
         </div>
        </div>
       </div>
      </div>  

      <?php if (isset($keyword)) { ?>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>     

      <div class="row">
       <div class="col-md-12">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
           <tr class="table-warning">
            <th>Nama Workplan</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td>
               <i class="fa fa-file-text-o"></i>
               &nbsp;
               <?php echo $value['title'] ?>
              </td>
              <?php $class_color = $value['status'] == 'OPEN' ? 'bg-warning' : 'bg-success' ?>
              <td class="<?php echo $class_color ?> text-center">
               <?php echo $value['status'] ?>
              </td>
              <td class="text-center">
               <?php if ($this->session->userdata('hak_akses') == 'superadmin' || $has_akses == 1) { ?>
<!--                <i class="fa fa-pencil grey-text hover" 
                   onclick="Plan.ubah('<?php echo $value['id'] ?>')"></i>
                &nbsp;                -->
               <?php } ?>
               <i class="fa fa-file-text grey-text hover" 
                  onclick="Plan.detail('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-trash grey-text hover" 
                  onclick="Plan.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;
  <!--               <i data-toggle="tooltip" title="Daftar Data"  class="fa fa-table grey-text hover" 
             onclick="Plan.detailInputData('<?php echo $value['id'] ?>')"></i>-->
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr class="text-center">
             <td colspan="7">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
         <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div> 
   </div>
  </div>
 </div>
</div>

<?php if ($this->session->userdata('hak_akses') == 'superadmin' || $has_akses == 1) { ?>
 <a href="#" class="float" onclick="Plan.createPlan()">
  <i class="fa fa-plus my-float fa-lg"></i>
 </a>
<?php } ?>
