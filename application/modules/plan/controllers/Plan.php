<?php

class Plan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $jabatan_id;
 public $struktur_id;
 public $pegawai_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->jabatan_id = $this->session->userdata('jabatan_id');
  $this->struktur_id = $this->session->userdata('struktur_id');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'plan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/plan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'work_plan';
 }

 public function getRootModule() {
  return "Work Plan";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " -  Plan";
  $data['title_content'] = ' Plan';
  $content = $this->getDataPlan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['has_akses'] = $this->getHasAksesWorkPlan();
  echo Modules::run('template', $data);
 }

 public function getHasAksesWorkPlan() {
  $user = $this->session->userdata('user_id');
  $data = Modules::run('database/get', array(
              'table' => 'work_plan_permission wp',
              'where' => "wp.deleted = 0 and user = '" . $user . "'"
  ));


  if (!empty($data)) {
   return 1;
  } else {
   if ($this->akses == 'superadmin') {
    return 1;
   }
  }


  return 0;
 }

 public function getJabatanHasChild() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.parent = '" . $this->struktur_id . "'"
  ));

  $is_has = false;
  if (!empty($data)) {
   $is_has = true;
  } else {
   if ($this->akses == 'superadmin') {
    $is_has = true;
   }
  }

  return $is_has;
 }

 public function getTotalDataPlan($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('wp.title', $keyword),
   );
  }

  $where = "wp.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "wp.deleted = 0";
  } else {
   $where = "wp.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' wp',
                'field' => array('wp.*', 'wps.status'),
                'join' => array(
                    array('(select max(id) as id, work_plan from work_plan_status group by work_plan) wpss', 'wpss.work_plan = wp.id'),
                    array('work_plan_status wps', 'wps.id = wpss.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' wp',
                'field' => array('wp.*', 'wps.status'),
                'join' => array(
                    array('(select max(id) as id, work_plan from work_plan_status group by work_plan) wpss', 'wpss.work_plan = wp.id'),
                    array('work_plan_status wps', 'wps.id = wpss.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataPlan($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('wp.title', $keyword),
   );
  }

  $where = "wp.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "wp.deleted = 0";
  } else {
   $where = "wp.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' wp',
                'field' => array('wp.*', 'wps.status'),
                'join' => array(
                    array('(select max(id) as id, work_plan from work_plan_status group by work_plan) wpss', 'wpss.work_plan = wp.id'),
                    array('work_plan_status wps', 'wps.id = wpss.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' wp',
                'field' => array('wp.*', 'wps.status'),
                'join' => array(
                    array('(select max(id) as id, work_plan from work_plan_status group by work_plan) wpss', 'wpss.work_plan = wp.id'),
                    array('work_plan_status wps', 'wps.id = wpss.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPlan($keyword)
  );
 }

 public function getDetailDataPlan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pjd',
              'field' => array('pjd.*', 'p.nama as nama_pegawai',
                  'p.nip', 'pg.nama as nama_pemberi',
                  'jds.status', 'jd.jenis', 'ut.id as user_pegawai'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user ut', 'ut.pegawai = p.id'),
                  array('user u', 'pjd.giver = u.id', 'left'),
                  array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                  array('jabatan j', 'pg.jabatan = j.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
                  array('jenis_job jd', 'pjd.jenis_job = jd.id'),
              ),
              'where' => "pjd.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getDetailPlan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName(),
              'where' => "deleted = 0 and id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListItemPlan($wp) {
  $data = Modules::run('database/get', array(
              'table' => "	work_plan_item wpi",
              'field' => array('wpi.*', 'pg.nama as nama_pegawai'),
              'join' => array(
                  array('user u', 'wpi.user = u.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "wpi.deleted = 0 and wpi.work_plan = '" . $wp . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['list_month'] = $this->getListMonthPlan($value['id']);
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);
//  die;

  return $result;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => "pegawai pg",
              'field' => array('pg.*', 'u.id as user_id'),
              'join' => array(
                  array('user u', 'u.pegawai = pg.id')
              ),
              'where' => "pg.deleted = 0",
              "limit" => 2000
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function ubah($id) {
  $data = $this->getDetailPlan($id);
  $data['title_wp'] = $data['title'];

  $data['view_file'] = 'form_add_edit';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Data";
  $data['title_content'] = 'Ubah Data';
  $data['list_item'] = $this->getListItemPlan($data['id']);

  echo Modules::run('template', $data);
 }

 public function getListEntriUser($table) {
  $data = Modules::run('database/get', array(
              'table' => 'room_table_permission rtp',
              'field' => array('rtp.*', 'pg.nama as nama_pegawai'),
              'join' => array(
                  array('user u', 'rtp.user = u.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "rtp.deleted = 0 and rtp.room_body_table = '" . $table . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailPlan($id);
  $data['title_wp'] = $data['title'];
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Data";
  $data['title_content'] = 'Detail Data';
  $data['list_item'] = $this->getListItemPlan($data['id']);
  $data['list_month'] = $this->getListMonthPlanHeader($data['id']);
  $data['list_pegawai'] = $this->getListPegawai();
//  echo '<pre>';
//  print_r($data['list_item']);die;
  $data['colspan_timeline'] = 4 * count($data['list_month']);
  $data['list_bulan'] = $this->getListBulan();
//  $data['list_entriuser'] = $this->getListEntriUser($data['id']);
  $data['list_entriuser'] = array();
  echo Modules::run('template', $data);
 }

 public function getListMonthPlanHeader($wp) {
  $data = Modules::run('database/get', array(
              'table' => 'work_plan_item_timeline wpi',
              'field' => array('distinct(wpi.bulan) as bulan'),
              'join' => array(
                  array('work_plan_item wi', 'wpi.work_plan_item = wi.id')
              ),
              'where' => "wpi.deleted = 0 and wi.work_plan = '" . $wp . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListMonthPlan($wpItem) {
  $data = Modules::run('database/get', array(
              'table' => 'work_plan_item_timeline wpi',
              'field' => array('wpi.*'),
              'where' => "wpi.deleted = 0 and wpi.work_plan_item = '" . $wpItem . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['list_week'] = $this->getListWeekOfMonth($value['id']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListWeekOfMonth($wp_timeline) {
  $data = Modules::run('database/get', array(
              'table' => 'work_plan_item_timeline_day wd',
              'field' => array('wd.*'),
              'where' => "wd.deleted = 0 "
              . "and wd.work_plan_item_timeline = '" . $wp_timeline . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $plan = array();
   $real = array();
   foreach ($data->result_array() as $value) {
    if ($value['plan'] == 'plan') {
     array_push($plan, $value);
    } else {
     array_push($real, $value);
    }
   }

   $result['plan'] = $plan;
   $result['real'] = $real;
  }


  return $result;
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Plan";
  $data['title_content'] = 'Data Plan';
  $content = $this->getDataPlan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function simpanPlan() {
  $data = json_decode($this->input->post('data'));
  $id = $data->id;

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_wp['title'] = $data->title;

   $wp = $id;
   if ($id == "") {
    $post_wp['tanggal_buat'] = date('Y-m-d');
    $wp = Modules::run('database/_insert', "work_plan", $post_wp);

    //plan item
    if (!empty($data->plan)) {
     foreach ($data->plan as $value) {
      $post_item['work_plan'] = $wp;
      $post_item['user'] = $value->pic;
      $post_item['title'] = $value->nama_item;
      $post_item['tanggal'] = $value->deadline;
      $plan_item = Modules::run('database/_insert', 'work_plan_item', $post_item);


      //plan timeline
      $counter = 1;
      if (!empty($data->plan_timeline)) {
       for ($i = 0; $i < count($data->plan_timeline); $i++) {
        $post_timeline['work_plan_item'] = $plan_item;
        $post_timeline['bulan'] = $data->plan_timeline[$i]->timeline;
//        echo '<pre>';
//        print_r($post_timeline);die;
        $plan_timeline = Modules::run('database/_insert',
                        'work_plan_item_timeline', $post_timeline);

        $end_index = 4 * $counter;
        $first_index = $end_index - 4;
        $index = 1;

        //plan timeline week        
        $post_week = array();
        for ($x = $first_index; $x < $end_index; $x++) {
         $post_week['week'] = $index++;
         $post_week['plan'] = 'plan';
         $post_week['work_plan_item_timeline'] = $plan_timeline;
         Modules::run('database/_insert', 'work_plan_item_timeline_day', $post_week);
        }

        //real timeline week        
        $index = 1;
        $post_week = array();
        for ($x = $first_index; $x < $end_index; $x++) {
         $post_week['week'] = $index++;
         $post_week['plan'] = 'real';
         $post_week['work_plan_item_timeline'] = $plan_timeline;
         Modules::run('database/_insert', 'work_plan_item_timeline_day', $post_week);
        }

        $counter += 1;
       }
      }
     }
    }

    //plan status
    $post_status['status'] = 'OPEN';
    $post_status['work_plan'] = $wp;
    Modules::run('database/_insert', 'work_plan_status', $post_status);
   } else {
    Modules::run('database/_update', $this->getTableName(), $post_wp, array('id' => $id));
   }

   if (!empty($data->item)) {
    foreach ($data->item as $value) {
     $post_item['work_plan'] = $wp;
     $post_item['title'] = $value->item;
     $post_item['tanggal'] = $value->tanggal;
     $post_item['keterangan'] = $value->keterangan;

     if ($value->id == "") {
      Modules::run('database/_insert', 'work_plan_item', $post_item);
     } else {
      if ($value->deleted == 1) {
       $post_item['deleted'] = 1;
      }

      Modules::run('database/_update', "work_plan_item", $post_item,
              array('id' => $value->id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function simpanPegawai() {
  $table = $this->input->post('table');
  $user = $this->input->post('user_id');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_tb['room_body_table'] = $table;
   $post_tb['user'] = $user;
   $data = Modules::run('database/get', array(
               'table' => 'room_table_permission',
               'where' => "deleted = 0 and user = '" . $user . "' "
               . "and room_body_table = '" . $table . "'"
   ));
   if (empty($data)) {
    Modules::run('database/_insert', "room_table_permission", $post_tb);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array(
      'is_valid' => $is_valid
  ));
 }

 public function deletePegawai($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', "room_table_permission", array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function deleteData($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', "room_body_row_uniq", array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getFieldData($id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_table_has_field rf',
              'field' => array('rf.*'),
              'where' => "rf.deleted = 0 and rf.room_body_table = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getFieldValueData($id, $keyword = "") {
  $like = array();
  if ($keyword != "") {
   $like = array(
       array('rfv.isi', $keyword)
   );
   $data = Modules::run('database/get', array(
               'table' => 'room_body_row_uniq rfq',
               'field' => array('rfq.*', 'rfv.isi'),
               'join' => array(
                   array('room_body_field_value rfv', 'rfv.room_body_row_uniq = rfq.id'),
               ),
               'is_or_like' => true,
               'like' => $like,
               'inside_brackets' => true,
               'where' => "rfq.deleted = 0 and rfq.room_body_table = '" . $id . "'"
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'room_body_row_uniq rfq',
               'field' => array('rfq.*', 'rfv.isi'),
               'join' => array(
                   array('room_body_field_value rfv', 'rfv.room_body_row_uniq = rfq.id'),
               ),
               'where' => "rfq.deleted = 0 and rfq.room_body_table = '" . $id . "'"
   ));
  }

  $result = array();
  if (!empty($data)) {
   $temp = array();
   foreach ($data->result_array() as $value) {
    if (!in_array($value['id'], $temp)) {
     $detail = array();
     foreach ($data->result_array() as $v_d) {
      if ($v_d['id'] == $value['id']) {
       array_push($detail, $v_d);
      }
     }
     $result[$value['id']] = $detail;

     $temp[] = $value['id'];
    }
   }
  }


  return $result;
 }

 public function data_input($id) {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $table_data = $this->getDetailTable($id);
  $data['table_name'] = $table_data['nama'];
  $data['view_file'] = 'data_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['title_content'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['kolom'] = $this->getFieldData($id);
  $data['value_data'] = $this->getFieldValueData($id);
//  echo '<pre>';
//  print_r($data['value_data']);die;
  $data['table_id'] = $id;
  $data['has_access'] = $this->hasAccessTable($id);

  echo Modules::run('template', $data);
 }

 public function searchData($id, $keyword) {

  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $table_data = $this->getDetailTable($id);
  $data['table_name'] = $table_data['nama'];
  $data['view_file'] = 'data_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['title_content'] = $this->getRootModule() . " - " . ucfirst($table_data['nama']);
  $data['kolom'] = $this->getFieldData($id);
  $data['value_data'] = $this->getFieldValueData($id, $keyword);
//  echo '<pre>';
//  print_r($data['value_data']);die;
  $data['table_id'] = $id;
  $data['has_access'] = $this->hasAccessTable($id);

  echo Modules::run('template', $data);
 }

 public function hasAccessTable($id) {
  $user = $this->session->userdata('user_id');
  $data = Modules::run('database/get', array(
              'table' => 'room_table_permission rtp',
              'where' => "rtp.deleted = 0 and rtp.user = '" . $user . "' "
              . "and rtp.room_body_table = '" . $id . "'"
  ));

  $is_has = 0;
  if (!empty($data)) {
   $is_has = 1;
  }


  return $is_has;
 }

 public function getDetailTableRow($id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_row_uniq ru',
              'field' => array('ru.*'),
              'where' => "ru.deleted = 0 and ru.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKolomTableValue($row_id) {
  $data = Modules::run('database/get', array(
              'table' => 'room_body_field_value rv',
              'field' => array('rv.*', 'rb.nama_field'),
              'join' => array(
                  array('room_body_table_has_field rb', 'rb.id = rv.room_body_table_has_field')
              ),
              'where' => "rv.deleted = 0 and rv.room_body_row_uniq = '" . $row_id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addData($table_id) {
  $data['view_file'] = 'form_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Data";
  $data['title_content'] = 'Tambah Data';
  $data['form'] = $this->getListKolomTable($table_id);
//  echo '<pre>';
//  print_r($data['form']);die;
  $data['table_id'] = $table_id;
  echo Modules::run('template', $data);
 }

 public function ubahData($id) {
  $table_data = $this->getDetailTableRow($id);

  $data['view_file'] = 'form_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Data";
  $data['title_content'] = 'Ubah Data';
  $data['form'] = $this->getListKolomTableValue($id);
  $data['id'] = $id;
//  echo '<pre>';
//  print_r($data['form']);die;
  echo Modules::run('template', $data);
 }

 public function detailData($id) {
  $table_data = $this->getDetailTableRow($id);

  $data['view_file'] = 'detail_input';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Data";
  $data['title_content'] = 'Detail Data';
  $data['form'] = $this->getListKolomTableValue($id);
  $data['id'] = $id;
  $data['table_id'] = $table_data['room_body_table'];
//  echo '<pre>';
//  print_r($data['form']);die;
  echo Modules::run('template', $data);
 }

 public function simpanValue() {
  $id = $this->input->post('id');
  $room_body_table = $this->input->post('table_id');
  $data = json_decode($this->input->post('data'));

  $is_valid = false;
  $this->db->trans_begin();
  try {
   if ($id == '') {
    if (!empty($data)) {
     $no_doc = Modules::run('no_generator/generateNoRow');
     $post_row['no_document'] = $no_doc;
     $post_row['room_body_table'] = $room_body_table;
     $post_row['user'] = $this->session->userdata('user_id');
     $id = Modules::run('database/_insert', 'room_body_row_uniq', $post_row);

     foreach ($data as $value) {
      $post_td['room_body_row_uniq'] = $id;
      $post_td['room_body_table_has_field'] = $value->field_id;
      $post_td['isi'] = $value->isi;
      Modules::run('database/_insert', 'room_body_field_value', $post_td);
     }
    }
   } else {
    //update
    if (!empty($data)) {
     foreach ($data as $value) {
      $post_data['isi'] = $value->isi;
      Modules::run('database/_update', 'room_body_field_value', $post_data,
              array(
                  'room_body_row_uniq' => $id,
                  'room_body_table_has_field' => $value->field_id
              )
      );
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function getListBulan() {
  $result = [];
  for ($i = 1; $i < 13; $i++) {
   switch ($i) {
    case 1:
     $result[$i] = 'Januari';
     break;
    case 2:
     $result[$i] = 'Februari';
     break;
    case 3:
     $result[$i] = 'Maret';
     break;
    case 4:
     $result[$i] = 'April';
     break;
    case 5:
     $result[$i] = 'Mei';
     break;
    case 6:
     $result[$i] = 'Juni';
     break;
    case 7:
     $result[$i] = 'Juli';
     break;
    case 8:
     $result[$i] = 'Agustus';
     break;
    case 9:
     $result[$i] = 'September';
     break;
    case 10:
     $result[$i] = 'Oktober';
     break;
    case 11:
     $result[$i] = 'November';
     break;
    case 12:
     $result[$i] = 'Desember';
     break;
    default:
     break;
   }
  }

  return $result;
 }

 public function createPlan() {
  $data['view_file'] = 'create_plan';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Create Workplan ";
  $data['title_content'] = 'Create Workplan';
  $data['list_bulan'] = $this->getListBulan();
  echo Modules::run('template', $data);
 }

 public function getPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'u.id as user_id'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai'),
              ),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function choosePIC() {
  $index_content = $_POST['index_content'];
  $data['index_content'] = $index_content;
  $data['data_pegawai'] = $this->getPegawai();
  echo $this->load->view('list_pic', $data, true);
 }

 public function updateWeekTimeline() {
  $id = $_POST['id'];
  $date = $_POST['date_str'];
//  echo '<pre>';
//  print_r($_POST);die;
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['tanggal'] = $date;
   Modules::run('database/_update', 'work_plan_item_timeline_day', $post, array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true),
           array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
