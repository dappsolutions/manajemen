<?php

class Dailytask extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $jabatan_id;
 public $struktur_id;
 public $pegawai_id;
 public $pegawai_data = array();

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
  $this->jabatan_id = $this->session->userdata('jabatan_id');
  $this->struktur_id = $this->session->userdata('struktur_id');
  $this->pegawai_id = $this->session->userdata('pegawai_id');
 }

 public function getModuleName() {
  return 'dailytask';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/dailytask.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pegawai_job_desk';
 }

 public function getRootModule() {
  return "";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Dailytask";
  $data['title_content'] = 'Dailytask';
  $content = $this->getDataDailytask();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getJabatanHasChild() {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.parent = '" . $this->struktur_id . "'"
  ));

  $is_has = false;
  if (!empty($data)) {
   $is_has = true;
  } else {
   if ($this->akses == 'superadmin') {
    $is_has = true;
   }
  }

  return $is_has;
 }

 public function getTotalDataDailytask($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'Superadmin') {
   $where = "pjd.deleted = 0 and jds.status != 'APPROVED'";
  } else {
   $where = "pjd.deleted = 0 and ((ut.id = '" . $user_id . "') or (u.id = '" . $user_id . "')) and (jds.status != 'APPROVED' and jds.status != 'CLOSED')";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi', 'jds.status', 'jd.jenis'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataDailytask($keyword = '') {
  $user_id = $this->session->userdata('user_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }

  $where = "pjd.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pjd.deleted = 0 and jds.status != 'APPROVED'";
  } else {
   $where = "pjd.deleted = 0 and ((ut.id = '" . $user_id . "') or (u.id = '" . $user_id . "')) and (jds.status != 'APPROVED' and jds.status != 'CLOSED')";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pjd',
                'field' => array('pjd.*', 'p.nama as nama_pegawai',
                    'p.nip', 'pg.nama as nama_pemberi',
                    'jds.status', 'jd.jenis', 'u.username'),
                'like' => $like,
                'join' => array(
                    array('pegawai p', 'pjd.pegawai = p.id'),
                    array('user ut', 'p.id = ut.pegawai'),
                    array('user u', 'pjd.giver = u.id', 'left'),
                    array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                    array('jabatan j', 'pg.jabatan = j.id', 'left'),
                    array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                    array('job_desk_status jds', 'pgs.id = jds.id'),
                    array('jenis_job jd', 'pjd.jenis_job = jd.id'),
                ),
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataDailytask($keyword)
  );
 }

 public function getDetailDataDailytask($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pjd',
              'field' => array('pjd.*', 'p.nama as nama_pegawai',
                  'p.nip', 'pg.nama as nama_pemberi',
                  'jds.status', 'jd.jenis', 'ut.id as user_pegawai'),
              'join' => array(
                  array('pegawai p', 'pjd.pegawai = p.id'),
                  array('user ut', 'ut.pegawai = p.id'),
                  array('user u', 'pjd.giver = u.id', 'left'),
                  array('pegawai pg', 'u.pegawai = pg.id', 'left'),
                  array('jabatan j', 'pg.jabatan = j.id', 'left'),
                  array('(select max(id) as id, pegawai_job_desk from job_desk_status group by pegawai_job_desk) pgs', 'pjd.id = pgs.pegawai_job_desk'),
                  array('job_desk_status jds', 'pgs.id = jds.id'),
                  array('jenis_job jd', 'pjd.jenis_job = jd.id'),
              ),
              'where' => "pjd.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getDataJabatanStruktur($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'jabatan_struktur js',
              'where' => "js.deleted = 0 and js.pegawai = '" . $pegawai . "'"
  ));

  $struktur = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $struktur = $data['id'];
  }

  return $struktur;
 }

 public function getChildStruktur($parent) {
  $sql = "select 
	js.id as js_id 
	, jh.title
	, pg.id as pegawai_id
	, pg.nip as nip
	, pg.nama as nama_pegawai
 , js.parent
from jabatan_struktur js
join pegawai pg
	on js.pegawai = pg.id
join jabatan_header jh
	on jh.id = js.jabatan_header
	where js.parent = '" . $parent . "'";
  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['term_pegawai'] = $value['nip'] . ' - ' . $value['nama_pegawai'];
    array_push($result, $value);
   }
  }

//if($parent == "19"){
// echo '<pre>';
// print_r($result);die;
//}
  return $result;
 }

 public function getListPegawai() {
  $pegawai = $this->pegawai_id;
  $this->pegawai_data = array();
  $js_id = $this->getDataJabatanStruktur($pegawai);

  $sql = "select 
	js.id as js_id 
	, jh.title
	, pg.id as pegawai_id
	, pg.nip as nip
	, pg.nama as nama_pegawai
 , js.parent
from jabatan_struktur js
join pegawai pg
	on js.pegawai = pg.id
join jabatan_header jh
	on jh.id = js.jabatan_header
	where js.id = '" . $js_id . "'";
  $data = Modules::run('database/get_custom', $sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['term_pegawai'] = $value['nip'] . ' - ' . $value['nama_pegawai'];
    array_push($result, $value);
   }
  }

  $this->getTreeNode($result);
 }

 public function getTreeNode($data) {
  foreach ($data as $value) {
   $child = $this->getChildStruktur($value['js_id']);

   if (!empty($child)) {
    foreach ($child as $v_child) {
     $result[$v_child['js_id']] = $v_child;
     $this->pegawai_data[$v_child['js_id']] = $v_child;
    }
    $this->getTreeNode($child);
   } else {
    $this->pegawai_data[$value['js_id']] = $value;
   }
  }
 }

 function buildTree(array $elements, $parentId = 0) {
  $branch = array();

  foreach ($elements as $element) {
   if ($element['parent'] == $parentId) {
    $children = $this->buildTree($elements, $element['js_id']);
    if ($children) {
//     echo '<pre>';
//     print_r($children);die;
     foreach ($children as $value) {
      $branch[$value['js_id']] = $value;
     }
    }
    $branch[$element['js_id']] = $element;
   }
  }

  return $branch;
 }

 public function add() {
  $data['has_child'] = $this->getJabatanHasChild();
  $data['pegawai_id'] = $this->pegawai_id;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Dailytask";
  $data['title_content'] = 'Tambah Dailytask';
  $this->getListPegawai();
  $data['list_pegawai'] = $this->pegawai_data;
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataDailytask($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Dailytask";
  $data['title_content'] = 'Ubah Dailytask';
  $data['list_pegawai'] = $this->getListPegawai();
  $data['has_child'] = $this->getJabatanHasChild();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataDailytask($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Dailytask";
  $data['title_content'] = "Detail Dailytask";
  $data['user_id'] = $this->session->userdata('user_id');
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getPostDataDailytask($value) {
  if ($value->pegawai != $this->pegawai_id) {
   $data['giver'] = $this->session->userdata('user_id');
   $data['pegawai'] = $value->pegawai;
   $data['jenis_job'] = '1';
  } else {
   $data['pegawai'] = $this->pegawai_id;
   $data['jenis_job'] = '1';
   //semua bawaha maka nanit harus dicari bawahan dari yang memberi pekerjaan lalu di insert ke pegawai
  }
  $data['keterangan'] = $value->keterangan;
  $data['tanggal'] = $value->tanggal;
  $data['time_target'] = $value->time_target;
  $data['sifat'] = $value->sifat;
  $data['proses_bisnis'] = $value->proses_bisnis;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataDailytask($data->jobdesk);
//   echo '<pre>';
//   print_r($post);
//   die;
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
    $js['pegawai_job_desk'] = $id;
    $js['status'] = 'DRAFT';
    Modules::run('database/_insert', 'job_desk_status', $js);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function doneTask() {
  $id = $this->input->post('id');
  $status = $this->input->post('status');
  $is_valid = false;
  switch ($status) {
   case 'DRAFT':
    $status = 'ONPROGRESS';
    break;
   case 'ONPROGRESS':
    $status = 'CLOSED';
    break;
   case 'CLOSED':
    $status = 'APPROVED';
    break;
   default:
    break;
  }

  $this->db->trans_begin();
  try {
   $post['pegawai_job_desk'] = $id;
   $post['status'] = $status;
   Modules::run('database/_insert', 'job_desk_status', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Dailytask";
  $data['title_content'] = 'Data Dailytask';
  $content = $this->getDataDailytask($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
