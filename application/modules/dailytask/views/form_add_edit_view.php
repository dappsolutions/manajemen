<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-3">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">
     <i onclick="Dailytask.main()" class="hover fa fa-file-text-o"></i>&nbsp;&nbsp;<?php echo ucfirst($module) ?>
     <hr/>
    </div>
   </div>
   <div class="col-md-9">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo 'Form' ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <form class="form-horizontal">
       <div class="form-group">
        <label class="control-label col-sm-1">Tanggal</label>
        <div class="col-sm-7">
         <input class="form-control required" readonly="" error="Tanggal" id="tanggal" type="text" placeholder="Tanggal" value="<?php echo isset($tanggal) ? $tanggal : '' ?>">
        </div>        
       </div>
       <?php // if ($has_child) { ?>
       <div class="form-group">
        <label class="control-label col-sm-1">Pegawai</label>
        <div class="col-sm-7">
         <select id="pegawai" class="form-control required" error="Pegawai">
          <option value="">Pilih Pegawai</option>
          <?php if (!empty($list_pegawai)) { ?>
           <?php foreach ($list_pegawai as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($pegawai)) { ?>
             <?php $selected = $pegawai == $value['pegawai_id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['pegawai_id'] ?>"><?php echo $value['term_pegawai'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>        
       </div>
       <?php // } ?>
     <!--<input type="hidden" value="" id="pegawai" class="form-control" />-->
       <div class="form-group">
        <label class="control-label col-sm-3">Uraian</label>
        <div class="col-sm-7">
         <textarea class="form-control required" error="Uraian" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
        </div>        
       </div>

       <div class="form-group">
        <label class="control-label col-sm-3">Time Target</label>
        <div class="col-sm-7">
         <input class="form-control required" readonly="" error="Time Target" id="time_target" type="text" placeholder="Time Target" value="<?php echo isset($time_target) ? $time_target : '' ?>">
        </div>        
       </div>

       <div class="form-group">
        <label class="control-label col-sm-1">Sifat</label>
        <div class="col-sm-7">
         <input class="form-control required" error="Sifat" id="sifat" type="text" placeholder="Sifat" value="<?php echo isset($sifat) ? $sifat : '' ?>">
        </div>        
       </div>

       <div class="form-group">
        <label class="control-label col-sm-3">Proses Bisnis</label>
        <div class="col-sm-7">
         <input class="form-control required" error="Proses Bisnis" id="proses_bisnis" type="text" placeholder="Proses Bisnis" value="<?php echo isset($proses_bisnis) ? $proses_bisnis : '' ?>">
        </div>        
       </div>
      </form>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <button class="btn btn-warning" type="button" onclick="Dailytask.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Dailytask.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Batal</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
