<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type='hidden' name='' id='status' class='form-control' value='<?php echo isset($status) ? $status : '' ?>'/>


<div class="row">
 <div class="col-md-12">
  <div class="row">
   <!--   <div class="col-md-3">
       <div class="tile">
        <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;<?php echo $title_content ?></h5>
       </div>
      </div>-->
   <div class="col-md-12">
    <div class="box box-solid box-primary">
     <div class="box-header ui-sortable-handle" style="cursor: move;">
      <i class="fa fa-file-text-o"></i> &nbsp;<?php echo ucwords($title_content) ?>
     </div>
    </div>
    <div class="tile">     
     <div class="tile-body">
      <div class="row">
       <div class="col-md-3 ">
        <b>Tanggal</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $tanggal ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Nama Pegawai</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $nip . ' - ' . $nama_pegawai ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Uraian</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $keterangan ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Time Target</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $time_target ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Sifat</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $sifat ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Proses Bisnis</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php echo $proses_bisnis ?>
       </div>        
      </div>
      <br/>
      <div class="row">
       <div class="col-md-3 ">
        <b>Status</b>
       </div> 
       <div class="col-sm-5 text-success">
        <?php if ($status == 'DRAFT') { ?>
         <button class="btn btn-warning font-10"><?php echo $status ?></button>
        <?php } ?>
        <?php if ($status == 'ONPROGRESS') { ?>
         <button class="btn btn-primary font-10"><?php echo $status ?></button>
        <?php } ?>
        <?php if ($status == 'REVIEW') { ?>
         <button class="btn btn-info font-10"><?php echo $status ?></button>
        <?php } ?>
        <?php if ($status == 'CLOSED' || $status == 'APPROVED') { ?>
         <button class="btn btn-success font-10"><?php echo $status ?></button>
        <?php } ?>
       </div>        
      </div>
     </div>
     <div class="tile-footer text-right">
      <!--<div class="col-sm-6">-->
      <?php if ($status == 'DRAFT') { ?>
       <?php if ($user_pegawai == $user_id) { ?>
        &nbsp;&nbsp;&nbsp;<a class="btn btn-primary text-white" onclick="Dailytask.doneTask('<?php echo $id ?>', event)"><i class="fa fa-fw fa-lg fa-check"></i>Kerjakan</a>
       <?php } ?>
      <?php } ?>
      <?php if ($status == 'ONPROGRESS') { ?>
       <?php if ($user_pegawai == $user_id) { ?>
        &nbsp;&nbsp;&nbsp;<a class="btn btn-primary text-white" onclick="Dailytask.doneTask('<?php echo $id ?>', event)"><i class="fa fa-fw fa-lg fa-check"></i>Selesai</a>
       <?php } ?>
      <?php } ?>
      <?php if ($status == 'CLOSED' && $giver != '') { ?>
       <?php if ($giver == $user_id) { ?>
        &nbsp;&nbsp;&nbsp;<a class="btn btn-primary text-white" onclick="Dailytask.doneTask('<?php echo $id ?>', event)"><i class="fa fa-fw fa-lg fa-check"></i>APPROVE</a>
       <?php } ?>
      <?php } ?>
      &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary text-white" onclick="Dailytask.back()"><i class="fa fa-fw fa-lg fa-times-circle"></i>Kembali</a>
      <!--</div>-->      
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
