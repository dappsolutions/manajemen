<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }
 
 public function generateNoRoom() {
  $no = 'ROOM' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'room',
  'like' => array(
  array('nomer_room', $no)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['nomer_room']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }
 
 public function generateNoRow() {
  $no = 'VAL' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'room_body_row_uniq',
  'like' => array(
  array('no_document', $no)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['no_document']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }

}
